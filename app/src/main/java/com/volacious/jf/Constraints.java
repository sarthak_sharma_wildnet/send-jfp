package com.volacious.jf;

public class Constraints {

    //Bottom navigation buttons
    public static final String TO_DO_BUTTON = "toDoButton";
    public static final String FILE_TRANSFERS_BUTTON = "fileTransfersButton";
    public static final String HISTORY_BUTTON = "historyButton";

    public static final String AS_HOME_FRAGMENT = "asHomeFragment";

    //PhotoshootStatus
    public static final String STATUS_UPLOADED = "delivered";
    public static final String STATUS_UPLOADING = "uploading";
    public static final String STATUS_COPYING = "copying";
    public static final String STATUS_COPIED = "copied";
    public static final String STATUS_NONE = "none"; //prepared to upload?
    public static final String STATUS_IN_PROGRES = "inProgress";
}
