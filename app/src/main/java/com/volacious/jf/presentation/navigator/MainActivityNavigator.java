package com.volacious.jf.presentation.navigator;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.volacious.jf.R;
import com.volacious.jf.presentation.cicerone.RocketFragmentNavigator;
import com.volacious.jf.presentation.router.base.Screens;
import com.volacious.jf.ui.fragment.FileTransfersFragment;
import com.volacious.jf.ui.fragment.MainFragment;
import com.volacious.jf.ui.fragment.StartUploadFragment;
import com.volacious.jf.ui.fragment.ToDoFragment;
import com.volacious.jf.ui.fragment.UploadProcessFragment;
import java.lang.ref.WeakReference;
import ru.terrakok.cicerone.commands.Command;

public class MainActivityNavigator extends RocketFragmentNavigator {

    private WeakReference<AppCompatActivity> activityWeakReference;

    public MainActivityNavigator(AppCompatActivity activity, FragmentManager fragmentManager, int containerId) {
        super(fragmentManager, containerId);
        activityWeakReference = new WeakReference<>(activity);
    }

    @Override
    protected Fragment createFragment(String screenKey, Object data) {
        switch (screenKey) {
            case Screens.MAIN_FRAGMENT: {
                return MainFragment.newInstance();
            }
            case Screens.FILE_TRANSFERS_FRAGMENT: {
                return FileTransfersFragment.newInstance();
            }
            case Screens.TODO_FRAGMENT: {
                return ToDoFragment.newInstance();
            }
            case Screens.UPLOAD_PROCESS_FRAGMENT: {
                return UploadProcessFragment.newInstance((int) data);
            }
            case Screens.START_UPLOAD_FRAGMENT: {
                return StartUploadFragment.newInstance((int) data);
            }
            default:
                throw new RuntimeException("Unknown screen key");
        }
    }

    @Override
    protected void showSystemMessage(String message) {
        if (activityWeakReference != null && activityWeakReference.get() != null) {
            Toast.makeText(activityWeakReference.get(), message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void exit() {
        if (activityWeakReference != null && activityWeakReference.get() != null) {
            activityWeakReference.get().finish();
        }
    }



    @Override
    protected void setupFragmentTransactionAnimation(Command command, Fragment currentFragment, Fragment nextFragment, FragmentTransaction fragmentTransaction) {
        fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_left,
                R.anim.slide_in_right,
                R.anim.slide_out_right,
                R.anim.slide_out_left
        );
    }
}
