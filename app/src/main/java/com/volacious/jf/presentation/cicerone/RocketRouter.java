package com.volacious.jf.presentation.cicerone;

import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.commands.Forward;
import ru.terrakok.cicerone.commands.Replace;

public class RocketRouter extends Router {

    private Integer overrideContainerId;

    public RocketRouter overrideContainer(int containerId) {
        overrideContainerId = containerId;
        return this;
    }

    @Override
    public void navigateTo(String screenKey, Object data) {
        if (overrideContainerId != null) {
            executeCommand(new ContainerForward(overrideContainerId, screenKey, data));
            overrideContainerId = null;
        } else {
            executeCommand(new Forward(screenKey, data));
        }
    }

    @Override
    public void replaceScreen(String screenKey, Object data) {
        if (overrideContainerId != null) {
            executeCommand(new ContainerReplace(overrideContainerId, screenKey, data));
            overrideContainerId = null;
        } else {
            executeCommand(new Replace(screenKey, data));
        }
    }
}
