package com.volacious.jf.presentation.navigator;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.volacious.jf.R;
import com.volacious.jf.presentation.router.base.Screens;
import com.volacious.jf.ui.fragment.LoginFragment;
import java.lang.ref.WeakReference;
import ru.terrakok.cicerone.android.SupportFragmentNavigator;
import ru.terrakok.cicerone.commands.Command;

public class LoginActivityNavigator extends SupportFragmentNavigator {
    private WeakReference<AppCompatActivity> activityWeakReference;

    public LoginActivityNavigator(AppCompatActivity activity, FragmentManager fragmentManager, int containerId) {
        super(fragmentManager, containerId);
        activityWeakReference = new WeakReference<>(activity);
    }

    @Override
    protected Fragment createFragment(String screenKey, Object data) {
        switch (screenKey) {
            case Screens.LOGIN_FRAGMENT: {
                return LoginFragment.newInstance();
            }
            default:
                throw new RuntimeException("Unknown screen key");
        }
    }

    @Override
    protected void showSystemMessage(String message) {
        if (activityWeakReference != null && activityWeakReference.get() != null) {
            Toast.makeText(activityWeakReference.get(), message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void exit() {
        if (activityWeakReference != null && activityWeakReference.get() != null) {
            activityWeakReference.get().finish();
        }
    }

    @Override
    protected void setupFragmentTransactionAnimation(Command command, Fragment currentFragment, Fragment nextFragment, FragmentTransaction fragmentTransaction) {
        fragmentTransaction.setCustomAnimations(
                R.anim.slide_in_left,
                R.anim.slide_in_right,
                R.anim.slide_out_right,
                R.anim.slide_out_left
        );
    }
}
