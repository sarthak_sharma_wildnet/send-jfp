package com.volacious.jf.presentation.router.base;

public class Screens {

    //Activity
    public static final String LOGIN_ACTIVITY = "LoginActivity";
    public static final String MAIN_ACTIVITY = "MainActivity";

    //Fragment
    public static final String LOGIN_FRAGMENT = "LoginFragment";
    public static final String MAIN_FRAGMENT = "MainFragment";
    public static final String MAIN_HOME_FRAGMENT = "MainHomeFragment";
    public static final String UPLOAD_FRAGMENT = "UploadFragment";
    public static final String HISTORY_FRAGMENT = "HistoryFragment";
    public static final String START_UPLOAD_FRAGMENT = "StartUploadFragment";
    public static final String UPLOAD_PROCESS_FRAGMENT = "UploadProcessFragment";
    public static final String TODO_FRAGMENT = "ToDoFragment";
    public static final String FILE_TRANSFERS_FRAGMENT = "FileTransfersFragment" ;
}
