package com.volacious.jf.presentation.cicerone;

public interface ContainerCommand {
    int getContainerId();
}
