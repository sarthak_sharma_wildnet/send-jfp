package com.volacious.jf.presentation.cicerone;

import ru.terrakok.cicerone.commands.Replace;

public class ContainerReplace extends Replace implements ContainerCommand {

    private int containerId;

    public ContainerReplace(int containerId, String screenKey, Object transitionData) {
        super(screenKey, transitionData);
        this.containerId = containerId;
    }

    @Override
    public int getContainerId() {
        return containerId;
    }
}
