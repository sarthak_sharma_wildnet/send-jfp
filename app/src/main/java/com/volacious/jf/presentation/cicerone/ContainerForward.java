package com.volacious.jf.presentation.cicerone;

import ru.terrakok.cicerone.commands.Forward;

public class ContainerForward extends Forward implements ContainerCommand {

    private int containerId;

    public ContainerForward(int containerId, String screenKey, Object transitionData) {
        super(screenKey, transitionData);
        this.containerId = containerId;
    }

    @Override
    public int getContainerId() {
        return containerId;
    }
}
