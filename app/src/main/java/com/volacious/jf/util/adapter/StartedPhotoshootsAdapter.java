package com.volacious.jf.util.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.R;
import com.volacious.jf.engine.network.result.Photoshoot;
import com.volacious.jf.util.viewholder.PhotoshootViewHolder;
import com.volacious.jf.util.viewholder.StartedPhotoshootViewHolder;

import java.util.ArrayList;
import java.util.List;

public class StartedPhotoshootsAdapter extends RecyclerView.Adapter {

    private List<Photoshoot> photoshoots;

    public StartedPhotoshootsAdapter(List<Photoshoot> photoshoots) {
        this.photoshoots = photoshoots;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.started_photoshoot_viewholder, viewGroup, false);
        return new StartedPhotoshootViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ((StartedPhotoshootViewHolder) viewHolder).bind(photoshoots.get(i));
    }

    @Override
    public int getItemCount() {
        return photoshoots.size();
    }
}
