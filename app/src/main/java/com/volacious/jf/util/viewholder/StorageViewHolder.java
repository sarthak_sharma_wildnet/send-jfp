package com.volacious.jf.util.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.R;
import com.volacious.jf.util.adapter.storages.UploadStorage;
import com.volacious.jf.util.callback.StorageSelectedCallback;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StorageViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.path_text)
    TextView pathText;

    WeakReference<StorageSelectedCallback> storageSelectedCallbackWeakReference;

    public StorageViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    private UploadStorage storage;

    public void bind(UploadStorage storage, UploadStorage selectedStorage) {
        this.storage = storage;
        pathText.setText(storage.getName());
        if (storage.equals(selectedStorage)) {
            //blue border
            pathText.setSelected(true);
        } else {
            //gray
            pathText.setSelected(false);
        }
    }

    @OnClick(R.id.path_text)
    void onStorageSelected() {
        pathText.setSelected(!pathText.isSelected());
        if (storageSelectedCallbackWeakReference != null && storageSelectedCallbackWeakReference.get() != null) {
            storageSelectedCallbackWeakReference.get().storageSelected(storage);
        }
    }

    public void setStorageSelectedCallback(StorageSelectedCallback storageSelectedCallback) {
        this.storageSelectedCallbackWeakReference = new WeakReference<>(storageSelectedCallback);
    }
}
