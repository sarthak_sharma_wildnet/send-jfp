package com.volacious.jf.util;

import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

public class ConstraintLayoutHelper {
    public interface ConstraintSetProcessor {
        void proccessConstraintSet(ConstraintSet set, ConstraintLayout layout, View view);
    }

    public static void updateConstraintSet(View view, ConstraintSetProcessor constraintSetProcessor) {
        ConstraintLayout layout = (ConstraintLayout) view.getParent();
        ConstraintSet set = new ConstraintSet();
        set.clone(layout);
        constraintSetProcessor.proccessConstraintSet(set, layout, view);
        set.applyTo(layout);
    }

}
