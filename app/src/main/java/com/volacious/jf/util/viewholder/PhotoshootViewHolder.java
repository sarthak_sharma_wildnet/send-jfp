package com.volacious.jf.util.viewholder;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.Constraints;
import com.volacious.jf.JfuerstApplication;
import com.volacious.jf.R;
import com.volacious.jf.engine.network.result.Photoshoot;
import com.volacious.jf.helpers.DateHelper;
import com.volacious.jf.presentation.router.base.Screens;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotoshootViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.street_address)
    TextView addressTextView;

    @BindView(R.id.street_date)
    TextView dateTextView;

    @BindView(R.id.street_upload_button)
    Button uploadButton;

    @BindView(R.id.street_status_layout)
    ConstraintLayout statusLayout;

    @BindView(R.id.street_status_success_image)
    ImageView statusImage;

    @BindView(R.id.street_status_success_time)
    TextView statusUpperText;

    @BindView(R.id.street_status_success_percent)
    TextView statusBottomText;

    @BindView(R.id.street_status_uploading_indicator)
    ProgressBar uploadingIndicator;

    @BindView(R.id.street_info_layout)
    ConstraintLayout photoshootInfoLayout;

    private int photoshootID;

    public PhotoshootViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Photoshoot photoshoot) {
        photoshootID = photoshoot.getId();
        addressTextView.setText(photoshoot.getProperty() != null ? photoshoot.getProperty().getAddress() : "");
        dateTextView.setText(DateHelper.getDateAsMonthDYHM(photoshoot.getDatetime()));
        //changeStatus(Constraints.STATUS_NONE);
    }

    void changeStatus(String status) {
        switch (status) {
            case Constraints.STATUS_UPLOADED: {
                //uploadButton.setVisibility(View.GONE);
                statusLayout.setVisibility(View.VISIBLE);
                statusUpperText.setText(DateHelper.getDateAsHM(null));
                statusBottomText.setText("100% done");
                statusImage.setVisibility(View.VISIBLE);
                uploadingIndicator.setVisibility(View.GONE);
                break;
            }
            case Constraints.STATUS_UPLOADING: {
                // uploadButton.setVisibility(View.GONE);
                statusLayout.setVisibility(View.VISIBLE);
                statusUpperText.setText("upload speed");
                statusBottomText.setText("% done");
                statusImage.setVisibility(View.INVISIBLE);
                uploadingIndicator.setVisibility(View.VISIBLE);
                break;
            }
            case Constraints.STATUS_COPYING: {
                //  uploadButton.setVisibility(View.GONE);
                statusLayout.setVisibility(View.VISIBLE);
                statusUpperText.setText("copy speed");
                statusBottomText.setText("% done");
                statusImage.setVisibility(View.INVISIBLE);
                uploadingIndicator.setVisibility(View.VISIBLE);
                break;
            }
            case Constraints.STATUS_COPIED: {
                //   uploadButton.setVisibility(View.GONE);
                statusLayout.setVisibility(View.VISIBLE);
                statusUpperText.setText("Copy complete");
                statusBottomText.setText("Start upload!");
                statusImage.setVisibility(View.GONE);
                uploadingIndicator.setVisibility(View.GONE);
                break;
            }
            case Constraints.STATUS_NONE: {
                uploadButton.setVisibility(View.VISIBLE);
                statusLayout.setVisibility(View.INVISIBLE);
                break;
            }
        }
    }

    @OnClick(R.id.street_upload_button)
    void onUpload() {
        JfuerstApplication.INSTANCE.getRouter().navigateTo(Screens.START_UPLOAD_FRAGMENT, photoshootID);
    }

    @OnClick(R.id.street_info_layout)
    void goToPhotoshoot() {
        //JfuerstApplication.INSTANCE.getRouter().navigateTo(Screens.UPLOAD_PROCESS_FRAGMENT, photoshootID);
    }

}
