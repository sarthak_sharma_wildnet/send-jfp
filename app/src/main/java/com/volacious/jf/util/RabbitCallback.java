package com.volacious.jf.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by John on 2/20/2018.
 */

public class RabbitCallback {
    private Object loadingView;
    private Date expirationDate;
    private WeakReference<UserNotificationDelegate> userNotificationDelegateWeakRef;

    public void onSuccess(JSONObject resp) {
    }

    public void onFailure() {
    }

    public RabbitCallback() {
        setExpirationTime(20);
    }

    public RabbitCallback(@Nullable Object view) {
        this();
        loadingView = view;
    }

    public RabbitCallback(@NonNull UserNotificationDelegate userNotificationDelegate) {
        this();
        this.userNotificationDelegateWeakRef = new WeakReference<>(userNotificationDelegate);
    }

    public RabbitCallback(@Nullable Object loadingView, @NonNull UserNotificationDelegate userNotificationDelegate) {
        this();
        this.loadingView = loadingView;
        this.userNotificationDelegateWeakRef = new WeakReference<>(userNotificationDelegate);
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public RabbitCallback setExpirationTime(int seconds) {
        Date d = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.SECOND, seconds);
        this.expirationDate = cal.getTime();
        return this;
    }

    public WeakReference<UserNotificationDelegate> getUserNotificationDelegateWeakRef() {
        return userNotificationDelegateWeakRef;
    }
}
