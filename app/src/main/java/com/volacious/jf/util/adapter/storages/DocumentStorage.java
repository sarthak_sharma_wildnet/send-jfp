package com.volacious.jf.util.adapter.storages;

import android.content.Context;

import com.volacious.jf.engine.copyservice.processors.copy.DocumentImpl;
import com.volacious.jf.engine.copyservice.processors.copy.FileInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.documentfile.provider.DocumentFile;

public class DocumentStorage implements UploadStorage {
    private DocumentFile documentFile;
    private List<FileInterface> sourceFiles;
    private long length;

    public DocumentStorage(DocumentFile documentFile, Context context) {
        this.documentFile = documentFile;
        sourceFiles = filesList(documentFile, context);
        length = 0;
        for (FileInterface sourceFile : sourceFiles) {
            length += sourceFile.length();
        }
    }

    public static List<FileInterface> filesList(DocumentFile sourceLocation, Context context) {
        if (sourceLocation.isDirectory()) {
            List<FileInterface> res = new ArrayList<>();
            DocumentFile[] files = sourceLocation.listFiles();
            for (DocumentFile file : files) {
                res.addAll(filesList(file, context));
            }
            return res;
        } else {
            return Collections.singletonList(new DocumentImpl(sourceLocation, context));
        }
    }


    @Override
    public long getLength() {
        return length;
    }

    @Override
    public List<FileInterface> getSourceFiles() {
        return sourceFiles;
    }

    @Override
    public String getName() {
        return "Choosen: " + documentFile.getName();
    }

    public DocumentFile getDocumentFile() {
        return documentFile;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof DocumentStorage) {
            DocumentStorage ds = (DocumentStorage) obj;
            if (getDocumentFile() == null && ds.getDocumentFile() != null) {
                return false;
            }
            return getDocumentFile().equals(ds.getDocumentFile());
        }
        return false;
    }
}
