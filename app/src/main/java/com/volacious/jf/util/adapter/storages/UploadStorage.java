package com.volacious.jf.util.adapter.storages;

import com.volacious.jf.engine.copyservice.processors.copy.FileInterface;

import java.util.List;

public interface UploadStorage {
    String getName();
    long getLength();
    List<FileInterface> getSourceFiles();
}
