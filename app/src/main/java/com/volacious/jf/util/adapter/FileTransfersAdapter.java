package com.volacious.jf.util.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.R;
import com.volacious.jf.engine.realm.models.ProgressStatusModel;
import com.volacious.jf.util.viewholder.FileTransferViewHolder;

import java.util.List;

public class FileTransfersAdapter extends RecyclerView.Adapter {

    private List<ProgressStatusModel> fileTransfers;

    public FileTransfersAdapter(List<ProgressStatusModel> fileTransfers) {
        this.fileTransfers = fileTransfers;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.file_transfer_viewholder, viewGroup, false);
        return new FileTransferViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ((FileTransferViewHolder) viewHolder).bind(fileTransfers.get(i));
    }

    @Override
    public int getItemCount() {
        return fileTransfers.size();
    }
}
