package com.volacious.jf.util.adapter.storages;

import com.volacious.jf.engine.copyservice.processors.FileUtils;
import com.volacious.jf.engine.copyservice.processors.copy.FileImpl;
import com.volacious.jf.engine.copyservice.processors.copy.FileInterface;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

public class PathStorage implements UploadStorage {
    private String path;
    private List<FileInterface> sourceFiles;
    private long length;

    public PathStorage(String path) {
        this.path = path;

        List<File> files = FileUtils.filesList(new File(path));
        FileUtils.sortFiles(files);
        sourceFiles = new ArrayList<>();
        length = 0;
        for (File file : files) {
            sourceFiles.add(new FileImpl(file));
            length += file.length();
        }
    }

    @Override
    public List<FileInterface> getSourceFiles() {
        return sourceFiles;
    }

    @Override
    public long getLength() {
        return length;
    }

    @Override
    public String getName() {
        return path;
    }

    public String getPath() {
        return path;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof PathStorage) {
            PathStorage ps = (PathStorage)obj;
            if (getPath() == null && ps.getPath() != null) {
                return false;
            }
            return getPath().equals(ps.getPath());
        }
        return false;
    }
}
