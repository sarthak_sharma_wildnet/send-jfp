package com.volacious.jf.util.toast;

import android.widget.Toast;

import com.volacious.jf.JfuerstApplication;
import com.volacious.jf.util.queueHelper.QueueHelper;

import java.lang.ref.WeakReference;

import es.dmoral.toasty.Toasty;

/**
 * Created by Nik on 08.09.2018.
 */
public class ToastManager {
    private static ToastManager instance;

    private ToastManager() {
    }

    public static synchronized ToastManager getInstance() {
        if (instance == null) {
            instance = new ToastManager();
        }
        return instance;
    }

    private WeakReference<Toast> toastWeakReference;

    public void showError(String errorMessage) {
        showToast(() ->
                Toasty.error(JfuerstApplication.INSTANCE, errorMessage, Toast.LENGTH_SHORT, true));
    }

    public void showSuccess(String successMessage) {
        showToast(() ->
                Toasty.success(JfuerstApplication.INSTANCE, successMessage, Toast.LENGTH_SHORT, true));
    }

    private void showToast(ToastMaker maker) {
        QueueHelper.performOnMainThread(() -> {
            if (toastWeakReference != null) {
                Toast toast = toastWeakReference.get();
                if (toast != null) {
                    toast.cancel();
                    toastWeakReference.clear();
                }
                toastWeakReference = null;
            }
            maker.prepareToast().show();
        });
    }

    private interface ToastMaker {
        Toast prepareToast();
    }
}
