package com.volacious.jf.util.adapter;

import androidx.annotation.Nullable;

public class FileStatus {

    public enum Status {
        none, copyDone, uploading, uploadDone
    }

    private String filename;
    private Status status;

    public FileStatus(String filename, Status status) {
        this.filename = filename;
        this.status = status;
    }

    public String getFilename() {
        return filename;
    }

    public Status getStatus() {
        return status;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof FileStatus) {
            FileStatus fs = (FileStatus)obj;
            return getStatus().equals(fs.getStatus()) &&
                    getFilename().equals(fs.getFilename());
        }
        return false;
    }
}
