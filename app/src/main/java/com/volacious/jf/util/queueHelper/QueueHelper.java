package com.volacious.jf.util.queueHelper;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

/**
 * Created by 76332 on 12.05.2018.
 */

public class QueueHelper {

    public static void performOnMainThread(Runnable runnable) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            Handler mainHandler = new Handler(Looper.getMainLooper());
            mainHandler.post(runnable);
        } else {
            runnable.run();
        }
    }

    public static void performOnBackgroundThread(Runnable runnable) {
        AsyncTask.execute(runnable);
    }
}
