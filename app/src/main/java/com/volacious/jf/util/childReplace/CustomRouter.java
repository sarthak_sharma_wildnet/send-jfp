package com.volacious.jf.util.childReplace;

import androidx.fragment.app.FragmentManager;

import ru.terrakok.cicerone.Router;

public class CustomRouter extends Router {

    public void replaceChildScreen(String screenKey, FragmentManager childFragmentManager) {
        replaceChildScreen(screenKey, null, childFragmentManager);
    }

    public void replaceChildScreen(String screenKey, Object data, FragmentManager childFragmentManager) {
        executeCommand(new ChildReplace(screenKey, data, childFragmentManager));
    }

}
