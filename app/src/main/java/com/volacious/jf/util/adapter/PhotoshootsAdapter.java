package com.volacious.jf.util.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.R;
import com.volacious.jf.engine.network.result.Photoshoot;
import com.volacious.jf.util.viewholder.PhotoshootViewHolder;

import java.util.ArrayList;

public class PhotoshootsAdapter extends RecyclerView.Adapter {

    ArrayList<Photoshoot> photoshoots;

    public PhotoshootsAdapter(ArrayList<Photoshoot> photoshoots) {
        this.photoshoots = photoshoots;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.street_viewholder, viewGroup, false);
        return new PhotoshootViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ((PhotoshootViewHolder) viewHolder).bind(photoshoots.get(i));
    }

    @Override
    public int getItemCount() {
        return photoshoots.size();
    }
}
