package com.volacious.jf.util;


import androidx.annotation.NonNull;

public interface UserNotificationDelegate {
    void showError(@NonNull String errorMessage);

    void showSuccess(@NonNull String successMessage);
}