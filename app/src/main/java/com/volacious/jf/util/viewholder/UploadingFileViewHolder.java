package com.volacious.jf.util.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.R;
import com.volacious.jf.util.adapter.FileStatus;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.volacious.jf.util.adapter.FileStatus.Status.none;
import static com.volacious.jf.util.adapter.FileStatus.Status.uploadDone;
import static com.volacious.jf.util.adapter.FileStatus.Status.uploading;

public class UploadingFileViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.uploading_file_name)
    TextView fileName;

    @BindView(R.id.uploading_file_image)
    ImageView fileUploadImage;

    @BindView(R.id.copying_file_image)
    ImageView fileCopyImage;

    @BindView(R.id.upload_process_status_progress)
    ProgressBar progressBar;

    public UploadingFileViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(FileStatus fileStatus) {
        fileName.setText(fileStatus.getFilename());
        fileCopyImage.setVisibility(fileStatus.getStatus() == none  ? View.GONE : View.VISIBLE);
        fileUploadImage.setVisibility(fileStatus.getStatus() == uploadDone ? View.VISIBLE : View.GONE);
        progressBar.setVisibility(fileStatus.getStatus() == uploading ? View.VISIBLE : View.GONE);
    }

}
