package com.volacious.jf.util.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.JfuerstApplication;
import com.volacious.jf.R;
import com.volacious.jf.engine.copyservice.processors.copy.CopyProcessor;
import com.volacious.jf.engine.copyservice.processors.upload.UploadProcessor;
import com.volacious.jf.engine.realm.RealmManager;
import com.volacious.jf.engine.realm.models.ProgressStatusModel;
import com.volacious.jf.presentation.router.base.Screens;
import com.volacious.jf.ui.fragment.uploadprocess.UIUpdateEvent;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class FileTransferViewHolder extends RecyclerView.ViewHolder {

    public static final int COMPLETE = 0;
    public static final int IN_PROGRESS = 1;
    public static final int NOTHING = -1;

    int fileTransferId;


    @BindView(R.id.file_copy_info)
    TextView fileTransferCopyInfo;

    @BindView(R.id.file_upload_info)
    TextView fileTransferUploadInfo;

    @BindView(R.id.file_transfer_progress)
    ProgressBar fileTransferProgress;

    @BindView(R.id.file_transfer_status_layout)
    ConstraintLayout fileTransferStatusLayout;

    @BindView(R.id.file_transfer_complete_image)
    ImageView fileTransferCompliteImage;

    @BindView(R.id.file_completed_number)
    TextView fileCompletedText;

    @BindView(R.id.file_completed_label)
    TextView fileCompletedLabel;

    @BindView(R.id.file_completed_count_layout)
    ConstraintLayout fileCompletedLayout;

    public FileTransferViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        fileTransferCopyInfo.setText(null);
        fileTransferUploadInfo.setText(null);
        statusChanged(IN_PROGRESS);
    }

    private void updateUI(UIUpdateEvent updateEvent) {
        fileTransferUploadInfo.setText("Upload not started");
        if (updateEvent.getCopyStatus() > 0) {
            if (updateEvent.getCopyStatus() > 1) {
                if (updateEvent.getUploadStatus() > 0) {
                    if (updateEvent.getUploadStatus() > 1) {

                        //upload finished
                        fileTransferCopyInfo.setText(updateEvent.getCopyLabel());
                        fileTransferUploadInfo.setText(updateEvent.getUploadLabel());
                        fileCompletedText.setText(updateEvent.getFilesProgressStatus());
                        fileCompletedLabel.setText(updateEvent.getSpeedLabel());
                        statusChanged(COMPLETE);
                    } else {

                        //upload in process
                        fileTransferCopyInfo.setText(updateEvent.getCopyLabel());
                        fileTransferUploadInfo.setText(updateEvent.getStatusLabel());

                        fileCompletedText.setText(updateEvent.getFilesProgressStatus());
                        fileCompletedLabel.setText(updateEvent.getSpeedLabel());

                        statusChanged(IN_PROGRESS);
                    }
                } else {

                    //prepare to upload
                    fileTransferCopyInfo.setText(updateEvent.getCopyLabel());
                    fileTransferUploadInfo.setText(updateEvent.getUploadLabel());
                    fileCompletedText.setText(updateEvent.getFilesProgressStatus());
                    fileCompletedLabel.setText(updateEvent.getStatusLabel());

                    statusChanged(IN_PROGRESS);
                }
            } else {
                //copy in progress
                fileTransferCopyInfo.setText(updateEvent.getStatusLabel());
                fileTransferUploadInfo.setText(updateEvent.getUploadLabel());

                fileCompletedText.setText(updateEvent.getFilesProgressStatus());
                fileCompletedLabel.setText(updateEvent.getSpeedLabel());
                statusChanged(IN_PROGRESS);
            }
        } else {

            //copy not started
            fileTransferCopyInfo.setText("Copy not started");
            fileTransferUploadInfo.setText("Upload not started");

            fileCompletedText.setText(updateEvent.getFilesProgressStatus());
            fileCompletedLabel.setText(updateEvent.getStatusLabel());

            statusChanged(NOTHING);
        }

    }

    private RealmResults<ProgressStatusModel> progressStatusModels;

    private RealmChangeListener<RealmResults<ProgressStatusModel>> uploadListener = uploadStatusModels -> {
        int x = 1;
        for (ProgressStatusModel progressStatusModel : uploadStatusModels) {
            if (progressStatusModel.getId() == fileTransferId) {
                updateUI(progressStatusModel.getUIUpdateEvent());
                break;
            }
        }
    };

    private void installListener(int fileTransferId) {
        progressStatusModels = RealmManager.getRealmInstance()
                .where(ProgressStatusModel.class)
                .equalTo("id", fileTransferId)
                .findAll();
        progressStatusModels.addChangeListener(uploadListener);
        uploadListener.onChange(progressStatusModels);
    }

    public void bind(ProgressStatusModel model) {
        fileTransferId = model.getId();
        installListener(fileTransferId);
    }

    void statusChanged(int status) {
        switch (status) {
            case COMPLETE: {
                fileTransferStatusLayout.setVisibility(View.VISIBLE);
                fileTransferProgress.setVisibility(View.GONE);
                fileTransferCompliteImage.setVisibility(View.VISIBLE);
                break;
            }
            case IN_PROGRESS: {
                fileTransferStatusLayout.setVisibility(View.VISIBLE);
                if (CopyProcessor.fileTransferInProgress == fileTransferId ||
                        UploadProcessor.fileTransferInProgress == fileTransferId) {
                    fileTransferProgress.setVisibility(View.VISIBLE);
                } else {
                    fileTransferProgress.setVisibility(View.GONE);
                }
                fileTransferCompliteImage.setVisibility(View.GONE);
                break;
            }
            case NOTHING: {
                fileTransferStatusLayout.setVisibility(View.INVISIBLE);
                break;
            }
        }
    }

    @OnClick(R.id.file_transfer_layout)
    void toFileTransferInfo() {
        JfuerstApplication.INSTANCE.getRouter().navigateTo(Screens.UPLOAD_PROCESS_FRAGMENT, fileTransferId);
    }

}
