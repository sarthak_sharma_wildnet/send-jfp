package com.volacious.jf.util.viewholder;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.JfuerstApplication;
import com.volacious.jf.R;
import com.volacious.jf.engine.network.result.Photoshoot;
import com.volacious.jf.engine.realm.RealmManager;
import com.volacious.jf.engine.realm.models.ProgressStatusModel;
import com.volacious.jf.helpers.DateHelper;
import com.volacious.jf.presentation.router.base.Screens;
import com.volacious.jf.util.adapter.FileTransfersAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.OrderedRealmCollection;
import io.realm.Sort;

public class StartedPhotoshootViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.street_address)
    TextView addressTextView;

    @BindView(R.id.street_date)
    TextView dateTextView;

    @BindView(R.id.street_upload_button)
    Button uploadButton;

    @BindView(R.id.file_transfers_recyclerview)
    RecyclerView fileTransfersRecycler;

    private int photoshootID;

    public StartedPhotoshootViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Photoshoot photoshoot) {
        photoshootID = photoshoot.getId();
        addressTextView.setText(photoshoot.getProperty().getAddress());
        dateTextView.setText(DateHelper.getDateAsMonthDYHM(photoshoot.getDatetime()));

        fileTransfersRecycler.setLayoutManager(new GridLayoutManager(itemView.getContext(), 1));
            OrderedRealmCollection<ProgressStatusModel> models = RealmManager.getRealmInstance().where(ProgressStatusModel.class)
                    .equalTo("photoshootId",photoshootID)
                    .findAll().sort("id", Sort.DESCENDING);
            FileTransfersAdapter adapter = new FileTransfersAdapter(models);
            fileTransfersRecycler.setAdapter(adapter);

    }

    @OnClick(R.id.street_upload_button)
    void startUpload() {
        JfuerstApplication.INSTANCE.getRouter().navigateTo(Screens.START_UPLOAD_FRAGMENT, photoshootID);
    }

}
