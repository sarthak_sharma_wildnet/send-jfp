package com.volacious.jf.util.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.R;
import com.volacious.jf.util.viewholder.UploadingFileViewHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UploadingFilesAdapter extends RecyclerView.Adapter {

    List<FileStatus> files;

    public UploadingFilesAdapter(List<FileStatus> files) {
        this.files = files;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.uploading_file_viewholder, viewGroup, false);
        return new UploadingFileViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ((UploadingFileViewHolder) viewHolder).bind(files.get(i));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, @NonNull List payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public int getItemCount() {
        return files.size();
    }

    public void updateData(List<FileStatus> newFiles) {
        if (newFiles.size() != files.size()) {
            files = newFiles;
            notifyDataSetChanged();
        } else {
            for (int i = 0; i < files.size(); i++) {
                if (!files.get(i).equals(newFiles.get(i))) {
                    files.set(i, newFiles.get(i));
                    notifyItemChanged(i);
                }
            }
        }
    }
}
