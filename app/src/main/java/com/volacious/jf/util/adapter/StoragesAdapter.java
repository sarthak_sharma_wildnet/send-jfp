package com.volacious.jf.util.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.R;
import com.volacious.jf.util.adapter.storages.DocumentStorage;
import com.volacious.jf.util.adapter.storages.UploadStorage;
import com.volacious.jf.util.callback.StorageSelectedCallback;
import com.volacious.jf.util.viewholder.StorageViewHolder;

import java.util.List;

public class StoragesAdapter extends RecyclerView.Adapter implements StorageSelectedCallback {

    private List<UploadStorage> storages;
    private UploadStorage selectedStorage;

    public StoragesAdapter(List<UploadStorage> storages) {
        this.storages = storages;
        for (UploadStorage storage : storages) {
            if (storage instanceof DocumentStorage) {
                selectedStorage = storage;
            }
        }
        if (selectedStorage == null && storages.size() > 0) {
            selectedStorage = storages.get(0);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.path_viewholder, viewGroup, false);
        StorageViewHolder storageViewHolder = new StorageViewHolder(v);
        storageViewHolder.setStorageSelectedCallback(this);
        return storageViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ((StorageViewHolder) viewHolder).bind(storages.get(i), selectedStorage);
    }

    @Override
    public int getItemCount() {
        return storages.size();
    }

    @Override
    public void storageSelected(UploadStorage storage) {
        selectedStorage = storage;
        notifyDataSetChanged();
    }

    public UploadStorage getSelectedStorage() {
        return selectedStorage;
    }
}
