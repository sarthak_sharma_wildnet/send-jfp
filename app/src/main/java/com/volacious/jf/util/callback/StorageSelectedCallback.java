package com.volacious.jf.util.callback;

import com.volacious.jf.util.adapter.storages.UploadStorage;

public interface StorageSelectedCallback {
    void storageSelected(UploadStorage storage);
}
