package com.volacious.jf.util.adapter.storages;

import com.github.mjdev.libaums.UsbMassStorageDevice;
import com.volacious.jf.engine.copyservice.processors.copy.FileInterface;
import com.volacious.jf.engine.usb.USBHelper;

import java.util.List;

import androidx.annotation.Nullable;

public class USBStorage implements UploadStorage {
    private UsbMassStorageDevice device;
    private List<FileInterface> sourceFiles;
    private long length;


    public USBStorage(UsbMassStorageDevice device) {
        this.device = device;
        sourceFiles = USBHelper.getFilesList(device);
        length = USBHelper.getFilesSize(device);
    }

    @Override
    public long getLength() {
        return length;
    }

    @Override
    public List<FileInterface> getSourceFiles() {
        return sourceFiles;
    }

    @Override
    public String getName() {
        return device.getUsbDevice().getDeviceName();
    }

    public UsbMassStorageDevice getDevice() {
        return device;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof USBStorage) {
            USBStorage us = (USBStorage)obj;
            if (getDevice() == null && us.getDevice() != null) {
                return false;
            }
            return getDevice().equals(us.getDevice());
        }
        return false;
    }
}
