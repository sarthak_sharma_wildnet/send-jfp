package com.volacious.jf.util.childReplace;

import androidx.fragment.app.FragmentManager;

import ru.terrakok.cicerone.commands.Replace;

public class ChildReplace extends Replace {

    private FragmentManager childFragmentManager;

    /**
     * Creates a {@link ChildReplace} navigation command.
     *
     * @param screenKey      screen key
     * @param transitionData initial data
     */

    public ChildReplace(String screenKey,Object transitionData, FragmentManager childFragmentManager){
        super(screenKey, transitionData);
        this.childFragmentManager = childFragmentManager;
    }

    public FragmentManager getChildFragmentManager() { return childFragmentManager; }
}