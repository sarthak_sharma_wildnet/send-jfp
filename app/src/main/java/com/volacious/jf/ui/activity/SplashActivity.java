package com.volacious.jf.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.volacious.jf.R;
import com.volacious.jf.helpers.PreferencesHelper;
import com.volacious.jf.presentation.router.base.Screens;

import io.realm.Realm;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        PreferencesHelper.saveString(PreferencesHelper.LAST_MAIN_FRAGMENT, Screens.TODO_FRAGMENT);

        if (!PreferencesHelper.getString(PreferencesHelper.PASSWORD).isEmpty()
                && !PreferencesHelper.getString(PreferencesHelper.USERNAME).isEmpty()) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

    }
}
