package com.volacious.jf.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.Constraints;
import com.volacious.jf.R;
import com.volacious.jf.engine.APIManager;
import com.volacious.jf.engine.EnsureLoginCallback;
import com.volacious.jf.engine.SafeResponseCallback;
import com.volacious.jf.engine.network.result.Photoshoot;
import com.volacious.jf.engine.network.result.User;
import com.volacious.jf.engine.realm.RealmManager;
import com.volacious.jf.engine.realm.models.ProgressStatusModel;
import com.volacious.jf.ui.activity.MainActivity;
import com.volacious.jf.ui.fragment.base.BaseFragment;
import com.volacious.jf.util.adapter.StartedPhotoshootsAdapter;
import com.volacious.jf.util.toast.ToastManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class FileTransfersFragment extends BaseFragment {

    public static FileTransfersFragment newInstance() {
        return new FileTransfersFragment();
    }

    @BindView(R.id.file_transfers_photoshoots_recyclerview)
    RecyclerView fileTransfersRecycler;

    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.file_transfers_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);
        ((MainActivity) getActivity()).selectedBottomButton(Constraints.FILE_TRANSFERS_BUTTON);
        load();
        setUsername(v);
        return v;
    }

    private void load() {
        showIndicator(true);
        APIManager.ensureLogin(new EnsureLoginCallback() {
            @Override
            public void loginSuccess(User user) {
                APIManager.retrofitService.getPhotoshoots(user.getId())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SafeResponseCallback<ArrayList<Photoshoot>>() {
                            @Override
                            public void onError(Throwable e) {
                                showIndicator(false);
                                ToastManager.getInstance().showError("Unable to get photoshoots!");
                            }

                            @Override
                            public void onNext(ArrayList<Photoshoot> photoshoots) {
                                super.onNext(photoshoots);

                                List<Photoshoot> res = new ArrayList<>();

                                for (Photoshoot photoshoot : photoshoots) {
                                    if (RealmManager.getRealmInstance().where(ProgressStatusModel.class)
                                            .equalTo("photoshootId", photoshoot.getId())
                                            .count() > 0) {
                                        res.add(photoshoot);
                                    }
                                }

                                res.sort((o1, o2) -> o2.getId() - o1.getId());

                                showIndicator(false);
                                StartedPhotoshootsAdapter adapter = new StartedPhotoshootsAdapter(res);
                                fileTransfersRecycler.setLayoutManager(new GridLayoutManager(getContext(), 1));
                                fileTransfersRecycler.setAdapter(adapter);
                            }
                        });
            }

            @Override
            public void loginFailed() {
                showIndicator(false);
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
