package com.volacious.jf.ui.activity.base;

import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.volacious.jf.JfuerstApplication;
import com.volacious.jf.R;
import com.volacious.jf.ui.activity.MainActivity;

public class BaseActivity extends AppCompatActivity {

    public void showIndicator(boolean visible) {
        View m_progressBar = findViewById(R.id.progress_bar_layout);
        if (m_progressBar != null) {
            if (visible)
                m_progressBar.setVisibility(View.VISIBLE);
            else
                m_progressBar.setVisibility(View.GONE);
        }
    }
}
