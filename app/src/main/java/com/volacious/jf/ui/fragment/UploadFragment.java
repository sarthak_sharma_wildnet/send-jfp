package com.volacious.jf.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.R;
import com.volacious.jf.engine.APIManager;
import com.volacious.jf.engine.EnsureLoginCallback;
import com.volacious.jf.engine.network.result.Photoshoot;
import com.volacious.jf.engine.network.result.User;
import com.volacious.jf.helpers.DateHelper;
import com.volacious.jf.ui.activity.base.BaseActivity;
import com.volacious.jf.ui.fragment.base.BaseFragment;
import com.volacious.jf.util.adapter.PhotoshootsAdapter;
import com.volacious.jf.util.toast.ToastManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class UploadFragment extends BaseFragment {

    public static UploadFragment newInstance() {
        return new UploadFragment();
    }

    Unbinder unbinder;

    @BindView(R.id.upload_photoshoots_recyclerview)
    RecyclerView photoshootsRecycler;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.upload_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);
        load();
        setUsername(v);
        return v;
    }

    private void load() {
        showIndicator(true);
        APIManager.ensureLogin(new EnsureLoginCallback() {
            @Override
            public void loginSuccess(User user) {
                APIManager.retrofitService.getPhotoshoots(user.getId())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<ArrayList<Photoshoot>>() {
                            @Override
                            public void onCompleted() {
                            }

                            @Override
                            public void onError(Throwable e) {
                                showIndicator(false);
                                ToastManager.getInstance().showError("Error!");
                            }

                            @Override
                            public void onNext(ArrayList<Photoshoot> photoshoots) {
                                photoshoots.sort((o1, o2) -> o2.getDatetime().compareTo(o1.getDatetime()));
                                photoshoots.removeIf(photoshoot -> !DateHelper.isToday(photoshoot.getDatetime()));
                                showIndicator(false);
                                PhotoshootsAdapter adapter = new PhotoshootsAdapter(photoshoots);
                                photoshootsRecycler.setLayoutManager(new GridLayoutManager(getContext(), 1));
                                photoshootsRecycler.setAdapter(adapter);
                            }
                        });
            }

            @Override
            public void loginFailed() {
                showIndicator(false);
            }
        });
    }

}
