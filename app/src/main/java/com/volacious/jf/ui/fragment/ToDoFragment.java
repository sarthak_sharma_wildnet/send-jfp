package com.volacious.jf.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.Constraints;
import com.volacious.jf.R;
import com.volacious.jf.engine.APIManager;
import com.volacious.jf.engine.EnsureLoginCallback;
import com.volacious.jf.engine.SafeResponseCallback;
import com.volacious.jf.engine.network.result.FileTransfer;
import com.volacious.jf.engine.network.result.Photoshoot;
import com.volacious.jf.engine.network.result.User;
import com.volacious.jf.engine.realm.RealmManager;
import com.volacious.jf.engine.realm.models.ProgressStatusModel;
import com.volacious.jf.helpers.DateHelper;
import com.volacious.jf.ui.activity.MainActivity;
import com.volacious.jf.ui.activity.base.BaseActivity;
import com.volacious.jf.ui.fragment.base.BaseFragment;
import com.volacious.jf.util.adapter.PhotoshootsAdapter;
import com.volacious.jf.util.toast.ToastManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.RealmResults;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ToDoFragment extends BaseFragment {

    public static ToDoFragment newInstance() {
        return new ToDoFragment();
    }

    @BindView(R.id.top_todo_textView)
    TextView currentDateTextView;

    @BindView(R.id.todo_photoshoots_recyclerview)
    RecyclerView todoRecycler;

    @BindView(R.id.middle_todo_no_tasks)
    TextView noTasksTextView;

    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.todo_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);
        currentDateTextView.setText(DateHelper.getDateAsMonthDYHM(null));
        ((MainActivity) getActivity()).selectedBottomButton(Constraints.TO_DO_BUTTON);
        load();
        setUsername(v);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void load() {
        showIndicator(true);
        APIManager.ensureLogin(new EnsureLoginCallback() {
            @Override
            public void loginSuccess(User user) {
                APIManager.retrofitService.getPhotoshoots(user.getId())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SafeResponseCallback<ArrayList<Photoshoot>>() {
                            @Override
                            public void onError(Throwable e) {
                                showIndicator(false);
                                ToastManager.getInstance().showError("Unable to get photoshoots!");
                            }

                            @Override
                            public void onNext(ArrayList<Photoshoot> photoshoots) {
                                super.onNext(photoshoots);

                                photoshoots.removeIf(photoshoot -> {
                                    RealmResults<ProgressStatusModel> progressModels = RealmManager.getRealmInstance().where(ProgressStatusModel.class).equalTo("photoshootId", photoshoot.getId()).findAll();
                                    if (progressModels.size() > 0) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                });

                                photoshoots.sort((o1, o2) -> o2.getDatetime().compareTo(o1.getDatetime()));
                                showIndicator(false);
                                noTasksTextView.setVisibility(photoshoots.size() > 0 ? View.GONE : View.VISIBLE);
                                PhotoshootsAdapter adapter = new PhotoshootsAdapter(photoshoots);
                                todoRecycler.setLayoutManager(new GridLayoutManager(getContext(), 1));
                                todoRecycler.setAdapter(adapter);

                            }
                        });
            }

            @Override
            public void loginFailed() {
                showIndicator(false);
            }
        });
    }

}
