package com.volacious.jf.ui.fragment.base;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.volacious.jf.R;
import com.volacious.jf.engine.realm.DataSourceDisposable;
import com.volacious.jf.helpers.PreferencesHelper;
import com.volacious.jf.ui.activity.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class BaseFragment extends Fragment {

    private List<DataSourceDisposable> dataSources = new ArrayList<>();

    public void showIndicator(boolean visible) {
        Activity activity = getActivity();
        if (activity instanceof BaseActivity) {
            ((BaseActivity) activity).showIndicator(visible);
        }
    }

    public void setUsername(View view) {
        try {
            ((TextView) view.findViewById(R.id.username))
                    .setText("(" + PreferencesHelper.getString(PreferencesHelper.USERNAME) + ")");
        } catch (Exception ignore) {
        }
    }

    private void dispose() {
        DataSourceDisposable[] ds = dataSources.toArray(new DataSourceDisposable[0]);
        for (DataSourceDisposable d : ds) {
            d.dispose();
        }
        dataSources = new ArrayList<>();
    }

    protected void addDataSource(DataSourceDisposable dataSource) {
        dataSources.add(dataSource);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dispose();
    }
}
