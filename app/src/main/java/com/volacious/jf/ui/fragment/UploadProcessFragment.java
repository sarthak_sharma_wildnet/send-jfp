package com.volacious.jf.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.JfuerstApplication;
import com.volacious.jf.R;
import com.volacious.jf.engine.APIManager;
import com.volacious.jf.engine.EnsureLoginCallback;
import com.volacious.jf.engine.SafeResponseCallback;
import com.volacious.jf.engine.cache.CacheManager;
import com.volacious.jf.engine.copyservice.processors.copy.CopyProcessor;
import com.volacious.jf.engine.copyservice.processors.copy.ProcessStopEvent;
import com.volacious.jf.engine.copyservice.processors.upload.UploadProcessor;
import com.volacious.jf.engine.network.result.FileTransfer;
import com.volacious.jf.engine.network.result.Photoshoot;
import com.volacious.jf.engine.network.result.User;
import com.volacious.jf.engine.realm.RealmManager;
import com.volacious.jf.engine.realm.RealmResultsDataSource;
import com.volacious.jf.engine.realm.models.ProgressStatusModel;
import com.volacious.jf.helpers.DateHelper;
import com.volacious.jf.ui.fragment.base.BaseFragment;
import com.volacious.jf.ui.fragment.uploadprocess.UIUpdateEvent;
import com.volacious.jf.util.adapter.FileStatus;
import com.volacious.jf.util.adapter.UploadingFilesAdapter;
import com.volacious.jf.util.toast.ToastManager;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.RealmResults;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class UploadProcessFragment extends BaseFragment {

    public static final String FILE_TRANSFER_ID = "com.volacious.jf.ui.fragment.FILE_TRANSFER_ID";

    public static UploadProcessFragment newInstance(int fileTransferID) {
        Bundle args = new Bundle();
        args.putSerializable(FILE_TRANSFER_ID, fileTransferID);
        UploadProcessFragment fragment = new UploadProcessFragment();
        fragment.setArguments(args);
        return fragment;
    }

    Unbinder unbinder;

    @BindView(R.id.upload_process_photoshoot_place)
    TextView photoshootPlace;

    @BindView(R.id.upload_process_photoshoot_date)
    TextView photoshootDate;

    @BindView(R.id.upload_process_photoshoot_status)
    TextView uploadStatus;

    //Speed
    @BindView(R.id.upload_process_speed_status_image)
    ImageView speedImage;

    @BindView(R.id.upload_process_speed)
    TextView speed;

    //Copy
    @BindView(R.id.upload_process_copy_files_icon)
    ImageView copyIcon;

    @BindView(R.id.upload_process_copy_files_label)
    TextView copyLabel;

    @BindView(R.id.upload_process_copy_progress)
    ProgressBar copyProgress;

    //Upload
    @BindView(R.id.upload_process_upload_files_icon)
    ImageView uploadIcon;

    @BindView(R.id.upload_process_upload_files_label)
    TextView uploadLabel;

    @BindView(R.id.upload_process_upload_progress)
    ProgressBar uploadProgress;

    //Files
    @BindView(R.id.upload_process_files_status_text)
    TextView filesLabel;

    @BindView(R.id.upload_process_files_recycler)
    RecyclerView filesRecycler;

    @BindView(R.id.upload_process_files_layout)
    ConstraintLayout filesLayout;

    //Notes
    @BindView(R.id.upload_process_note_text)
    EditText notesText;

    @BindView(R.id.notes_textview)
    TextView notesLabel;

    @BindView(R.id.upload_process_note_text_layout)
    ScrollView notesContainer;

    @BindView(R.id.upload_process_status_progress)
    ProgressBar mainStatusProgress;

    @BindView(R.id.upload_process_resume_button)
    Button resumeButton;

    @BindView(R.id.copy_stop)
    Button stopCopyButton;

    private int fileTransferID = -1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.upload_process_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);
        //((MainActivity) getActivity()).moveToStart = true;
        init();
        setUsername(v);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
       final String notes = notesText.getText() == null ? "" : notesText.getText().toString();
        RealmManager.executeRealmTransactionAsync(realm -> {
            ProgressStatusModel progressModel = realm.where(ProgressStatusModel.class).equalTo("id", fileTransferID).findFirst();
            progressModel.setNoteText(notes);
        });
        APIManager.retrofitService.getFileTransfer(fileTransferID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SafeResponseCallback<FileTransfer>() {
                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(FileTransfer fileTransfer) {
                        fileTransfer.setPhotogNotes(notes);
                        fileTransfer.sendUpdate();
                    }
                });
        unbinder.unbind();
    }

    private void updateFileStatuses(List<FileStatus> files) {
        if (filesRecycler.getAdapter() != null) {
            ((UploadingFilesAdapter) filesRecycler.getAdapter()).updateData(files);
        } else {
            filesRecycler.setAdapter(new UploadingFilesAdapter(files));
        }
    }

    private void updateUI(UIUpdateEvent updateEvent) {
        mainStatusProgress.setVisibility(updateEvent.isLoadingIndicatorVisible() ? View.VISIBLE : View.GONE);

        stopCopyButton.setVisibility(updateEvent.isCopyStopVisible() ? View.VISIBLE : View.GONE);

        if (updateEvent.getCopyStatus() == 1 && fileTransferID == CopyProcessor.fileTransferInProgress) {
            //show button "stop copy"
            mainStatusProgress.setVisibility(View.VISIBLE);
        } else {
            if (updateEvent.getUploadStatus() == 1 && fileTransferID == UploadProcessor.fileTransferInProgress) {
                //show button "stop copy"
                mainStatusProgress.setVisibility(View.VISIBLE);
            } else {
                //hide button
                mainStatusProgress.setVisibility(View.GONE);
            }
        }

        if (updateEvent.getStatusLabel() != null) {
            uploadStatus.setText(updateEvent.getStatusLabel());
            filesLayout.setVisibility(View.VISIBLE);
            notesLabel.setVisibility(updateEvent.isUploadIconVisible() ? View.GONE : View.VISIBLE);
            notesContainer.setVisibility(updateEvent.isUploadIconVisible() ? View.INVISIBLE : View.VISIBLE);
            notesText.setVisibility(updateEvent.isUploadIconVisible() ? View.INVISIBLE : View.VISIBLE);


            if (updateEvent.getCopyLabel() != null) {
                copyLabel.setVisibility(View.VISIBLE);
                copyLabel.setText(updateEvent.getCopyLabel());
            } else {
                copyLabel.setVisibility(View.GONE);
            }
            copyIcon.setVisibility(updateEvent.isCopyIconVisible() ? View.VISIBLE : View.GONE);

            if (updateEvent.getUploadLabel() != null) {
                uploadLabel.setVisibility(View.VISIBLE);
                uploadLabel.setText(updateEvent.getUploadLabel());
                notesContainer.setVisibility(View.INVISIBLE);
                notesLabel.setVisibility(View.GONE);
            } else {
                uploadLabel.setVisibility(View.GONE);
            }
            uploadIcon.setVisibility(updateEvent.isUploadIconVisible() ? View.VISIBLE : View.GONE);

            if (updateEvent.getProgress() >= 0) {
                uploadProgress.setVisibility(View.VISIBLE);
                uploadProgress.setProgress(updateEvent.getProgress());
            } else {
                uploadProgress.setVisibility(View.GONE);
            }

            if (updateEvent.getSpeedLabel() != null) {
                speed.setVisibility(View.VISIBLE);
                speed.setText(updateEvent.getSpeedLabel());
            } else {
                speed.setVisibility(View.GONE);
            }
            speedImage.setVisibility(updateEvent.isSpeedIconVisible() ? View.VISIBLE : View.GONE);


            if (updateEvent.getFilesProgressStatus() != null) {
                filesLabel.setText(updateEvent.getFilesProgressStatus());
            }

            if (updateEvent.getFileStatuses() != null && updateEvent.getFileStatuses().size() > 0) {
                updateFileStatuses(updateEvent.getFileStatuses());
            }


        } else {
            uploadStatus.setText("Pending");

            notesContainer.setVisibility(View.VISIBLE);
            notesText.setVisibility(View.VISIBLE);
            notesLabel.setVisibility(View.VISIBLE);
//            if (updateEvent.getNoteText() != null && !updateEvent.getNoteText().isEmpty()) {
//                notesText.setText(updateEvent.getNoteText());
//            } else {
            notesText.setHint("Note not added");
//            }

            filesLayout.setVisibility(View.VISIBLE);
            filesRecycler.setVisibility(View.VISIBLE);
            filesLabel.setVisibility(View.VISIBLE);

            if (updateEvent.getFileStatuses() != null && updateEvent.getFileStatuses().size() > 0) {
                filesLabel.setText("Files (" + updateEvent.getFileStatuses().size() + ")");
                updateFileStatuses(updateEvent.getFileStatuses());
            } else {
                filesLabel.setText("No files");
            }

            copyIcon.setVisibility(View.VISIBLE);
            copyLabel.setVisibility(View.VISIBLE);
            copyLabel.setText("Copy not started");

            uploadIcon.setVisibility(View.VISIBLE);
            uploadLabel.setVisibility(View.VISIBLE);
            uploadLabel.setText("Upload not started");
        }

    }

    /*@Subscribe(threadMode = ThreadMode.MAIN)
    public void onUploadEvent(UploadProgress uploadEvent) {
        if (uploadEvent.getFileTransfer().getId() != fileTransferID) {
            return;
        }
        updateUI(uploadEvent.getUIUpdateEvent());
        checkResumeButton(uploadEvent.getFileTransfer());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUploadStopEvent(UploadStopEvent uploadStopEvent) {
        if (uploadStopEvent.getFileTransferId() == fileTransferID) {
            mainStatusProgress.setVisibility(View.GONE);
        }
        checkResumeButton(uploadStopEvent.getFileTransfer());
    }*/

    private void checkResumeButton() {
        if (UploadProcessor.inProgress() && UploadProcessor.fileTransferInProgress != fileTransferID) {
            resumeButton.setVisibility(View.VISIBLE);
            resumeButton.setText("Force upload");
        } else {
            resumeButton.setVisibility(View.GONE);
        }
    }

    private boolean uploadStatusAvailable = false;

    public void uploadStatusChanged(RealmResults<ProgressStatusModel> progressStatusModels) {
        if (progressStatusModels.size() > 0) {
            uploadStatusAvailable = true;
            updateUI(progressStatusModels.get(0).getUIUpdateEvent());
            checkResumeButton();
        }
    }

    void init() {
        showIndicator(true);
        filesRecycler.setLayoutManager(new GridLayoutManager(getContext(), 5, GridLayoutManager.HORIZONTAL, false));
        uploadProgress.setProgress(0);
        if (getArguments() != null) {
            fileTransferID = getArguments().getInt(FILE_TRANSFER_ID, -1);

            addDataSource(new RealmResultsDataSource<RealmResults<ProgressStatusModel>>()
                    .mainThread(true)
                    .onChange(this::uploadStatusChanged)
                    .observe((realm) -> realm.where(ProgressStatusModel.class).equalTo("id", fileTransferID).findAll()));


            APIManager.ensureLogin(new EnsureLoginCallback() {
                @Override
                public void loginSuccess(User user) {
                    APIManager.retrofitService.getFileTransfer(fileTransferID)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new SafeResponseCallback<FileTransfer>() {
                                @Override
                                public void onError(Throwable e) {
                                    showIndicator(false);
                                }

                                @Override
                                public void onNext(FileTransfer fileTransfer) {
                                    showIndicator(false);

                                    if (!uploadStatusAvailable) {
                                        //updateUI(fileTransfer.getUIUpdateEvent());
                                    }


                                    //SET NOTE
                                    notesText.setVisibility(View.VISIBLE);
                                    if (fileTransfer.getPhotogNotes() != null) {
                                        notesText.setText(fileTransfer.getPhotogNotes());
                                    } else {
                                        notesText.setText("");
                                    }
                                    Photoshoot photoshoot = CacheManager.getInstance().getObject(Photoshoot.class, fileTransfer.getPhotoshootId());
                                    if (photoshoot != null) {
                                        if (photoshoot.getProperty() != null) {
                                            photoshootPlace.setText(photoshoot.getProperty().getAddress());
                                        }
                                        if (photoshoot.getDatetime() != null) {
                                            photoshootDate.setText(DateHelper.getDateAsMonthDYHM(photoshoot.getDatetime()));
                                        }
                                    }
                                }
                            });
                }

                @Override
                public void loginFailed() {
                    showIndicator(false);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (fileTransferID == -1) {
            ToastManager.getInstance().showError("No photoshoot in cache");
            JfuerstApplication.INSTANCE.getRouter().exit();
        }
    }

    @OnClick(R.id.copy_stop)
    public void stopCopyPressed() {
        EventBus.getDefault().post(new ProcessStopEvent(fileTransferID));
    }

    @OnClick(R.id.upload_process_resume_button)
    public void resumePressed() {
        resumeButton.setVisibility(View.GONE);

        RealmManager.executeRealmTransactionSync(realm -> {
            ProgressStatusModel progressStatusModel = realm
                    .where(ProgressStatusModel.class)
                    .equalTo("id", fileTransferID)
                    .findFirst();
            if (progressStatusModel != null) {
                int max = realm.where(ProgressStatusModel.class).max("priority").intValue();
                progressStatusModel.setPriority(max + 1);
            }
        });

        if (UploadProcessor.inProgress()) {
            EventBus.getDefault().post(new ProcessStopEvent(UploadProcessor.fileTransferInProgress));
        }
    }
}
