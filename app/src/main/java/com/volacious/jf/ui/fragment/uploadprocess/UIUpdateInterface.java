package com.volacious.jf.ui.fragment.uploadprocess;

public interface UIUpdateInterface {
    UIUpdateEvent getUIUpdateEvent();
}
