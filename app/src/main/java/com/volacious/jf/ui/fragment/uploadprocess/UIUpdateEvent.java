package com.volacious.jf.ui.fragment.uploadprocess;

import com.volacious.jf.util.adapter.FileStatus;

import java.util.List;

public class UIUpdateEvent {

    private String mainUIStatus;

    private String copyLabel;
    private boolean copyIconVisible = false;

    private String uploadLabel;
    private boolean uploadIconVisible = false;

    private String speedLabel;
    private boolean speedIconVisible = false;

    private int progress = -1;

    private String statusLabel;

    private List<FileStatus> fileStatuses;

    private String filesProgressStatus;
    private String noteText;

    private boolean loadingIndicatorVisible = false;

    private int copyStatus = 0; // 0 - not started, 1 - in progress, 2 - completed
    private int uploadStatus = 0; // 0 - not started, 1 - in progress, 2 - completed

    private boolean copyStopVisible = false;

    private String resumeButtonText = null;

    public UIUpdateEvent() {
    }

    public String getCopyLabel() {
        return copyLabel;
    }

    public void setCopyLabel(String copyLabel) {
        this.copyLabel = copyLabel;
    }

    public String getUploadLabel() {
        return uploadLabel;
    }

    public void setUploadLabel(String uploadLabel) {
        this.uploadLabel = uploadLabel;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public List<FileStatus> getFileStatuses() {
        return fileStatuses;
    }

    public void setFileStatuses(List<FileStatus> fileStatuses) {
        this.fileStatuses = fileStatuses;
    }

    public boolean isCopyIconVisible() {
        return copyIconVisible;
    }

    public void setCopyIconVisible(boolean copyIconVisible) {
        this.copyIconVisible = copyIconVisible;
    }

    public String getMainUIStatus() {
        return mainUIStatus;
    }

    public void setMainUIStatus(String mainUIStatus) {
        this.mainUIStatus = mainUIStatus;
    }

    public boolean isUploadIconVisible() {
        return uploadIconVisible;
    }

    public void setUploadIconVisible(boolean uploadIconVisible) {
        this.uploadIconVisible = uploadIconVisible;
    }

    public String getSpeedLabel() {
        return speedLabel;
    }

    public void setSpeedLabel(String speedLabel) {
        this.speedLabel = speedLabel;
    }

    public boolean isSpeedIconVisible() {
        return speedIconVisible;
    }

    public void setSpeedIconVisible(boolean speedIconVisible) {
        this.speedIconVisible = speedIconVisible;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public String getFilesProgressStatus() {
        return filesProgressStatus;
    }

    public void setFilesProgressStatus(String filesProgressStatus) {
        this.filesProgressStatus = filesProgressStatus;
    }

    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }

    public boolean isLoadingIndicatorVisible() {
        return loadingIndicatorVisible;
    }

    public void setLoadingIndicatorVisible(boolean loadingIndicatorVisible) {
        this.loadingIndicatorVisible = loadingIndicatorVisible;
    }

    public int getCopyStatus() {
        return copyStatus;
    }

    public void setCopyStatus(int copyStatus) {
        this.copyStatus = copyStatus;
    }

    public int getUploadStatus() {
        return uploadStatus;
    }

    public void setUploadStatus(int uploadStatus) {
        this.uploadStatus = uploadStatus;
    }

    public boolean isCopyStopVisible() {
        return copyStopVisible;
    }

    public void setCopyStopVisible(boolean copyStopVisible) {
        this.copyStopVisible = copyStopVisible;
    }

    public String getResumeButtonText() {
        return resumeButtonText;
    }

    public void setResumeButtonText(String resumeButtonText) {
        this.resumeButtonText = resumeButtonText;
    }
}
