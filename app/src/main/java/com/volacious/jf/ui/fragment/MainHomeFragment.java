package com.volacious.jf.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.volacious.jf.R;
import com.volacious.jf.engine.APIManager;
import com.volacious.jf.engine.EnsureLoginCallback;
import com.volacious.jf.engine.cache.CacheManager;
import com.volacious.jf.engine.copyservice.CopyUploadService;
import com.volacious.jf.engine.network.result.Photoshoot;
import com.volacious.jf.engine.network.result.User;
import com.volacious.jf.helpers.DateHelper;
import com.volacious.jf.presentation.router.base.Screens;
import com.volacious.jf.ui.activity.base.BaseActivity;
import com.volacious.jf.ui.fragment.base.BaseFragment;
import com.volacious.jf.util.ConstraintLayoutHelper;
import com.volacious.jf.util.adapter.PhotoshootsAdapter;
import com.volacious.jf.util.toast.ToastManager;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainHomeFragment extends BaseFragment {

    public static MainHomeFragment newInstance() {
        return new MainHomeFragment();
    }

    @BindView(R.id.main_home_date_textview)
    TextView dateTextView;

    @BindView(R.id.main_home_today)
    TextView todayTextView;

    @BindView(R.id.history_streets_recyclerview)
    RecyclerView photoshootsRecycler;

    @BindView(R.id.upload_streets_recyclerview)
    RecyclerView todayPhotoshootsRecycler;

    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.main_home_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);
        init();
        return v;
    }

    void init() {
        dateTextView.setText(DateHelper.getDateAsMMDDYYYY(new Date()));
        load();
    }

    @OnClick(R.id.stopForegroundService)
    public void stopService(View v) {
        Intent serviceIntent = new Intent(getActivity(), CopyUploadService.class);
        getActivity().stopService(serviceIntent);
    }

    private void load() {
        showIndicator(true);
        APIManager.ensureLogin(new EnsureLoginCallback() {
            @Override
            public void loginSuccess(User user) {
                APIManager.retrofitService.getPhotoshoots(user.getId())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<ArrayList<Photoshoot>>() {
                            @Override
                            public void onCompleted() {
                            }

                            @Override
                            public void onError(Throwable e) {
                                showIndicator(false);
                                ToastManager.getInstance().showError("Error!");
                            }

                            @Override
                            public void onNext(ArrayList<Photoshoot> photoshoots) {
                                photoshoots.removeIf(photoshoot -> DateHelper.isToday(photoshoot.getDatetime()));
                                int newHeight = getPixels(photoshoots.size() * 100);
                                ConstraintLayoutHelper.updateConstraintSet(photoshootsRecycler, (set, layout, view1) ->
                                        set.constrainHeight(photoshootsRecycler.getId(), newHeight));

                                CacheManager.getInstance().cacheObjects(photoshoots);
                                photoshoots.sort((o1, o2) -> o2.getDatetime().compareTo(o1.getDatetime()));
                                showIndicator(false);
                                PhotoshootsAdapter adapter = new PhotoshootsAdapter(photoshoots);
                                photoshootsRecycler.setLayoutManager(new GridLayoutManager(getContext(), 1) {
                                    @Override
                                    public boolean canScrollVertically() {
                                        return false;
                                    }
                                });
                                photoshootsRecycler.setAdapter(adapter);
                            }
                        });
            }

            @Override
            public void loginFailed() {
                showIndicator(false);
            }
        });

        showIndicator(true);
        APIManager.ensureLogin(new EnsureLoginCallback() {
            @Override
            public void loginSuccess(User user) {
                APIManager.retrofitService.getPhotoshoots(user.getId())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<ArrayList<Photoshoot>>() {
                            @Override
                            public void onCompleted() {
                            }

                            @Override
                            public void onError(Throwable e) {
                                showIndicator(false);
                                ToastManager.getInstance().showError("Error!");
                            }

                            @Override
                            public void onNext(ArrayList<Photoshoot> photoshoots) {
                                photoshoots.removeIf(photoshoot -> !DateHelper.isToday(photoshoot.getDatetime()));
                                int newHeight = getPixels(photoshoots.size() * 100);
                                ConstraintLayoutHelper.updateConstraintSet(todayPhotoshootsRecycler, (set, layout, view1) ->
                                        set.constrainHeight(todayPhotoshootsRecycler.getId(), newHeight));
                                CacheManager.getInstance().cacheObjects(photoshoots);
                                photoshoots.sort((o1, o2) -> o2.getDatetime().compareTo(o1.getDatetime()));
                                showIndicator(false);
                                PhotoshootsAdapter adapter = new PhotoshootsAdapter(photoshoots);
                                todayPhotoshootsRecycler.setLayoutManager(new GridLayoutManager(getContext(), 1) {
                                    @Override
                                    public boolean canScrollVertically() {
                                        return false;
                                    }
                                });
                                todayPhotoshootsRecycler.setAdapter(adapter);
                            }
                        });
            }

            @Override
            public void loginFailed() {
                showIndicator(false);
            }
        });
    }

    private static Float scale = null;

    public float getScale() {
        if (scale == null) {
            scale = getContext().getResources().getDisplayMetrics().density;
        }
        return scale;
    }

    public int getPixels(int dp) {
        return (int) (dp * getScale() + 0.5f);
    }

    public int getDP(int pixels) {
        return (int) (pixels / getScale() + 0.5f);
    }

}
