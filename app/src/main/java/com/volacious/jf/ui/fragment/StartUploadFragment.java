package com.volacious.jf.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.documentfile.provider.DocumentFile;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mjdev.libaums.UsbMassStorageDevice;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.volacious.jf.JfuerstApplication;
import com.volacious.jf.R;
import com.volacious.jf.engine.APIManager;
import com.volacious.jf.engine.EnsureLoginCallback;
import com.volacious.jf.engine.SafeResponseCallback;
import com.volacious.jf.engine.copyservice.DataTransitionManager;
import com.volacious.jf.engine.copyservice.processors.copy.FileInterface;
import com.volacious.jf.engine.network.result.User;
import com.volacious.jf.engine.realm.RealmManager;
import com.volacious.jf.engine.realm.models.ProgressStatusModel;
import com.volacious.jf.engine.sdcardreceiver.SDCardEvent;
import com.volacious.jf.engine.cache.CacheManager;
import com.volacious.jf.engine.copyservice.CopyUploadService;
import com.volacious.jf.engine.copyservice.processors.FileUtils;
import com.volacious.jf.engine.copyservice.processors.copy.CopyProcessor;
import com.volacious.jf.engine.copyservice.processors.copy.CopyProgress;
import com.volacious.jf.engine.network.result.FileTransfer;
import com.volacious.jf.engine.network.result.Photoshoot;
import com.volacious.jf.engine.usb.USBEvent;
import com.volacious.jf.engine.usb.USBHelper;
import com.volacious.jf.helpers.DateHelper;
import com.volacious.jf.presentation.router.base.Screens;
import com.volacious.jf.ui.fragment.base.BaseFragment;
import com.volacious.jf.util.adapter.StoragesAdapter;
import com.volacious.jf.util.adapter.storages.DocumentStorage;
import com.volacious.jf.util.adapter.storages.PathStorage;
import com.volacious.jf.util.adapter.storages.USBStorage;
import com.volacious.jf.util.adapter.storages.UploadStorage;
import com.volacious.jf.util.toast.ToastManager;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.RealmResults;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class StartUploadFragment extends BaseFragment {

    public static final String PHOTOSHOOT_ID = "com.volacious.jf.ui.fragment.FILE_TRANSFER_ID";

    public static StartUploadFragment newInstance(int photoshootID) {
        Bundle args = new Bundle();
        args.putSerializable(PHOTOSHOOT_ID, photoshootID);
        StartUploadFragment fragment = new StartUploadFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.start_upload_button)
    Button startUploadButton;

    @BindView(R.id.start_upload_path_recycler)
    RecyclerView pathRecycler;

    @BindView(R.id.start_upload_photoshoot_date)
    TextView photoshootDate;

    @BindView(R.id.start_upload_photoshoot_place)
    TextView photoshootPlace;

    @BindView(R.id.start_upload_photoshoot_status)
    TextView photoshootStatus;

    @BindView(R.id.start_upload_note_text)
    EditText notesText;

    Unbinder unbinder;

    private StoragesAdapter storagesAdapter;

    private int photoshootID;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.start_upload_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);
        init();
        setUsername(v);
        return v;
    }

    void init() {
        if (getArguments() != null) {
            photoshootID = getArguments().getInt(PHOTOSHOOT_ID);
            Photoshoot photoshoot = CacheManager.getInstance().getObject(Photoshoot.class, photoshootID);
            if (photoshoot != null) {
                if (photoshoot.getProperty() != null) {
                    photoshootPlace.setText(photoshoot.getProperty().getAddress());
                }
                if (photoshoot.getDatetime() != null) {
                    photoshootDate.setText(DateHelper.getDateAsMonthDYHM(photoshoot.getDatetime()));
                }

                RealmResults<ProgressStatusModel> transfers =  RealmManager.getRealmInstance().where(ProgressStatusModel.class).equalTo("photoshootId",photoshootID).findAll();
                if (transfers.size() > 0) {
                    int succedTransfers = transfers.where().equalTo("isSuccessful",true).findAll().size();
                    photoshootStatus.setText("transfers: " + transfers.size() + ", successfull: " + succedTransfers);
                } else {
                    photoshootStatus.setText("transfers not found");
                }
            } else {
                ToastManager.getInstance().showError("No photoshoot in cache");
                JfuerstApplication.INSTANCE.getRouter().exit();
            }
        }
        fillPaths();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private DocumentFile selectedDocumentFile;

    @OnClick(R.id.start_upload_choose_path)
    public void choosePressed() {
        Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        startActivityForResult(Intent.createChooser(i, "Choose directory"), 9999);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case 9999:
                if (data != null && data.getData() != null && getActivity() != null) {
                    Uri uri = data.getData();
                    selectedDocumentFile = DocumentFile.fromTreeUri(getActivity(), uri);
                    fillPaths();
                } else {
                    ToastManager.getInstance().showSuccess("Nothing selected");
                }
                break;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        unbinder.unbind();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSDCardEvent(SDCardEvent sdCardEvent) {
        showIndicator(true);
        fillPaths();
    }

    private RxPermissions rxPermissions;

    @SuppressLint("CheckResult")
    void fillPaths() {
        pathRecycler.setLayoutManager(new GridLayoutManager(getContext(), 1));
        rxPermissions = new RxPermissions(getActivity());
        rxPermissions
                .request(Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                    if (granted) {
                        List<UploadStorage> storages = new ArrayList<>();

                        //TODO: COMMENT THIS
                        for (String s : FileUtils.getExtSdCardDataPathsDCIM()) {
                            storages.add(new PathStorage(s));
                        }

                        UsbMassStorageDevice device = USBHelper.getInstance().getAvailableDevice();
                        if (device != null) {
                            storages.add(new USBStorage(device));
                        }

                        if (selectedDocumentFile != null) {
                            storages.add(new DocumentStorage(selectedDocumentFile, getActivity()));
                        }

                        storagesAdapter = new StoragesAdapter(storages);
                        pathRecycler.setAdapter(storagesAdapter);
                    }
                });
        showIndicator(false);
    }

    @OnClick(R.id.start_upload_button)
    void onStartUpload() {
        if (CopyProcessor.inProgress()) {
            ToastManager.getInstance().showError("Unable to start while other copy in progress.");
            return;
        }

        UploadStorage selectedStorage = storagesAdapter.getSelectedStorage();

        if (selectedStorage == null) {
            ToastManager.getInstance().showError("No storage selected.");
            return;
        }

        File appDir = getActivity().getFilesDir();
        long availableSpace = FileUtils.checkAvailableSpace(appDir);
        long requiredSpace = selectedStorage.getLength();

        if (requiredSpace >= availableSpace) {
            ToastManager.getInstance().showError("Unable to start copy. Not enough space.");
            return;
        }

        createFileTransfer(selectedStorage);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUSBEvent(USBEvent usbEvent) {
        fillPaths();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCopyEvent(CopyProgress copyProgress) {
        showIndicator(false);
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        JfuerstApplication.INSTANCE.getRouter().replaceScreen(Screens.UPLOAD_PROCESS_FRAGMENT, copyProgress.getFileTransfer().getId());
        //JfuerstApplication.INSTANCE.getRouter().navigateTo(Screens.UPLOAD_PROCESS_FRAGMENT, copyProgress.getFileTransfer().getId());
    }

    private void createFileTransfer(UploadStorage uploadStorage) {
        showIndicator(true);

        if (uploadStorage.getSourceFiles().size() > 0) {
            List<Map<String, Object>> manifests = new ArrayList<>();
            for (FileInterface file : uploadStorage.getSourceFiles()) {
                Map<String, Object> manifest = new HashMap<>();
                manifest.put("filename", file.getName());
                manifest.put("size", file.length());
                manifests.add(manifest);
            }

            Map<String, Object> parameters = new HashMap<>();
            parameters.put("photogNotes", notesText.getText() != null ? notesText.getText().toString() : "");
            parameters.put("manifest", manifests);

            APIManager.ensureLogin(new EnsureLoginCallback() {
                @Override
                public void loginSuccess(User user) {
                    APIManager.retrofitService.createFileTransfer(photoshootID, parameters)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new SafeResponseCallback<FileTransfer>() {
                                @Override
                                public void onError(Throwable e) {
                                    ToastManager.getInstance().showError("Unable to create FileTransfer");
                                    showIndicator(false);
                                }

                                @Override
                                public void onNext(FileTransfer fileTransfer) {
                                    super.onNext(fileTransfer);
                                    DataTransitionManager.getInstance().putData(DataTransitionManager.UPLOAD_STORAGE, uploadStorage);
                                    showIndicator(false);
                                    Intent serviceIntent = new Intent(getActivity(), CopyUploadService.class);
                                    serviceIntent.putExtra(CopyUploadService.FILE_TRANSFER, fileTransfer);
                                    ContextCompat.startForegroundService(getActivity(), serviceIntent);
                                }
                            });
                }

                @Override
                public void loginFailed() {
                    ToastManager.getInstance().showError("Login failed. Please relogin.");
                    showIndicator(false);
                }
            });
        }
    }
}
