package com.volacious.jf.ui.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.volacious.jf.Constraints;
import com.volacious.jf.JfuerstApplication;
import com.volacious.jf.R;
import com.volacious.jf.engine.copyservice.CopyUploadService;
import com.volacious.jf.engine.copyservice.processors.ToastEvent;
import com.volacious.jf.engine.usb.USBContextHolder;
import com.volacious.jf.engine.usb.USBHelper;
import com.volacious.jf.helpers.PreferencesHelper;
import com.volacious.jf.presentation.navigator.MainActivityNavigator;
import com.volacious.jf.presentation.router.base.Screens;
import com.volacious.jf.ui.activity.base.BaseActivity;
import com.volacious.jf.util.toast.ToastManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MainActivity extends BaseActivity implements USBContextHolder {

    private static final String MAIN_START_SCREEN_KEY = "mainStartScreenKey";

    private MainActivityNavigator navigator;

    public static Intent newIntent(Context context, @Nullable String mainStartScreenKey) {
        Intent intent = new Intent(context, MainActivity.class);
        if (mainStartScreenKey != null) {
            Bundle extras = new Bundle();
            extras.putString(MAIN_START_SCREEN_KEY, mainStartScreenKey);
            intent.putExtras(extras);
        }
        return intent;
    }

    @Override
    public Context getUSBContext() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigator = new MainActivityNavigator(this, getSupportFragmentManager(), R.id.container_fragment);

        JfuerstApplication.INSTANCE.getRouter().replaceScreen(Screens.MAIN_FRAGMENT);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        USBHelper.initUSBHelper(this);

        IntentFilter filter = new IntentFilter(USBHelper.ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(USBHelper.getInstance().getUsbReceiver(), filter);

        if (!serviceIsRunningInForeground(this, CopyUploadService.class)) {
            Intent serviceIntent = new Intent(this, CopyUploadService.class);
            ContextCompat.startForegroundService(this, serviceIntent);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onToastEvent(ToastEvent toastEvent) {
        if (toastEvent.getError() != null) {
            ToastManager.getInstance().showError(toastEvent.getError());
        } else if (toastEvent.getStatus() != null) {
            ToastManager.getInstance().showSuccess(toastEvent.getStatus());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(USBHelper.getInstance().getUsbReceiver());
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        JfuerstApplication.INSTANCE.getNavigatorHolder().setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        super.onPause();
        JfuerstApplication.INSTANCE.getNavigatorHolder().removeNavigator();
    }

    public void selectedBottomButton(String selectedButton) {
        switch (selectedButton) {
            case Constraints.TO_DO_BUTTON: {
                PreferencesHelper.saveString(PreferencesHelper.LAST_MAIN_FRAGMENT, Screens.TODO_FRAGMENT);
                isToDoButtonActive(true);
                isFileTransfersButtonActive(false);
                break;
            }
            case Constraints.FILE_TRANSFERS_BUTTON: {
                isToDoButtonActive(false);
                PreferencesHelper.saveString(PreferencesHelper.LAST_MAIN_FRAGMENT, Screens.FILE_TRANSFERS_FRAGMENT);
                isFileTransfersButtonActive(true);
                break;
            }
        }
    }

    void isToDoButtonActive(boolean isActive) {
        View todo = findViewById(R.id.toDoIndicator);
        ImageView todoImg = findViewById(R.id.toDoImg);
        if (todo != null && todoImg != null) {
            if (isActive) {
                todo.setVisibility(View.VISIBLE);
                todoImg.setImageResource(R.drawable.ic_home_unselected);
            } else {
                todo.setVisibility(View.INVISIBLE);
                todoImg.setImageResource(R.drawable.ic_home_selected);
            }
        }
    }

    void isFileTransfersButtonActive(boolean isActive) {
        View fileTransfersIndicator = findViewById(R.id.fileTransfersIndicator);
        View fileTransfersImg = findViewById(R.id.fileTransfersImg);
        if (fileTransfersIndicator != null && fileTransfersImg != null) {
            if (isActive) {
                fileTransfersIndicator.setVisibility(View.VISIBLE);
                ((ImageView) fileTransfersImg).setImageResource(R.drawable.cloud_upload_alt_gray);
            } else {
                fileTransfersIndicator.setVisibility(View.INVISIBLE);
                ((ImageView) fileTransfersImg).setImageResource(R.drawable.cloud_upload_alt_solid);
            }
        }
    }

    /**
     * Returns true if this is a foreground service.
     *
     * @param context The {@link Context}.
     */
    public boolean serviceIsRunningInForeground(Context context, Class serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }

    public void goToLoginActivity() {
        PreferencesHelper.removeString(PreferencesHelper.PASSWORD);
        PreferencesHelper.removeString(PreferencesHelper.USERNAME);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
