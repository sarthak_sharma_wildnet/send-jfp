package com.volacious.jf.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.volacious.jf.JfuerstApplication;
import com.volacious.jf.R;
import com.volacious.jf.presentation.navigator.LoginActivityNavigator;
import com.volacious.jf.presentation.router.base.Screens;
import com.volacious.jf.ui.activity.base.BaseActivity;

public class LoginActivity extends BaseActivity {

    private static final String LOGIN_START_SCREEN_KEY = "loginStartScreenKey";

    private LoginActivityNavigator navigator;

    public static Intent newIntent(Context context, @Nullable String loginStartScreenKey) {
        Intent intent = new Intent(context, LoginActivity.class);
        if (loginStartScreenKey != null) {
            Bundle extras = new Bundle();
            extras.putString(LOGIN_START_SCREEN_KEY, loginStartScreenKey);
            intent.putExtras(extras);
        }
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        navigator = new LoginActivityNavigator(this, getSupportFragmentManager(), R.id.container_fragment);
        JfuerstApplication.INSTANCE.getRouter().navigateTo(Screens.LOGIN_FRAGMENT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        JfuerstApplication.INSTANCE.getNavigatorHolder().setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        super.onPause();
        JfuerstApplication.INSTANCE.getNavigatorHolder().removeNavigator();
    }

    @Override
    public void onBackPressed() {
        JfuerstApplication.INSTANCE.getRouter().exit();
    }

    public void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
