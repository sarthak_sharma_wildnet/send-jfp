package com.volacious.jf.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.volacious.jf.JfuerstApplication;
import com.volacious.jf.R;
import com.volacious.jf.helpers.PreferencesHelper;
import com.volacious.jf.presentation.router.base.Screens;
import com.volacious.jf.ui.activity.MainActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainFragment extends Fragment {

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.main_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);
        if (!PreferencesHelper.getString(PreferencesHelper.LAST_MAIN_FRAGMENT).isEmpty()) {
            JfuerstApplication.INSTANCE.getRouter().overrideContainer(R.id.main_fragment_container).replaceScreen(PreferencesHelper.getString(PreferencesHelper.LAST_MAIN_FRAGMENT));
        } else {
            JfuerstApplication.INSTANCE.getRouter().overrideContainer(R.id.main_fragment_container).replaceScreen(Screens.TODO_FRAGMENT);
        }
        return v;
    }

    @OnClick(R.id.logout)
    void logout() {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).goToLoginActivity();
        }
    }

    @OnClick(R.id.todoLayout)
    public void toDoButtonClicked() {
        JfuerstApplication.INSTANCE.getRouter().overrideContainer(R.id.main_fragment_container).replaceScreen(Screens.TODO_FRAGMENT);
    }

    @OnClick(R.id.fileTransfersLayout)
    public void filTransfersButtonClicked() {
        JfuerstApplication.INSTANCE.getRouter().overrideContainer(R.id.main_fragment_container).replaceScreen(Screens.FILE_TRANSFERS_FRAGMENT);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
