package com.volacious.jf.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.volacious.jf.R;
import com.volacious.jf.engine.APIManager;
import com.volacious.jf.engine.network.result.User;
import com.volacious.jf.helpers.PreferencesHelper;
import com.volacious.jf.ui.activity.LoginActivity;
import com.volacious.jf.ui.fragment.base.BaseFragment;
import com.volacious.jf.util.toast.ToastManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginFragment extends BaseFragment {

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @BindView(R.id.login_emain_edittext)
    EditText emailEditText;

    @BindView(R.id.login_password_edittext)
    EditText passwordEditText;

    @BindView(R.id.login_button)
    Button loginButton;

    @BindView(R.id.login_forgot_pass_textview)
    TextView forgotPassTextView;

    Unbinder unbinder;
    String localUsername;
    String localPassword;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @OnClick(R.id.login_button)
    void onLoginButtonClick() {
        Log.e("BUTTON", "clicked");
        APIManager.updateRetrofitService();
        localUsername = emailEditText.getText().toString();
        localPassword = passwordEditText.getText().toString();
        if (!localUsername.isEmpty() && !localPassword.isEmpty()) {
            //try to log in
            showIndicator(true);
            APIManager.retrofitService.login(localUsername, localPassword)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<User>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            showIndicator(false);
                            ToastManager.getInstance().showError("Failed to log in!");
                        }

                        @Override
                        public void onNext(User userResult) {
                            loginSucceed();
                        }
                    });
        } else {
            ToastManager.getInstance().showError("Enter email and password!");
        }

    }

    void loginSucceed() {
        showIndicator(false);
        PreferencesHelper.saveString(PreferencesHelper.USERNAME, localUsername);
        PreferencesHelper.saveString(PreferencesHelper.PASSWORD, localPassword);

        if (getActivity() instanceof LoginActivity) {
            ((LoginActivity) getActivity()).goToMainActivity();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
