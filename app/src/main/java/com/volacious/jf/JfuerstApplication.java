package com.volacious.jf;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.preference.PreferenceManager;

import androidx.multidex.MultiDexApplication;

import com.volacious.jf.engine.logger.Logger;
import com.volacious.jf.helpers.PreferencesHelper;
import com.volacious.jf.presentation.cicerone.RocketRouter;
import com.volacious.jf.util.childReplace.CustomRouter;

import io.realm.Realm;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

public class JfuerstApplication extends MultiDexApplication {

    public static final String CHANNEL_ID = "JFuerstServiceChannel";

    public static JfuerstApplication INSTANCE;

    private Cicerone<RocketRouter> cicerone;

    @Override
    public void onCreate() {
        super.onCreate();
        Logger.enable("jf");
        PreferencesHelper.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        INSTANCE = this;
        Realm.init(this);
        cicerone = Cicerone.create(new RocketRouter());
        createNotificationChannel();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "JFuerst Notification Channel",
                    NotificationManager.IMPORTANCE_HIGH
            );
            serviceChannel.setSound(null, null);

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    public NavigatorHolder getNavigatorHolder() {
        return cicerone.getNavigatorHolder();
    }

    public RocketRouter getRouter() {
        return (cicerone.getRouter());
    }

}
