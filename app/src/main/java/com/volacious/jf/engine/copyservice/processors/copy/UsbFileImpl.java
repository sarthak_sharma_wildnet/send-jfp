package com.volacious.jf.engine.copyservice.processors.copy;

import com.github.mjdev.libaums.fs.UsbFile;
import com.github.mjdev.libaums.fs.UsbFileInputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class UsbFileImpl implements FileInterface {

    UsbFile usbFile;

    public UsbFileImpl(UsbFile usbFile) {
        this.usbFile = usbFile;
    }

    @Override
    public String getName() {
        return usbFile.getName();
    }

    @Override
    public long length() {
        return usbFile.getLength();
    }

    @Override
    public void copyFile(File targetLocation) throws Exception {
        InputStream in = new UsbFileInputStream(usbFile);
        OutputStream out = new FileOutputStream(targetLocation);

        // Copy the bits from instream to outstream
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }
}
