package com.volacious.jf.engine.copyservice.processors.copy;

import java.util.Date;

public class CopyInfo {
    private Date StartTime;
    private Date EndTime;

    public CopyInfo(Date startTime) {
        StartTime = startTime;
    }

    public Date getStartTime() {
        return StartTime;
    }

    public void setStartTime(Date startTime) {
        StartTime = startTime;
    }

    public Date getEndTime() {
        return EndTime;
    }

    public void setEndTime(Date endTime) {
        EndTime = endTime;
    }
}
