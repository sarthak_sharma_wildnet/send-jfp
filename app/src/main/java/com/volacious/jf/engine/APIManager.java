package com.volacious.jf.engine;

import androidx.annotation.NonNull;

import com.volacious.jf.engine.network.RetrofitBuilder;
import com.volacious.jf.engine.network.VolaciousAPI;
import com.volacious.jf.engine.network.result.User;
import com.volacious.jf.helpers.PreferencesHelper;
import com.volacious.jf.util.toast.ToastManager;

import rx.Observer;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class APIManager {

    public static VolaciousAPI retrofitService = RetrofitBuilder.createRetrofitService(VolaciousAPI.class);

    private static User user;

    public static void updateRetrofitService() {
        user = null;
        retrofitService = RetrofitBuilder.createRetrofitService(VolaciousAPI.class);
    }

    public static void ensureLogin(@NonNull EnsureLoginCallback ensureLoginCallback) {
        ensureLogin(ensureLoginCallback, AndroidSchedulers.mainThread());
    }

    public static void ensureLogin(@NonNull EnsureLoginCallback ensureLoginCallback, Scheduler scheduler) {
        if (user != null) {
            ensureLoginCallback.loginSuccess(user);
            return;
        }

        String username = PreferencesHelper.getString(PreferencesHelper.USERNAME);
        String password = PreferencesHelper.getString(PreferencesHelper.PASSWORD);
        if (!username.isEmpty() && !password.isEmpty()) {
            APIManager.retrofitService.login(username, password)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(scheduler)
                    .subscribe(new Observer<User>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            ToastManager.getInstance().showError("Unable to log in! Please logout and login again!");
                            ensureLoginCallback.loginFailed();
                        }

                        @Override
                        public void onNext(User userResult) {
                            APIManager.user = userResult;
                            ensureLoginCallback.loginSuccess(userResult);
                        }
                    });
        } else {
            ToastManager.getInstance().showError("Please logout and login again!");
            ensureLoginCallback.loginFailed();
        }
    }
}
