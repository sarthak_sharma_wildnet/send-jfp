package com.volacious.jf.engine.copyservice;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.volacious.jf.R;
import com.volacious.jf.engine.copyservice.processors.copy.CopyProcessor;
import com.volacious.jf.engine.copyservice.processors.ServiceProcessor;
import com.volacious.jf.engine.copyservice.processors.copy.CopyProcessorThreadHelper;
import com.volacious.jf.engine.copyservice.processors.upload.UploadProcessor;
import com.volacious.jf.engine.copyservice.processors.upload.UploadProcessorThreadHelper;
import com.volacious.jf.engine.logger.Logger;
import com.volacious.jf.engine.network.result.FileTransfer;
import com.volacious.jf.engine.realm.RealmManager;
import com.volacious.jf.engine.realm.models.ProgressStatusModel;
import com.volacious.jf.ui.activity.MainActivity;
import com.volacious.jf.util.adapter.storages.UploadStorage;

import java.io.Serializable;

import io.realm.RealmResults;
import io.realm.Sort;

import static com.volacious.jf.JfuerstApplication.CHANNEL_ID;

public class CopyUploadService extends Service implements ServiceInterface {

    public static final String FILE_TRANSFER = "fileTransfer";

    private CopyProcessor copyProcessor;
    private UploadProcessor uploadProcessor;

    private MediaPlayer player;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void updateNotification(String note) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("JFuerst Photo")
                .setContentText(note)
                .setSmallIcon(R.drawable.ic_check)
                .setContentIntent(pendingIntent);
        Notification notification = notificationBuilder.build();
        startForeground(77777, notification);
        startCheckThread();
    }

    public void playSound(SoundType soundType) {
        if (player != null) {
            if (player.isPlaying()) {
                player.stop();
                player.release();
            }
        }
        player = MediaPlayer.create(this, soundType.getValue());
        player.start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("BBBB", "BOOOT SERVICE");
        if (intent != null) {
            Serializable fileTransferSerializable = intent.getSerializableExtra(FILE_TRANSFER);
            if (fileTransferSerializable == null) {
                updateNotification("Running...");
                return START_STICKY;
            }

            if (fileTransferSerializable instanceof FileTransfer) {
                UploadStorage uploadStorage = (UploadStorage) DataTransitionManager.getInstance().receiveData(DataTransitionManager.UPLOAD_STORAGE);
                copyProcessor = new CopyProcessor(this, (FileTransfer) fileTransferSerializable, uploadStorage);
                CopyProcessorThreadHelper.getHandler().post(() -> copyProcessor.process());
                return START_STICKY;
            }

            return START_STICKY;
        } else {
            updateNotification("Running...");
            return START_STICKY;
        }
    }

    private void checkUploads() {
        if (!UploadProcessor.inProgress()) {
            RealmManager.executeRealmTransactionSync(realm -> {
                String[] fieldNames = {"priority", "creationDate"};
                Sort sort[] = {Sort.DESCENDING, Sort.ASCENDING};
                RealmResults<ProgressStatusModel> progressStatusModels = realm
                        .where(ProgressStatusModel.class)
                        .sort(fieldNames, sort)
                        .findAll();
                if (progressStatusModels != null) {
                    //Logger.log("FOUND MODELS:" + progressStatusModels.size());
                }

                if (progressStatusModels != null && progressStatusModels.size() > 0) {
                    for (ProgressStatusModel progressStatusModel : progressStatusModels) {
                        if (progressStatusModel.isReadyToUpload()) {
                            Logger.log(progressStatusModel.toString());
                            UploadProcessor newUploadProcessor = new UploadProcessor(this, progressStatusModel.getId());
                            UploadProcessorThreadHelper.getHandler().post(() -> {
                                uploadProcessor = newUploadProcessor;
                                newUploadProcessor.process();
                            });
                            break;
                        }
                    }
                }
            });
        }
    }

    public void createNotificationChannel(int channelId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    "J&F " + channelId,
                    "JFuerst Notification Channel " + channelId,
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            serviceChannel.setSound(null, null);

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    public void deleteNotificationChannel(int channelId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.deleteNotificationChannel("J&F " + channelId);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void processStarted(ServiceProcessor processor) {
        createNotificationChannel(processor.getNotificationChannelId());
        updateNotification(processor.getStartNotification());
    }

    @Override
    public void processFinished(ServiceProcessor processor, @Nullable ServiceProcessor nextProcessor) {
        processor.processFinished();
        deleteNotificationChannel(processor.getNotificationChannelId());
        updateNotification(processor.getFinishNotification());
        if (processor instanceof CopyProcessor) {
            copyProcessor = null;
        }
        if (processor instanceof UploadProcessor) {
            uploadProcessor = null;
        }
    }

    private Thread checkThread;

    private void startCheckThread() {
        if (checkThread != null) {
            return;
        }

        Log.i("BBBB", "STARTING CHECK THREAD");

        checkThread = new Thread(() -> {
            while (true) {
                try {
                    //Log.i("BBBB", "CHECKING UPLOADS");
                    checkUploads();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e1) {
                    break;
                }
            }
        });
        checkThread.start();
    }
}
