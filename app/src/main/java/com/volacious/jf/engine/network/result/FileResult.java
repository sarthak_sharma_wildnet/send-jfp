package com.volacious.jf.engine.network.result;

import com.google.gson.annotations.SerializedName;

public class FileResult {
    @SerializedName("checksum")
    private String checksum;

    @SerializedName("verified")
    private boolean verified;

    @SerializedName("size")
    private int size;

    @SerializedName("originalName")
    private String originalName;

    @SerializedName("filename")
    private String filename;

    @SerializedName("destination")
    private String destination;

    @SerializedName("path")
    private String path;

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
