package com.volacious.jf.engine.copyservice.processors.copy;

import com.volacious.jf.helpers.DoubleHelper;
import com.volacious.jf.ui.fragment.uploadprocess.UIUpdateEvent;
import com.volacious.jf.ui.fragment.uploadprocess.UIUpdateInterface;
import com.volacious.jf.util.adapter.FileStatus;
import com.volacious.jf.engine.network.result.FileTransfer;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.volacious.jf.util.adapter.FileStatus.Status.copyDone;
import static com.volacious.jf.util.adapter.FileStatus.Status.none;

public class CopyProgress implements UIUpdateInterface {
    private long totalSize;
    private long doneSize;
    private long filesDone;
    private long filesCount;
    private Long transferSpeed;
    private FileTransfer fileTransfer;
    private List<FileInterface> sourceFiles;

    public CopyProgress(FileTransfer fileTransfer, List<FileInterface> copyFiles) {
        this.fileTransfer = fileTransfer;
        this.sourceFiles = copyFiles;
        filesCount = copyFiles.size();
        totalSize = 0;
        for (FileInterface copyFile : copyFiles) {
            totalSize += copyFile.length();
        }
        filesDone = 0;
    }

    public void setDoneSize(long doneSize) {
        this.doneSize = doneSize;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public long getDoneSize() {
        return doneSize;
    }

    public long getFilesDone() {
        return filesDone;
    }

    public void setFilesDone(long filesDone) {
        this.filesDone = filesDone;
        if (filesDone == filesCount) {
            transferSpeed = getTransferSpeed();
        }
    }

    public long getFilesCount() {
        return filesCount;
    }

    public int getProgressPercent() {
        if (filesDone == filesCount) {
            return 100;
        }
        int res = (int) (((double) getDoneSize() / getTotalSize()) * 100);
        if (res < 0) {
            return 0;
        }
        return res;
    }

    public boolean isFinished() {
        return filesDone == filesCount;
    }

    public long getTransferSpeed() {
        if (transferSpeed != null) {
            return transferSpeed;
        }
        Date endDate = getFileTransfer().getCopyEndTime();
        if (endDate == null) {
            endDate = new Date();
        }
        long diffInMs = endDate.getTime() - getFileTransfer().getCopyStartTime().getTime();
        return doneSize * 1000 * 8 / diffInMs;
    }

    @Override
    public UIUpdateEvent getUIUpdateEvent() {
        UIUpdateEvent updateEvent = new UIUpdateEvent();
        updateEvent.setProgress(getProgressPercent());
        updateEvent.setCopyIconVisible(true);
        updateEvent.setUploadIconVisible(false);
        updateEvent.setUploadLabel(null);
        updateEvent.setUploadStatus(0);
        updateEvent.setFilesProgressStatus("" + (getFilesCount() - getFilesDone()) +
                "/" + getFilesCount() + " files left");
        updateEvent.setLoadingIndicatorVisible(true);
        updateEvent.setNoteText(getFileTransfer().getPhotogNotes());
        if (isFinished()) {
            updateEvent.setCopyStatus(2);
            updateEvent.setCopyLabel("Copy finished (100%)");
            updateEvent.setStatusLabel("Prepare for upload");
            updateEvent.setSpeedIconVisible(false);
            updateEvent.setSpeedLabel(null);
        } else {
            //updateEvent.setCopyStopVisible(true);
            updateEvent.setCopyStatus(1);
            updateEvent.setCopyLabel("Copying in progress");
            updateEvent.setStatusLabel("Copying (" + getProgressPercent() + "%)");
            updateEvent.setSpeedIconVisible(true);
            updateEvent.setSpeedLabel(DoubleHelper.asTwoNumbersAfterDot(((double) getTransferSpeed()) / 1024 / 1024) + " Mb/s");
        }

        List<FileStatus> fileStatuses = new ArrayList<>();

        if (sourceFiles != null) {
            for (int i = 0; i < sourceFiles.size(); i++) {
                fileStatuses.add(new FileStatus(sourceFiles.get(i).getName(), i < getFilesDone() ? copyDone : none));
            }
        } else {
            List<File> allFiles = getFileTransfer().getFilesToUpload();
            for (int i = 0; i < allFiles.size(); i++) {
                fileStatuses.add(new FileStatus(allFiles.get(i).getName(), i < getFilesDone() ? copyDone : none));
            }
        }

        updateEvent.setFileStatuses(fileStatuses);

        return updateEvent;
    }

    public FileTransfer getFileTransfer() {
        return fileTransfer;
    }

    public void setFileTransfer(FileTransfer fileTransfer) {
        this.fileTransfer = fileTransfer;
    }
}
