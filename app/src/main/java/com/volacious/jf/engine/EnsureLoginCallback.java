package com.volacious.jf.engine;

import com.volacious.jf.engine.network.result.User;

public interface EnsureLoginCallback {
    void loginSuccess(User user);
    void loginFailed();
}
