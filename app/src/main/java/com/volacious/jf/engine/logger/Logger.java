package com.volacious.jf.engine.logger;

import android.util.Log;

import com.volacious.jf.engine.KeyGenerator;
import com.volacious.jf.engine.logger.model.LogRequestResponse;
import com.volacious.jf.engine.logger.model.SimpleLogRequest;
import com.volacious.jf.engine.logger.model.logLevel.LogDestination;
import com.volacious.jf.engine.logger.model.logLevel.LogLevel;
import com.volacious.jf.engine.logger.model.logLevel.LogLevelResponse;
import com.volacious.jf.engine.network.RetrofitBuilder;
import com.volacious.jf.helpers.PreferencesHelper;

import java.util.Date;

import rx.Observer;
import rx.schedulers.Schedulers;


public class Logger {
    public static LogLevel logLevel = LogLevel.none;
    public static LogDestination logDestination = LogDestination.device;
    public static String token;
    public static String baseUrl = "http://91.235.246.22:15454/";

    public static LogAPI retrofitService = RetrofitBuilder.createRetrofitService(LogAPI.class, baseUrl);

    public static void log(String item) {
        Logger.log(item, LogLevel.debug);
    }

    public static void log(String _item, LogLevel logLevel) {
        if (logLevel.compare(Logger.logLevel) <= 0) {
            String item = PreferencesHelper.getString(PreferencesHelper.USERNAME) + ":" + _item;

            if (Logger.logDestination == LogDestination.both || Logger.logDestination == LogDestination.device) {
                Log.i("Logger", item);
            }
            String token = Logger.token;
            if (token != null && (Logger.logDestination == LogDestination.both || Logger.logDestination == LogDestination.server)) {
                retrofitService.sendNewLog(new SimpleLogRequest(KeyGenerator.generateKey(), item, token, new Date())).subscribeOn(Schedulers.newThread())
                        .observeOn(Schedulers.newThread())
                        .subscribe(new Observer<LogRequestResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(LogRequestResponse logRequestResponse) {
                            }
                        });
            }
        }
    }

    public static void enable(String appName) {
        retrofitService.getLogLevel(appName).subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread()).subscribe(new Observer<LogLevelResponse>() {

            @Override
            public void onNext(LogLevelResponse logLevelResponse) {
                if (logLevelResponse.getLogLevel() != null) {
                    Logger.logLevel = logLevelResponse.getLogLevel();
                }
                if (logLevelResponse.getLogDestination() != null) {
                    Logger.logDestination = logLevelResponse.getLogDestination();
                }
                if (logLevelResponse.getToken() != null) {
                    Logger.token = logLevelResponse.getToken();
                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onCompleted() {

            }
        });
    }
}
