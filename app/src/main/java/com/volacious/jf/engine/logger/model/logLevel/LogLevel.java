package com.volacious.jf.engine.logger.model.logLevel;

public enum LogLevel {
    none("none"), debug("debug"), verbose("verbose");

    String level;

    private LogLevel(String level) {
        this.level = level;
    }

    public String getLevel() {
        return this.level;
    }

    public int compare(LogLevel to) {
        return this.getLevel().length() - to.getLevel().length();
    }
}