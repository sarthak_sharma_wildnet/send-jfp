package com.volacious.jf.engine.realm.models;

import android.util.Log;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;

import com.volacious.jf.engine.copyservice.processors.upload.UploadProcessor;
import com.volacious.jf.helpers.DateHelper;
import com.volacious.jf.helpers.DoubleHelper;
import com.volacious.jf.ui.fragment.uploadprocess.UIUpdateEvent;
import com.volacious.jf.ui.fragment.uploadprocess.UIUpdateInterface;
import com.volacious.jf.util.adapter.FileStatus;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.volacious.jf.util.adapter.FileStatus.Status.copyDone;
import static com.volacious.jf.util.adapter.FileStatus.Status.uploadDone;
import static com.volacious.jf.util.adapter.FileStatus.Status.uploading;

public class UploadStatusModel extends RealmObject implements UIUpdateInterface {

    private Date uploadStartDate;
    private Date uploadFinishDate;

    private long doneSize;
    private RealmList<FileCompleteModel> filesCompleted;
    private Long transferSpeed;

    @LinkingObjects("uploadStatusModel")
    private final RealmResults<ProgressStatusModel> parent = null;

    public Date getUploadStartDate() {
        return uploadStartDate;
    }

    public void setUploadStartDate(Date uploadStartDate) {
        this.uploadStartDate = uploadStartDate;
    }

    public Date getUploadFinishDate() {
        return uploadFinishDate;
    }

    public void setUploadFinishDate(Date uploadFinishDate) {
        this.uploadFinishDate = uploadFinishDate;
    }

    public long getDoneSize() {
        return doneSize;
    }

    public void setDoneSize(long doneSize) {
        this.doneSize = doneSize;
    }

    public RealmList<FileCompleteModel> getFilesCompleted() {
        return filesCompleted;
    }

    public void setFilesCompleted(RealmList<FileCompleteModel> filesCompleted) {
        this.filesCompleted = filesCompleted;
    }

    public Long getTransferSpeed() {
        return transferSpeed;
    }

    public Long safeGetTransferSpeed(){
        if (getTransferSpeed() == null){
             return 0L;
        } else {
            return getTransferSpeed();
        }
    }

    public void setTransferSpeed(Long transferSpeed) {
        this.transferSpeed = transferSpeed;
    }



    public boolean isFileCompleted(File file) {
        if (filesCompleted != null) {
            for (FileCompleteModel fileComplete : filesCompleted) {
                if (fileComplete.getFile().equals(file.getPath())) {
                    return true;
                }
            }
        }
        return false;
    }

    public void addFileToCompleted(File file, Date start, Date end, Realm realm) {
        if (filesCompleted == null) {
            filesCompleted = new RealmList<>();
        }
        FileCompleteModel fileCompleteModel = realm.createObject(FileCompleteModel.class);
        fileCompleteModel.setFile(file.getPath());
        fileCompleteModel.setStart(start);
        fileCompleteModel.setEnd(end);
        fileCompleteModel.setSpeed(file.length() * 1000 * 8 / (end.getTime() - start.getTime()));
        filesCompleted.add(fileCompleteModel);
    }

    public void updateTransferSpeed(long txBytesBefore, long txBytesAfter, Date dateBefore, Date dateAfter) {
        long filesDone = getFilesCompleted() != null ? getFilesCompleted().size() : 0;
        long timeDiff = dateAfter.getTime() - dateBefore.getTime();
        long res = (txBytesAfter - txBytesBefore) * 8 * 1000 / timeDiff;
        long newRate = (safeGetTransferSpeed() * (filesDone - 1) + res) / filesDone;
        Log.i("TRRRRR", "" + (((double) safeGetTransferSpeed()) / 1024 / 1024) + " " + (((double) res) / 1024 / 1024));
        setTransferSpeed(newRate);
    }

    public long getFilesDone() {
        return getFilesCompleted() != null ? getFilesCompleted().size() : 0;
    }

    public long getFilesCount() {
        long count = 0;
        try{
          count = parent.first().getFilesCount();
        } catch (Exception ignored){}
        return count;
    }

    public int getProgressPercent() {
        if (getFilesDone() == getFilesCount()) {
            return 100;
        }
        int res = (int) (((double) getDoneSize() / parent.first().getTotalSize()) * 100);

        if (res < 0) {
            return 0;
        }
        return res;
    }

    public boolean isPossibleToContinue() {
        boolean result = true;
        if (getFilesDone() == parent.first().getFilesCount()) {
            result = false;
        }

        try {
            if (parent.first().getFiles() != null) {
                for (String fileStr : parent.first().getFiles()) {
                    File file = new File(fileStr);
                    if (!file.exists()) {
                        result = false;
                        break;
                    }
                }
            } else {
                result = false;
            }
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public boolean isStopped() {
        return !isFinished() && UploadProcessor.fileTransferInProgress != parent.first().getId();
    }

    public boolean isFinished() {
        return getFilesDone() == getFilesCount();
    }

    @Override
    public UIUpdateEvent getUIUpdateEvent() {
        UIUpdateEvent updateEvent = new UIUpdateEvent();
        updateEvent.setCopyLabel("Copy finished (100%)");
        updateEvent.setCopyStatus(2);
        updateEvent.setCopyIconVisible(true);
        updateEvent.setUploadLabel(null);
        if (isStopped()) {
            updateEvent.setUploadStatus(1);
            updateEvent.setLoadingIndicatorVisible(false);
            updateEvent.setProgress(getProgressPercent());
            updateEvent.setStatusLabel("Upload stopped: " + getProgressPercent() + "%");
            updateEvent.setSpeedIconVisible(true);
            updateEvent.setFilesProgressStatus("" + (getFilesCount() - getFilesDone()) +
                    "/" + getFilesCount() + " files left");
            updateEvent.setSpeedLabel(DoubleHelper.asTwoNumbersAfterDot(((double) getTransferSpeed()) / 1024 / 1024) + " Mb/s");
        } else if (isFinished()) {
            updateEvent.setUploadStatus(2);
            updateEvent.setLoadingIndicatorVisible(false);
            updateEvent.setUploadIconVisible(true);
            updateEvent.setUploadLabel("Upload finished (100%)");
            updateEvent.setStatusLabel("Uploaded");
            updateEvent.setSpeedIconVisible(true);
            updateEvent.setSpeedLabel(DoubleHelper.asTwoNumbersAfterDot(((double) getTransferSpeed()) / 1024 / 1024) + " Mb/s");
            updateEvent.setFilesProgressStatus("Files (" + getFilesCount() + ")");
        } else {
            updateEvent.setUploadStatus(1);
            updateEvent.setLoadingIndicatorVisible(true);
            updateEvent.setProgress(getProgressPercent());
            updateEvent.setStatusLabel("Uploading " + getProgressPercent() + "%");
            updateEvent.setSpeedIconVisible(true);
            updateEvent.setFilesProgressStatus("" + (getFilesCount() - getFilesDone()) +
                    "/" + getFilesCount() + " files left");
            updateEvent.setSpeedLabel(DoubleHelper.asTwoNumbersAfterDot(((double) getTransferSpeed()) / 1024 / 1024) + " Mb/s");
        }

        List<FileStatus> fileStatuses = new ArrayList<>();
        RealmList<String> allFiles = parent.first().getFiles();
        for (int i = 0; i < allFiles.size(); i++) {
            String filePath = allFiles.get(i);
            File file = new File(filePath);
            if (UploadProcessor.getFilesInProgress().contains(file)) {
                fileStatuses.add(new FileStatus(file.getName(), uploading));
            } else if (isFileCompleted(file)) {
                fileStatuses.add(new FileStatus(file.getName(), uploadDone));
            } else {
                fileStatuses.add(new FileStatus(file.getName(), copyDone));
            }
        }

        updateEvent.setFileStatuses(fileStatuses);

        return updateEvent;
    }

    @Override
    public String toString() {
        return "UploadStatusModel{" +
                "uploadStartDate=" + getUploadStartDate() +
                ", uploadFinishDate=" + getUploadFinishDate() +
                ", doneSize=" + getDoneSize() +
                ", filesCompleted=" + getFilesCompleted().toString() +
                ", transferSpeed=" + getTransferSpeed() +
                '}';
    }
}
