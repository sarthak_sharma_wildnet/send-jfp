package com.volacious.jf.engine.copyservice.processors.upload;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Process;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Nik on 11.09.2018.
 */
public class UploadProcessorThreadHelper {
    private static final HandlerThread handlerThread;
    private static final Scheduler scheduler;

    private static final String threadKey = "UploadThread";

    static {
        handlerThread = new HandlerThread(threadKey, Process.THREAD_PRIORITY_BACKGROUND);
        if (!handlerThread.isAlive())
            handlerThread.start();
        scheduler = AndroidSchedulers.from(handlerThread.getLooper());
    }

    public static Handler getHandler() {
        return new Handler(handlerThread.getLooper());
    }

    public static Scheduler getScheduler() {
        return scheduler;
    }
}
