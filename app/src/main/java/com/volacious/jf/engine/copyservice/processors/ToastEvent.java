package com.volacious.jf.engine.copyservice.processors;

public class ToastEvent {
    private String error;
    private String status;

    public ToastEvent(String error, String status) {
        this.error = error;
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public String getStatus() {
        return status;
    }
}
