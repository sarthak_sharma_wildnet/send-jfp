package com.volacious.jf.engine.copyservice.processors.upload;

public class UploadStopEvent {
    private int fileTransferId;

    public UploadStopEvent(int fileTransferId) {
        this.fileTransferId = fileTransferId;
    }

    public int getFileTransferId() {
        return fileTransferId;
    }
}
