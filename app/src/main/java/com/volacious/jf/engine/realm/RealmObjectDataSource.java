package com.volacious.jf.engine.realm;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by Nik on 08.09.2018.
 */
public class RealmObjectDataSource<T extends RealmObject> implements DataSourceDisposable {

    private Disposable disposable;

    public RealmObjectDataSource() {
    }

    private boolean returnToMainThread = true;

    public void dispose() {
        if (returnToMainThread) {
            disposeWorker();
        } else {
            DataSourceThreadHelper.getHandler().post(this::disposeWorker);
        }
    }

    private void disposeWorker() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
            disposable = null;
        }
    }

    public RealmObjectDataSource<T> mainThread(boolean returnToMainThread) {
        this.returnToMainThread = returnToMainThread;
        return this;
    }

    public RealmObjectDataSource<T> onChange(Consumer<T> onChange) {
        this.onChange = onChange;
        return this;
    }

    private Consumer<T> onChange;

    public RealmObjectDataSource<T> observe(RealmObjectGetterInterface<T> getter) {
        return observe(getter, RealmManager.BASE_REALM);
    }

    public RealmObjectDataSource<T> observe(RealmObjectGetterInterface<T> getter, int RealmID) {
        if (returnToMainThread) {
            RealmManager.executeWithRealm(realm -> {
                T object = getter.getObject(realm);
                if (object != null) {
                    disposable = object
                            .asFlowable()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe((Consumer<RealmObject>) onChange);
                }
            }, RealmID);
        } else {
            DataSourceThreadHelper.getHandler().post(() ->
                    RealmManager.executeWithRealm(realm -> {
                        T object = getter.getObject(realm);
                        if (object != null) {
                            disposable = object
                                    .asFlowable()
                                    .observeOn(DataSourceThreadHelper.getScheduler())
                                    .subscribe((Consumer<RealmObject>) onChange);
                        }
                    }, RealmID)
            );
        }
        return this;
    }

    public interface RealmObjectGetterInterface<T> {
        T getObject(Realm realm);
    }
}
