package com.volacious.jf.engine.copyservice.processors.check;

import com.volacious.jf.engine.copyservice.ServiceInterface;
import com.volacious.jf.engine.copyservice.processors.FileUtils;
import com.volacious.jf.engine.copyservice.processors.ServiceProcessor;

import java.io.File;

public class CheckProcessor extends ServiceProcessor {

    public CheckProcessor(ServiceInterface serviceInterface) {
        super(serviceInterface);
    }

    @Override
    public int getNotificationChannelId() {
        return 99999;
    }

    @Override
    public String getNotificationTitle() {
        return "J&F Check";
    }

    @Override
    public String getStartNotification() {
        return "Check: " + getNotificationTitle();
    }

    @Override
    public String getFinishNotification() {
        return "Check finished: " + getNotificationTitle();
    }

    @Override
    public void process() {
        getServiceInterface().processStarted(this);
        updateNotification("Checking pending uploads");
        File appDir = getServiceInterface().getContext().getFilesDir();
        File[] files = appDir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory() && file.getPath().contains("photoshoot_")) {
                }
            }
        }
        getServiceInterface().processFinished(this, null);
    }
}
