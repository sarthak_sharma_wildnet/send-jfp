package com.volacious.jf.engine.copyservice;

import java.util.HashMap;

/**
 * Created by Nik on 09.06.2018.
 */

public class DataTransitionManager {

    public final static String UPLOAD_STORAGE = "UPLOAD_STORAGE";

    private HashMap<String, Object> transitionData = new HashMap<>();

    private DataTransitionManager() {}

    private static DataTransitionManager instance;

    public static DataTransitionManager getInstance() {
        if (instance == null) {
            instance = new DataTransitionManager();
        }
        return instance;
    }

    public void putData(String key, Object data) {
        transitionData.put(key, data);
    }

    public Object receiveData(String key) {
        if (transitionData.containsKey(key)) {
            Object data = transitionData.get(key);
            transitionData.remove(key);
            return data;
        }
        return null;
    }
}
