package com.volacious.jf.engine.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volacious.jf.engine.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder {

    public static <T> T createRetrofitService(final Class<T> clazz) {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();

        final Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(createOkHttpClient())
                .build();

        return restAdapter.create(clazz);
    }

    public static <T> T createRetrofitService(final Class<T> clazz, String baseUrl) {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();

        final Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(createOkHttpClient())
                .build();

        return restAdapter.create(clazz);
    }

    private static okhttp3.OkHttpClient createOkHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        okhttp3.OkHttpClient.Builder b = new okhttp3.OkHttpClient.Builder();
        b.readTimeout(1000, TimeUnit.SECONDS);
        b.writeTimeout(1000, TimeUnit.SECONDS);
        b.connectTimeout(1000, TimeUnit.SECONDS);
        b.addInterceptor(chain -> chain.proceed(chain.request()));


        //listener
        //OnResponseReceived onResponseReceived = new OnResponseReceived();

        //PersistentStatsHandler networkRequestStatsHandler = new PersistentStatsHandler(JfuerstApplication.INSTANCE);
        //register your listener with the PersistentStatsHandler
        //networkRequestStatsHandler.addListener(onResponseReceived);

        //NetworkInterpreter networkInterpreter = new DefaultInterpreter(new NetworkEventReporterImpl(networkRequestStatsHandler));

        /*NetworkInterceptor networkInterceptor = new NetworkInterceptor.Builder()
                .setNetworkInterpreter(networkInterpreter)
                .setEnabled(true)
                .build();*/

        //b.addInterceptor(networkInterceptor);

        //TransferInterceptor transferInterceptor = new TransferInterceptor();

        //b.addInterceptor(transferInterceptor);
        //b.interceptors().add(transferInterceptor);

        b.interceptors().add(interceptor);
        //b.interceptors().add(networkInterceptor);

        b.cookieJar(new JFCookieJar());
        return b.build();
    }
}
