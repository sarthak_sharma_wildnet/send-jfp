package com.volacious.jf.engine.copyservice.processors.check;

import com.volacious.jf.engine.copyservice.ServiceInterface;
import com.volacious.jf.engine.copyservice.processors.FileUtils;
import com.volacious.jf.engine.copyservice.processors.ServiceProcessor;

import java.io.File;

public class CleanProcessor extends ServiceProcessor {

    public CleanProcessor(ServiceInterface serviceInterface) {
        super(serviceInterface);
    }


    @Override
    public int getNotificationChannelId() {
        return 99998;
    }

    @Override
    public String getNotificationTitle() {
        return "J&F Clean";
    }

    @Override
    public String getStartNotification() {
        return "Clean: " + getNotificationTitle();
    }

    @Override
    public String getFinishNotification() {
        return "Clean finished: " + getNotificationTitle();
    }

    @Override
    public void process() {
        getServiceInterface().processStarted(this);
        updateNotification("Cleaning temporary files");
        File appDir = getServiceInterface().getContext().getFilesDir();
        File[] files = appDir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory() && (file.getPath().contains("temp_photoshoot_")/* ||
                        file.getPath().contains("photoshoot_")*/)) {
                    FileUtils.deleteDirectory(file);
                }
            }
        }
        getServiceInterface().processFinished(this, null);
    }
}
