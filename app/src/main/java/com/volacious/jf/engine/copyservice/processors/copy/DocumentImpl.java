package com.volacious.jf.engine.copyservice.processors.copy;
import android.content.Context;
import androidx.documentfile.provider.DocumentFile;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class DocumentImpl implements FileInterface {
    private DocumentFile documentFile;
    private Context context;

    public DocumentImpl(DocumentFile documentFile, Context context) {
        this.documentFile = documentFile;
        this.context = context;
    }

    @Override
    public String getName() {
        return documentFile.getName();
    }

    @Override
    public long length() {
        return documentFile.length();
    }

    @Override
    public void copyFile(File targetLocation) throws Exception {
        InputStream in = context.getContentResolver().openInputStream(documentFile.getUri());
        OutputStream out = new FileOutputStream(targetLocation);

        // Copy the bits from instream to outstream
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }
}
