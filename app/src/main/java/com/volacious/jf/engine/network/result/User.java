package com.volacious.jf.engine.network.result;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class User {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("lastName")
    private String lastName;

    @SerializedName("failedAttempts")
    private int failedAttempts;

    @SerializedName("sysPermissions")
    private String sysPermissions;

    @SerializedName("emails")
    private ArrayList<Email> emails;

    @SerializedName("Roles")
    private ArrayList<Role> Roles;

    @SerializedName("PhotogProfile")
    private PhotogProfile PhotogProfile;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getFailedAttempts() {
        return failedAttempts;
    }

    public void setFailedAttempts(int failedAttempts) {
        this.failedAttempts = failedAttempts;
    }

    public String getSysPermissions() {
        return sysPermissions;
    }

    public void setSysPermissions(String sysPermissions) {
        this.sysPermissions = sysPermissions;
    }

    public ArrayList<Email> getEmails() {
        return emails;
    }

    public void setEmails(ArrayList<Email> emails) {
        this.emails = emails;
    }

    public ArrayList<Role> getRoles() {
        return Roles;
    }

    public void setRoles(ArrayList<Role> roles) {
        Roles = roles;
    }

    public com.volacious.jf.engine.network.result.PhotogProfile getPhotogProfile() {
        return PhotogProfile;
    }

    public void setPhotogProfile(com.volacious.jf.engine.network.result.PhotogProfile photogProfile) {
        PhotogProfile = photogProfile;
    }
}
