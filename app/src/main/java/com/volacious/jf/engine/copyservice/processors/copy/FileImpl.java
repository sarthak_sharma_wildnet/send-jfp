package com.volacious.jf.engine.copyservice.processors.copy;

import com.volacious.jf.engine.copyservice.processors.FileUtils;

import java.io.File;

public class FileImpl implements FileInterface {
    private File file;

    public FileImpl(File file) {
        this.file = file;
    }

    @Override
    public String getName() {
        return file.getName();
    }

    @Override
    public long length() {
        return file.length();
    }

    @Override
    public void copyFile(File targetLocation) throws Exception {
        FileUtils.copyFile(file, targetLocation);
    }
}
