package com.volacious.jf.engine;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by 76332 on 13.03.2018.
 */

public class KeyGenerator {

    public static final String MCSTemporaryKey = "mcs_key_for_temporary_model";

    @NonNull
    public static String generateKey() {
        long t = (new Date()).getTime();
        t = t - 1475547348163L;

        List<Integer> code = new ArrayList<>();

        while(t>0) {
            int x = (int)(t % 61);
            code.add(x);
            t -= x;
            t /= 61;
        }

        //fixed number of random characters
        for(int i = 0; i < 5; i++) {
            code.add(RandomGenerator.getRandomIndex(61));
        }

        StringBuilder id = new StringBuilder();

        for (int x : code) {
            if(x < 9) {
                x += 49;
            }
            else if(x<35) {
                x += 65-9;
            }
            else {
                x += 97-35;
            }

            id.append((char) x);
        }

        return id.toString();
    }
}
