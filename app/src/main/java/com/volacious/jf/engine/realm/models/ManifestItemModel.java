package com.volacious.jf.engine.realm.models;

import io.realm.RealmObject;

public class ManifestItemModel extends RealmObject {
    private String filename;

    private long size;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
