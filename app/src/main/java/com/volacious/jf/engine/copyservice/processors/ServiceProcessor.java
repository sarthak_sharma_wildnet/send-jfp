package com.volacious.jf.engine.copyservice.processors;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;

import com.volacious.jf.R;
import com.volacious.jf.engine.copyservice.ServiceInterface;
import com.volacious.jf.ui.activity.MainActivity;

public abstract class ServiceProcessor {
    private ServiceInterface serviceInterface;
    protected boolean isStopping = false;

    public ServiceProcessor(ServiceInterface serviceInterface) {
        this.serviceInterface = serviceInterface;
    }

    public void updateNotification(String note) {
        Intent notificationIntent = new Intent(serviceInterface.getContext(), MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(serviceInterface.getContext(),
                0, notificationIntent, 0);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat
                .Builder(serviceInterface.getContext(), "J&F " + getNotificationChannelId())
                .setContentTitle(getNotificationTitle())
                .setContentText(note)
                .setSmallIcon(R.drawable.ic_check)
                .setContentIntent(pendingIntent);
        Notification notification = notificationBuilder.build();
        final NotificationManager notificationManager = (NotificationManager) serviceInterface.getContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(getNotificationChannelId(), notification);
    }

    public abstract int getNotificationChannelId();

    public abstract String getNotificationTitle();

    public abstract String getStartNotification();

    public abstract String getFinishNotification();

    public ServiceInterface getServiceInterface() {
        return serviceInterface;
    }

    public abstract void process();

    public void processFinished() {
    }

    public void stopProcessor() {
        isStopping = true;
    }

    public boolean isStopping() {
        return isStopping;
    }
}
