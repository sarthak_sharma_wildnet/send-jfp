package com.volacious.jf.engine.copyservice.processors.copy;

public class ProcessStopEvent {
    private int fileTransferId;

    public ProcessStopEvent(int fileTransferId) {
        this.fileTransferId = fileTransferId;
    }

    public int getFileTransferId() {
        return fileTransferId;
    }
}
