package com.volacious.jf.engine.bootreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import android.util.Log;

import com.volacious.jf.engine.copyservice.CopyUploadService;

public class BootUpReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("BBBB", "BOOOT");
        Intent serviceIntent = new Intent(context, CopyUploadService.class);
        ContextCompat.startForegroundService(context, serviceIntent);
    }
}
