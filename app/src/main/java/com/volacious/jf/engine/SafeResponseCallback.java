package com.volacious.jf.engine;

import com.volacious.jf.engine.cache.CacheManager;
import com.volacious.jf.engine.network.result.IDInterface;

import java.util.List;

import rx.Observer;

public abstract class SafeResponseCallback<T> implements Observer<T> {
    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(T t) {
        if (t instanceof IDInterface) {
            CacheManager.getInstance().cacheObject((IDInterface) t);
        } else if (t instanceof List) {
            CacheManager.getInstance().cacheObjects((List) t);
        }
    }
}
