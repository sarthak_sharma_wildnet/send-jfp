package com.volacious.jf.engine.realm.models;

import android.util.Log;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;

import com.volacious.jf.helpers.DateHelper;
import com.volacious.jf.helpers.DoubleHelper;
import com.volacious.jf.ui.fragment.uploadprocess.UIUpdateEvent;
import com.volacious.jf.ui.fragment.uploadprocess.UIUpdateInterface;
import com.volacious.jf.util.adapter.FileStatus;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

public class CopyStatusModel extends RealmObject implements UIUpdateInterface {

    private Date copyStartDate;
    private Date copyFinishDate;

    private long doneSize;
    private RealmList<FileCompleteModel> filesCompleted;
    private Long transferSpeed;

    private String copyFaliedExc;

    @LinkingObjects("copyStatusModel")
    private final RealmResults<ProgressStatusModel> parent = null;

    public Date getCopyStartDate() {
        return copyStartDate;
    }

    public void setCopyStartDate(Date copyStartDate) {
        this.copyStartDate = copyStartDate;
    }

    public Date getCopyFinishDate() {
        return copyFinishDate;
    }

    public void setCopyFinishDate(Date copyFinishDate) {
        this.copyFinishDate = copyFinishDate;
    }

    public long getDoneSize() {
        return doneSize;
    }

    public void setDoneSize(long doneSize) {
        this.doneSize = doneSize;
    }

    public RealmList<FileCompleteModel> getFilesCompleted() {
        return filesCompleted;
    }

    public void setFilesCompleted(RealmList<FileCompleteModel> filesCompleted) {
        this.filesCompleted = filesCompleted;
    }

    public Long getTransferSpeed() {
        return transferSpeed;
    }

    public Long safeGetTransferSpeed() {
        if (getTransferSpeed() == null) {
            return 0L;
        } else {
            return getTransferSpeed();
        }
    }

    public void setTransferSpeed(Long transferSpeed) {
        this.transferSpeed = transferSpeed;
    }

    public String getCopyFaliedExc() {
        return copyFaliedExc;
    }

    public void updateTransferSpeed() {
        Date localFinishDate = new Date();

        if (getCopyFinishDate() != null) {
            localFinishDate = getCopyFinishDate();
        }
        long diffInMs = localFinishDate.getTime() - getCopyStartDate().getTime();
        setTransferSpeed(doneSize * 1000 * 8 / diffInMs);
    }

    public void setCopyFaliedExc(String copyFaliedExc) {
        this.copyFaliedExc = copyFaliedExc;
    }

    public boolean isFileCompleted(File file) {
        if (filesCompleted != null) {
            for (FileCompleteModel fileComplete : filesCompleted) {
                if (fileComplete.getFile().equals(file.getPath())) {
                    return true;
                }
            }
        }
        return false;
    }

    public void addFileToCompleted(File file, Date start, Date end, Realm realm) {
        if (filesCompleted == null) {
            filesCompleted = new RealmList<>();
        }
        FileCompleteModel fileCompleteModel = realm.createObject(FileCompleteModel.class);
        fileCompleteModel.setFile(file.getPath());
        fileCompleteModel.setStart(start);
        fileCompleteModel.setEnd(end);
        fileCompleteModel.setSpeed(file.length() * 1000 * 8 / (end.getTime() - start.getTime()));
        filesCompleted.add(fileCompleteModel);
    }

    public void updateTransferSpeed(long txBytesBefore, long txBytesAfter, Date dateBefore, Date dateAfter) {
        long filesDone = getFilesCompleted() != null ? getFilesCompleted().size() : 0;
        long timeDiff = dateAfter.getTime() - dateBefore.getTime();
        long res = (txBytesAfter - txBytesBefore) * 8 * 1000 / timeDiff;
        long newRate = (safeGetTransferSpeed() * (filesDone - 1) + res) / filesDone;
        Log.i("TRRRRR", "" + (((double) safeGetTransferSpeed()) / 1024 / 1024) + " " + (((double) res) / 1024 / 1024));
        setTransferSpeed(newRate);
    }

    public long getFilesDone() {
        return getFilesCompleted() != null ? getFilesCompleted().size() : 0;
    }

    public long getFilesCount() {
        long count = 0;
        try {
            count = parent.first().getFilesCount();
        } catch (Exception ignored) {
        }
        return count;
    }

    public int getProgressPercent() {
        if (getFilesDone() == parent.first().getFilesCount()) {
            return 100;
        }
        int res = (int) (((double) getDoneSize() / parent.first().getSourceSize()) * 100);
        if (res < 0) {
            return 0;
        }
        return res;
    }

    public boolean isPossibleToContinue() {
        boolean result = true;
        if (getFilesDone() == parent.first().getFilesCount()) {
            result = false;
        }

        try {
            if (parent.first().getFiles() != null) {
                for (String fileStr : parent.first().getFiles()) {
                    File file = new File(fileStr);
                    if (!file.exists()) {
                        result = false;
                        break;
                    }
                }
            } else {
                result = false;
            }
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

//    public boolean isStopped() {
//        return !isFinished() && UploadProcessor.fileTransferInProgress != getId();  uploadProcessor?
//    }

    public boolean isFinished() {
        return getFilesDone() != 0 && getFilesDone() == getFilesCount();
    }

    @Override
    public UIUpdateEvent getUIUpdateEvent() {
        UIUpdateEvent updateEvent = new UIUpdateEvent();

        if (getCopyFinishDate() != null) {
            updateEvent.setUploadIconVisible(false);
            updateEvent.setUploadLabel(null);
            updateEvent.setUploadStatus(0);
            updateEvent.setFilesProgressStatus("" + (getFilesCount() - getFilesDone()) +
                    "/" + getFilesCount() + " files left");
            updateEvent.setLoadingIndicatorVisible(true);
            updateEvent.setNoteText(parent.first().getNoteText());

            updateEvent.setCopyStatus(2);
            updateEvent.setCopyIconVisible(true);
            updateEvent.setCopyLabel("Copied (" + DateHelper.getDateAsMDYHM(getCopyFinishDate()) + ")");
        } else {
            if (getCopyStartDate() == null) {
                updateEvent.setCopyStatus(0);
                // updateEvent.setStatusLabel("Waiting for copy");
                return updateEvent;
            } else {
                updateEvent.setCopyStatus(1);
                updateEvent.setStatusLabel("Copying in progress");
                updateEvent.setCopyLabel("Copied "+ getProgressPercent() + "%");
                updateEvent.setProgress(getProgressPercent());
                updateEvent.setSpeedIconVisible(true);
                updateEvent.setFilesProgressStatus("" + (parent.first().getFilesCount() - getFilesCompleted().size()) +
                        "/" + parent.first().getFilesCount() + " files left");
                    updateEvent.setSpeedLabel(DoubleHelper.asTwoNumbersAfterDot(((double) safeGetTransferSpeed()) / 1024 / 1024) + " Mb/s"); // ?? kbps
            }
        }

        ArrayList<FileStatus> fileStatuses = new ArrayList<>();
        RealmList<String> sourceFiles = parent.first().getSourceFiles();
        for (int i = 0; i < sourceFiles.size(); i++) {
            String fileName = sourceFiles.get(i);
            if (i < getFilesCompleted().size()) {
                fileStatuses.add(new FileStatus(fileName, FileStatus.Status.copyDone));
            } else {
                fileStatuses.add(new FileStatus(fileName, FileStatus.Status.none));
            }
        }
        updateEvent.setFileStatuses(fileStatuses);

        return updateEvent;
    }

    @Override
    public String toString() {
        return "CopyStatusModel{" +
                "copyStartDate=" + getCopyStartDate() +
                ", copyFinishDate=" + getCopyStartDate() +
                ", doneSize=" + getDoneSize() +
                ", filesCompleted=" + getFilesCompleted().toString() +
                ", transferSpeed=" + getTransferSpeed() +
                ", copyFaliedExc='" + getCopyFaliedExc() +
                '}';
    }
}
