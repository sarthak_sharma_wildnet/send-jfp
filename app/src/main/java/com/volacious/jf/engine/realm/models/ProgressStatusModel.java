package com.volacious.jf.engine.realm.models;


import com.volacious.jf.ui.fragment.uploadprocess.UIUpdateEvent;
import com.volacious.jf.ui.fragment.uploadprocess.UIUpdateInterface;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ProgressStatusModel extends RealmObject implements UIUpdateInterface {

    @PrimaryKey
    private int id;

    private int priority;

    private Date creationDate;

    private long totalSize;
    private long sourceSize;

    private RealmList<String> files;
    private RealmList<String> sourceFiles;

    private String notificationTitle;

    private CopyStatusModel copyStatusModel;
    private UploadStatusModel uploadStatusModel = null;

    private int photoshootId;

    private boolean isSuccessful;

    private String noteText;

    //  get\set
    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }

    public long getSourceSize() {
        return sourceSize;
    }

    public void setSourceSize(long sourceSize) {
        this.sourceSize = sourceSize;
    }

    public CopyStatusModel getCopyStatusModel() {
        return copyStatusModel;
    }

    public void setCopyStatusModel(CopyStatusModel copyStatusModel) {
        this.copyStatusModel = copyStatusModel;
    }

    public UploadStatusModel getUploadStatusModel() {
        return uploadStatusModel;
    }

    public void setUploadStatusModel(UploadStatusModel uploadStatusModel) {
        this.uploadStatusModel = uploadStatusModel;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(boolean successful) {
        isSuccessful = successful;
    }

    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }

    public RealmList<String> getSourceFiles() {
        return sourceFiles;
    }

    public void setSourceFiles(RealmList<String> sourceFiles) {
        this.sourceFiles = sourceFiles;
    }

    public RealmList<String> getFiles() {
        return files;
    }

    public List<File> getFileEntities() {
        List<File> res = new ArrayList<>();
        if (getFiles() == null) {
            return res;
        }
        try {
            for (String file : getFiles()) {
                res.add(new File(file));
            }
        } catch (Exception e) {
            return new ArrayList<>();
        }
        return res;
    }

    public void setFiles(RealmList<String> files) {
        this.files = files;
    }

    public int getPhotoshootId() {
        return photoshootId;
    }

    public void setPhotoshootId(int photoshootId) {
        this.photoshootId = photoshootId;
    }



    public long getFilesCount() {
        if(getUploadStatusModel() != null) {
            return getFiles() != null ? getFiles().size() : 0;
        } else {
            return getSourceFiles().size();
        }

    }

    @Override
    public UIUpdateEvent getUIUpdateEvent() {
        if(getUploadStatusModel() != null) {
            return getUploadStatusModel().getUIUpdateEvent();
        } else {
            return getCopyStatusModel().getUIUpdateEvent();
        }
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public ProgressStatusModel() {
        setPriority(0);
    }

    public boolean isReadyToUpload() {
        return getUploadStatusModel() != null && getUploadStatusModel().getFilesDone() < getFilesCount();
    }

    @Override
    public String toString() {
        return "ProgressStatusModel{" +
                "id=" + getId() +
                ", priority=" + getPriority() +
                ", creationDate=" + getCreationDate() +
                ", totalSize=" + getTotalSize() +
                ", files=" + getFiles() +
                ", sourceFiles=" + getSourceSize() +
                ", notificationTitle='" + getNotificationTitle() + '\'' +
                ", copyStatusModel=" + (getCopyStatusModel() != null ? getCopyStatusModel().toString(): "no model") +
                ", uploadStatusModel=" + (getUploadStatusModel() != null ? getUploadStatusModel().toString(): "no model") +
                ", photoshootId=" + getPhotoshootId() +
                ", isSuccessful=" + isSuccessful() +
                '}';
    }
}
