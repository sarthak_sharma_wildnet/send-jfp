package com.volacious.jf.engine.logger;

import com.volacious.jf.engine.logger.model.LogRequestResponse;
import com.volacious.jf.engine.logger.model.SimpleLogRequest;
import com.volacious.jf.engine.logger.model.logLevel.LogLevelResponse;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface LogAPI {

    @Headers({"Content-Type: application/json"})
    @GET("/api/log/level")
    Observable<LogLevelResponse> getLogLevel(@Query("app") String app);

    @Headers({"Content-Type: application/json"})
    @POST("/api/log/new")
    Observable<LogRequestResponse> sendNewLog(@Body SimpleLogRequest logRequest);
}
