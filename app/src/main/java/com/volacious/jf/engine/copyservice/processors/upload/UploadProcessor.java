package com.volacious.jf.engine.copyservice.processors.upload;

import android.net.TrafficStats;
import android.os.Process;

import com.volacious.jf.engine.APIManager;
import com.volacious.jf.engine.EnsureLoginCallback;
import com.volacious.jf.engine.SafeResponseCallback;
import com.volacious.jf.engine.copyservice.ServiceInterface;
import com.volacious.jf.engine.copyservice.SoundType;
import com.volacious.jf.engine.copyservice.processors.ServiceProcessor;
import com.volacious.jf.engine.copyservice.processors.ToastEvent;
import com.volacious.jf.engine.copyservice.processors.copy.ProcessStopEvent;
import com.volacious.jf.engine.logger.Logger;
import com.volacious.jf.engine.logger.model.logLevel.LogLevel;
import com.volacious.jf.engine.network.result.FileResult;
import com.volacious.jf.engine.network.result.FileTransfer;
import com.volacious.jf.engine.network.result.User;
import com.volacious.jf.engine.realm.RealmManager;
import com.volacious.jf.engine.realm.models.ProgressStatusModel;
import com.volacious.jf.engine.realm.models.UploadStatusModel;
import com.volacious.jf.engine.uploadservice.MD5Helper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observer;
import rx.Subscription;
import rx.schedulers.Schedulers;

public class UploadProcessor extends ServiceProcessor {

    public static int fileTransferInProgress = -1;

    private List<File> filesToUpload;
    private int fileTransferId;
    private String notificationTitle;

    public static boolean inProgress() {
        return fileTransferInProgress != -1;
    }

    public UploadProcessor(ServiceInterface serviceInterface, int fileTransferId) {
        super(serviceInterface);
        this.fileTransferId = fileTransferId;
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onStopEvent(ProcessStopEvent processStopEvent) {
        if (processStopEvent.getFileTransferId() == fileTransferId) {
            stopProcessor();
        }
    }

    @Override
    public String getNotificationTitle() {
        if (notificationTitle != null) {
            return notificationTitle;
        }
        return "J&F Upload";
    }

    @Override
    public String getStartNotification() {
        return "Upload: " + getNotificationTitle();
    }

    @Override
    public String getFinishNotification() {
        return "Upload finished: " + getNotificationTitle();
    }

    @Override
    public int getNotificationChannelId() {
        return fileTransferId;
    }

    @Override
    public void processFinished() {
        EventBus.getDefault().unregister(this);
        fileTransferInProgress = -1;
        getServiceInterface().playSound(SoundType.UPLOAD_FINISHED);
    }

    private long txBytes = 0;
    private Date txBytesDate = null;

    @Override
    public void process() {
        fileTransferInProgress = fileTransferId;
        getServiceInterface().playSound(SoundType.UPLOAD_STARTED);

        isStopping = false;
        updateNotification("Preparing upload");
        ProgressStatusModel progressStatusModel = RealmManager.getRealmInstance()
                .where(ProgressStatusModel.class)
                .equalTo("id", fileTransferId)
                .findFirst();

        RealmManager.getRealmInstance().executeTransaction(realm -> {
            ProgressStatusModel progressModel = realm.where(ProgressStatusModel.class).equalTo("id",fileTransferId).findFirst();
            UploadStatusModel uploadModel = progressModel.getUploadStatusModel();
            uploadModel.setUploadStartDate(new Date());
        });

        notificationTitle = progressStatusModel.getNotificationTitle();

        getServiceInterface().processStarted(this);

        updateNotification("Uploading " +
                progressStatusModel.getUploadStatusModel().getFilesDone() + "/" +
                progressStatusModel.getFilesCount() + " (" +
                progressStatusModel.getUploadStatusModel().getProgressPercent() + "%)");

        filesToUpload = progressStatusModel.getFileEntities();

        ProgressStatusModel progressModelLog = RealmManager.getRealmInstance().where(ProgressStatusModel.class).equalTo("id",fileTransferId).findFirst();
        Logger.log("Upload process init with: " + progressModelLog.toString());

        //TOUCH MODEL
        RealmManager.executeRealmTransactionSync(realm -> {
            ProgressStatusModel statusModel = realm
                    .where(ProgressStatusModel.class)
                    .equalTo("id", fileTransferId)
                    .findFirst();
            statusModel.setNotificationTitle(statusModel.getNotificationTitle());
        });
        //------------

        APIManager.ensureLogin(new EnsureLoginCallback() {
            @Override
            public void loginSuccess(User user) {
                startUploading();
            }

            @Override
            public void loginFailed() {
                EventBus.getDefault().post(new ToastEvent("Upload failed. Unable to login.", null));
                updateNotification("Upload failed");
                getServiceInterface().processFinished(UploadProcessor.this, null);

                ProgressStatusModel progressModelLog = RealmManager.getRealmInstance().where(ProgressStatusModel.class).equalTo("id",fileTransferId).findFirst();
                Logger.log("Upload failed. Unable to login, model: " + progressModelLog.toString());

            }
        }, UploadProcessorThreadHelper.getScheduler());
    }

    private void startUploading() {
        txBytes = TrafficStats.getUidTxBytes(Process.myUid());
        txBytesDate = new Date();
        subscriptions = new HashMap<>();
        for(int i = 0; i < uploadLimit; i++) {
            onNextFileUpload();
        }
    }

    private int retryCount = 0;

    private void forceStop() {
        //EventBus.getDefault().post(new UploadStopEvent(fileTransfer));

        ProgressStatusModel progressModelLog = RealmManager.getRealmInstance().where(ProgressStatusModel.class).equalTo("id",fileTransferId).findFirst();
        Logger.log("Upload stopped: " + progressModelLog.toString());

        updateNotification("Upload stopped");
        EventBus.getDefault().post(new ToastEvent(null, "Upload stopped"));
        processFinished();
    }

    private void clearFiles() {
        ProgressStatusModel progressStatusModel = RealmManager.getRealmInstance()
                .where(ProgressStatusModel.class)
                .equalTo("id", fileTransferId)
                .findFirst();

        if (progressStatusModel == null) {
            return;
        }

        List<File> files = progressStatusModel.getFileEntities();

        if (files != null && files.size() > 0) {
            File first = files.get(0);
            File dir = first.getParentFile();
            for (File file : dir.listFiles()) {
                file.delete();
            }
            dir.delete();
        }
    }

    @Override
    public void stopProcessor() {
        super.stopProcessor();
        for (Subscription subscription : subscriptions.values()) {
            subscription.unsubscribe();
        }
        subscriptions = new HashMap<>();
        forceStop();
    }

    private static Map<File, Subscription> subscriptions = new HashMap<>();

    public static Set<File> getFilesInProgress() {
        return subscriptions.keySet();
    }

    private final int uploadLimit = 12;

    private void onNextFileUpload() {
        if (getFilesInProgress().size() >= uploadLimit) {
            //Limit of parallel uploads reached. Wait for next file upload finish.
            return;
        }

        //GETTING NEXT FILE
        File newFile = null;

        ProgressStatusModel progressStatusModel = RealmManager.getRealmInstance()
                .where(ProgressStatusModel.class)
                .equalTo("id", fileTransferId)
                .findFirst();

        for (File f : filesToUpload) {
            if (!progressStatusModel.getUploadStatusModel().isFileCompleted(f) &&
                !getFilesInProgress().contains(f)) {
                newFile = f;
                break;
            }
        }

        if (newFile == null) {
            //No more files in queue.
            if (getFilesInProgress().size() == 0) {
                //UPLOAD FINISHED
                finalizeUpload();
            }
            return;
        }

        //START NEXT FILE
        final File file = newFile;

        RequestBody fbody = RequestBody.create(MediaType.parse("image/*"), file);
        String check_sum = MD5Helper.fileToMD5(file);
        if (check_sum == null) {
            EventBus.getDefault().post(new ToastEvent("unable to read file", null));
            stopProcessor();
            return;
        }
        RequestBody checksum = RequestBody.create(MediaType.parse("text/plain"), check_sum);

        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), fbody);

        Date dateStart = new Date();

        Subscription subscription = APIManager.retrofitService.uploadFile(fileTransferId, checksum, body)
                .subscribeOn(Schedulers.newThread())
                .observeOn(UploadProcessorThreadHelper.getScheduler())
                .subscribe(new Observer<FileResult>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        subscriptions.remove(file);
                        if (isStopping()) {
                            return;
                        }
                        onNextFileUpload();
                        EventBus.getDefault().post(new ToastEvent("Upload failed. Retrying...", null));

                        ProgressStatusModel progressModelLog = RealmManager.getRealmInstance().where(ProgressStatusModel.class).equalTo("id",fileTransferId).findFirst();
                        Logger.log("Upload failed with: " + progressModelLog.toString());
                        /*if (retryCount < 3) {
                            onNextFileUpload(fileTransfer);
                            EventBus.getDefault().post(new ToastEvent("Upload failed. Retrying...", null));
                        } else {
                            EventBus.getDefault().post(new ToastEvent("Upload failed. Stop uploading.", null));
                            updateNotification("Upload failed");
                            getServiceInterface().processFinished(UploadProcessor.this, null);
                        }
                        retryCount++;*/
                    }

                    @Override
                    public void onNext(FileResult fileResult) {
                        if (isStopping()) {
                            return;
                        }
                        subscriptions.remove(file);
                        retryCount = 0;

                        RealmManager.getRealmInstance().executeTransaction(realm -> {
                            Date endDate = new Date();
                            ProgressStatusModel statusModel = realm.where(ProgressStatusModel.class).equalTo("id", fileTransferId).findFirst();
                            UploadStatusModel uploadModel = statusModel.getUploadStatusModel();
                            uploadModel.addFileToCompleted(file, dateStart, endDate, realm);
                            uploadModel.setDoneSize(uploadModel.getDoneSize() + file.length());

                            statusModel.setSuccessful(uploadModel.getDoneSize() == statusModel.getTotalSize());

                            long newBytes = TrafficStats.getUidTxBytes(Process.myUid());
                            Date newBytesDate = new Date();
                            uploadModel.updateTransferSpeed(txBytes, newBytes, txBytesDate, newBytesDate);
                            txBytes = newBytes;
                            txBytesDate = newBytesDate;

                            updateNotification("Uploading " +
                                    uploadModel.getFilesDone() + "/" +
                                    uploadModel.getFilesCount() + " (" +
                                    uploadModel.getProgressPercent() + "%)");
                        });

                        ProgressStatusModel progressModelLog = RealmManager.getRealmInstance().where(ProgressStatusModel.class).equalTo("id",fileTransferId).findFirst();
                        Logger.log("File uploaded: "+ file.getName() + " with: " + progressModelLog.toString(), LogLevel.verbose);

                        onNextFileUpload();
                    }
                });
        subscriptions.put(file, subscription);
    }

    private void finalizeUpload() {

        RealmManager.executeRealmTransactionSync(realm -> {
            ProgressStatusModel progressModel = RealmManager.getRealmInstance().where(ProgressStatusModel.class).equalTo("id",fileTransferId).findFirst();
            progressModel.getUploadStatusModel().setUploadFinishDate(new Date());
            Logger.log("Upload finished: " + progressModel.toString());
        });

        APIManager.retrofitService.getFileTransfer(fileTransferId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(UploadProcessorThreadHelper.getScheduler())
                .subscribe(new SafeResponseCallback<FileTransfer>() {
                    @Override
                    public void onError(Throwable e) {
                        EventBus.getDefault().post(new ToastEvent(null, "Upload finished"));
                        updateNotification("Upload finished");
                        getServiceInterface().processFinished(UploadProcessor.this, null);
                    }

                    @Override
                    public void onNext(FileTransfer fileTransfer) {
                        ProgressStatusModel progressStatusModel = RealmManager.getRealmInstance()
                                .where(ProgressStatusModel.class)
                                .equalTo("id", fileTransferId)
                                .findFirst();
                        long sp = progressStatusModel.getUploadStatusModel().getTransferSpeed() * 8;
                        fileTransfer.setAvgBitsPerSecond((int) sp);
                        fileTransfer.sendUpdate();
                        clearFiles();
                        EventBus.getDefault().post(new ToastEvent(null, "Upload finished"));
                        updateNotification("Upload finished");
                        getServiceInterface().processFinished(UploadProcessor.this, null);
                    }
                });
    }
}
