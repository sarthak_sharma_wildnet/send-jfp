package com.volacious.jf.engine.cache;

import androidx.annotation.Nullable;

import com.volacious.jf.engine.network.result.IDInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CacheManager {

    private static CacheManager instance;
    private Map<String, List<IDInterface>> cache = new HashMap<>();

    private CacheManager() {
    }

    public static CacheManager getInstance() {
        if (instance == null) {
            instance = new CacheManager();
        }
        return instance;
    }

    public void cacheObjects(List objects) {
        if (objects.size() > 0) {
            if (objects.get(0) instanceof IDInterface) {
                String className = objects.get(0).getClass().getName();
                List<IDInterface> list = cache.get(className);
                if (list == null) {
                    list = new ArrayList<>();
                }
                for (Object object : objects) {
                    IDInterface idObject = (IDInterface) object;
                    list.remove(idObject);
                    list.add(idObject);
                }
                cache.put(className, list);
            }
        }
    }

    public void cacheObject(IDInterface object) {
        String className = object.getClass().getName();
        List<IDInterface> list = cache.get(className);
        if (list == null) {
            list = new ArrayList<>();
        }
        list.remove(object);
        list.add(object);
        cache.put(className, list);
    }

    public @Nullable
    <E extends IDInterface> E getObject(Class<E> clazz, int id) {
        String clazzName = clazz.getName();
        List<IDInterface> list = cache.get(clazzName);
        if (list != null) {
            for (IDInterface idInterface : list) {
                if (idInterface.getId() == id) {
                    return (E) idInterface;
                }
            }
        }
        return null;
    }
}
