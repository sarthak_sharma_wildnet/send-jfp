package com.volacious.jf.engine.copyservice.processors.copy;


import com.volacious.jf.engine.cache.CacheManager;
import com.volacious.jf.engine.copyservice.ServiceInterface;
import com.volacious.jf.engine.copyservice.SoundType;
import com.volacious.jf.engine.copyservice.processors.ServiceProcessor;
import com.volacious.jf.engine.copyservice.processors.ToastEvent;
import com.volacious.jf.engine.copyservice.processors.upload.UploadProcessor;
import com.volacious.jf.engine.logger.Logger;
import com.volacious.jf.engine.logger.model.logLevel.LogDestination;
import com.volacious.jf.engine.logger.model.logLevel.LogLevel;
import com.volacious.jf.engine.network.result.FileTransfer;
import com.volacious.jf.engine.network.result.Photoshoot;
import com.volacious.jf.engine.realm.RealmManager;
import com.volacious.jf.engine.realm.models.CopyStatusModel;
import com.volacious.jf.engine.realm.models.ProgressStatusModel;
import com.volacious.jf.engine.realm.models.UploadStatusModel;
import com.volacious.jf.util.adapter.storages.UploadStorage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;

public class CopyProcessor extends ServiceProcessor {

    public static int fileTransferInProgress = -1;
    private FileTransfer fileTransfer;
    private List<FileInterface> sourceFiles;
    private static int retryLimit = 10;

    public static boolean inProgress() {
        return fileTransferInProgress != -1;
    }

    public CopyProcessor(ServiceInterface serviceInterface, FileTransfer fileTransfer, UploadStorage uploadStorage) {
        super(serviceInterface);
        this.sourceFiles = uploadStorage.getSourceFiles();
        this.fileTransfer = fileTransfer;
        EventBus.getDefault().register(this);
//        Logger.logDestination = LogDestination.both;
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onStopEvent(ProcessStopEvent processStopEvent) {
        if (processStopEvent.getFileTransferId() == fileTransfer.getId()) {
            stopProcessor();
        }
    }

    @Override
    public String getNotificationTitle() {
        if (fileTransfer != null) {
            int photoshootid = fileTransfer.getPhotoshootId();
            Photoshoot photoshoot = CacheManager.getInstance().getObject(Photoshoot.class, photoshootid);
            if (photoshoot != null && photoshoot.getProperty() != null) {
                return photoshoot.getProperty().getAddress();
            }
        }
        return "J&F Copy";
    }

    @Override
    public String getStartNotification() {
        return "Copy: " + getNotificationTitle();
    }

    @Override
    public String getFinishNotification() {
        return "Copy finished: " + getNotificationTitle();
    }

    @Override
    public int getNotificationChannelId() {
        return fileTransfer.getId();
    }

    @Override
    public void processFinished() {
        EventBus.getDefault().unregister(this);
        fileTransferInProgress = -1;
        getServiceInterface().playSound(SoundType.COPY_FINISHED);
    }

    @Override
    public void process() {
        getServiceInterface().processStarted(this);
        Logger.log("copy started, fid:" + fileTransfer.getId() +
                ", photoshootId:" + fileTransfer.getPhotoshootId());

        getServiceInterface().playSound(SoundType.COPY_STARTED);
        updateNotification("Preparing to start copy");

        RealmManager.getRealmInstance().executeTransaction(realm -> {
            ProgressStatusModel progressModel = realm.createObject(ProgressStatusModel.class, fileTransfer.getId());
            progressModel.setCreationDate(new Date());

            RealmList<String> fileNames = new RealmList<>();
            long sourceSize = 0;
            for (FileInterface file : sourceFiles) {
                fileNames.add(file.getName());
                sourceSize += file.length();
            }
            progressModel.setSourceFiles(fileNames);
            progressModel.setSourceSize(sourceSize);

            CopyStatusModel copyModel = realm.createObject(CopyStatusModel.class);
            copyModel.setCopyStartDate(new Date());
            progressModel.setCopyStatusModel(copyModel);

            int photoshootid = fileTransfer.getPhotoshootId();
            progressModel.setPhotoshootId(photoshootid);

            updateNotification("Copy progress: " +
                    copyModel.getFilesDone() + "/" +
                    copyModel.getFilesCount() + " " +
                    copyModel.getProgressPercent() + "%");

        });

        ProgressStatusModel progressModelLog = RealmManager.getRealmInstance().where(ProgressStatusModel.class).equalTo("id", fileTransfer.getId()).findFirst();
        Logger.log("Copy process init with: " + progressModelLog.toString());

        fileTransfer.setCopyStartTime(new Date());
        fileTransferInProgress = fileTransfer.getId();
        fileTransfer.sendUpdate();
        CacheManager.getInstance().cacheObject(fileTransfer);
        File appDir = getServiceInterface().getContext().getFilesDir();
        String dirName = "" + fileTransfer.getId();
        File photoshootDir = new File(appDir, dirName);

        List<File> resultFiles = new ArrayList<>();
        if (photoshootDir.mkdir()) {
            int tryCount = 0;
            for (FileInterface file : sourceFiles) {
                try {
                    if (isStopping()) {
                        forceStop(photoshootDir, resultFiles);
                        return;
                    }
                    File newFile = new File(photoshootDir, file.getName());
                    Date startCopyTime = new Date();
                    file.copyFile(newFile);
                    Date endCopyTime = new Date();
                    resultFiles.add(newFile);


                    //TODO: to comment
//                    try {
//                        Thread.sleep(3000);
//                    } catch (InterruptedException e1) {
//                        break;
//                    }
                    //

                    RealmManager.getRealmInstance().executeTransaction(realm -> {
                        ProgressStatusModel progressModel = realm.where(ProgressStatusModel.class)
                                .equalTo("id", fileTransfer.getId()).findFirst();
                        CopyStatusModel copyModel = progressModel.getCopyStatusModel();
                        copyModel.setDoneSize(copyModel.getDoneSize() + file.length());
                        copyModel.addFileToCompleted(newFile, startCopyTime, endCopyTime, realm);
                        copyModel.updateTransferSpeed();
                        updateNotification("Copy progress: " +
                                copyModel.getFilesDone() + "/" +
                                copyModel.getFilesCount() + " " +
                                copyModel.getProgressPercent() + "%");

                    });

                    Logger.log("File copied: " + file.getName() + " with \n" + progressModelLog.toString(), LogLevel.verbose);


                } catch (Exception e) {
                    e.printStackTrace();
                    RealmManager.getRealmInstance().executeTransaction(realm -> {
                        ProgressStatusModel progressModel = realm.where(ProgressStatusModel.class)
                                .equalTo("id", fileTransfer.getId()).findFirst();
                        CopyStatusModel copyModel = progressModel.getCopyStatusModel();
                        copyModel.setCopyFaliedExc(e.getLocalizedMessage());
                    });
                    if (tryCount < retryLimit) {
                        tryCount++;
                        Logger.log("FAILED COPY " + tryCount + "/" + retryLimit);
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e1) {}
                    }
                }
                tryCount = 0;
            }

            if (tryCount >= retryLimit) {
                failedStop(photoshootDir, resultFiles);
                Logger.log("Copy failed with: " + progressModelLog.toString());
                return;
            }
        }
        updateNotification("Copy finishing...");
        fileTransfer.setFilesToUpload(resultFiles);
        //File doneDir = new File(appDir, "photoshoot_" + fileTransfer.getId());
        //photoshootDir.renameTo(doneDir);

        RealmManager.executeRealmTransactionSync(realm -> {
            ProgressStatusModel progressModel = realm.where(ProgressStatusModel.class).equalTo("id", fileTransfer.getId()).findFirst();
            RealmList<String> files = new RealmList<>();
            for (File resultFile : resultFiles) {
                files.add(resultFile.getPath());
            }
            progressModel.setFiles(files);
            progressModel.setTotalSize(progressModel.getCopyStatusModel().getDoneSize());

            UploadStatusModel uploadModel = realm.createObject(UploadStatusModel.class);
            uploadModel.setTransferSpeed(0L);
            uploadModel.setDoneSize(0L);
            progressModel.setUploadStatusModel(uploadModel);

            Photoshoot photoshoot = CacheManager.getInstance().getObject(Photoshoot.class, progressModel.getPhotoshootId());
            if (photoshoot != null && photoshoot.getProperty() != null) {
                progressModel.setNotificationTitle(photoshoot.getProperty().getAddress());
            }
        });

        Logger.log("Copy finished: " + progressModelLog.toString());

        fileTransfer.setCopyEndTime(new Date());
        fileTransfer.sendUpdate();
        EventBus.getDefault().post(new ToastEvent(null, "Copy finished"));

        getServiceInterface().processFinished(CopyProcessor.this,
                new UploadProcessor(getServiceInterface(), fileTransfer.getId()));
    }

    private void failedStop(File photoshootDir, List<File> filesToDelete) {
        updateNotification("Terminating copy...");
        EventBus.getDefault().post(new ToastEvent(null, "Terminating copy"));
        for (File file : filesToDelete) {
            file.delete();
        }
        photoshootDir.delete();
        updateNotification("Copy failed");
        EventBus.getDefault().post(new ToastEvent("Copy failed", null));
        getServiceInterface().processFinished(this, null);

        ProgressStatusModel progressModelLog = RealmManager.getRealmInstance().where(ProgressStatusModel.class).equalTo("id", fileTransfer.getId()).findFirst();
        Logger.log("Copy failed STOP: " + progressModelLog.toString());
    }

    private void forceStop(File photoshootDir, List<File> filesToDelete) {
        updateNotification("Terminating copy...");
        EventBus.getDefault().post(new ToastEvent(null, "Terminating copy"));
        for (File file : filesToDelete) {
            file.delete();
        }
        photoshootDir.delete();
        updateNotification("Copy stopped");
        EventBus.getDefault().post(new ToastEvent(null, "Copy stopped"));
        getServiceInterface().processFinished(this, null);

        ProgressStatusModel progressModelLog = RealmManager.getRealmInstance().where(ProgressStatusModel.class).equalTo("id", fileTransfer.getId()).findFirst();
        Logger.log("Copy force STOP: " + progressModelLog.toString());
    }
}
