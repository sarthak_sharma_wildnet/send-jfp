package com.volacious.jf.engine.logger.model;

import java.util.Date;

public class SimpleLogRequest {
    private String id;
    private String item;
    private String token;
    private Date date;

    public SimpleLogRequest(String id, String item, String token, Date date) {
        this.id = id;
        this.item = item;
        this.token = token;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public String getItem() {
        return item;
    }

    public String getToken() {
        return token;
    }

    public Date getDate() {
        return date;
    }
}
