package com.volacious.jf.engine.copyservice.processors.upload;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

public class FileComplete implements Serializable {
    private File file;
    private Date start;
    private Date end;
    private Long speed;

    public FileComplete(File file, Date start, Date end) {
        this.file = file;
        this.start = start;
        this.end = end;
    }

    public File getFile() {
        return file;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    public long getTransferSpeed() {
        if (speed == null) {
            speed = file.length() * 1000 * 8 / (end.getTime() - start.getTime());
        }
        return speed;
    }
}
