package com.volacious.jf.engine.network.result;

import com.google.gson.annotations.SerializedName;

public class Property {
    @SerializedName("id")
    private int id;

    @SerializedName("type")
    private String type;

    @SerializedName("address")
    private String address;

    @SerializedName("unit")
    private String unit;

    @SerializedName("zipcode")
    private String zipcode;

    @SerializedName("region")
    private String region;

    @SerializedName("sqFt")
    private int sqFt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getSqFt() {
        return sqFt;
    }

    public void setSqFt(int sqFt) {
        this.sqFt = sqFt;
    }
}
