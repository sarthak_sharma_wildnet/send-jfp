package com.volacious.jf.engine.network.result;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ManifestItem implements Serializable {
    @SerializedName("filename")
    private String filename;

    @SerializedName("size")
    private long size;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
