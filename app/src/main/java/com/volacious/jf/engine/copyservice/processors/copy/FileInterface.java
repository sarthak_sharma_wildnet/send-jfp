package com.volacious.jf.engine.copyservice.processors.copy;

import java.io.File;
import java.io.Serializable;

public interface FileInterface extends Serializable {
    String getName();
    long length();
    void copyFile(File targetLocation) throws Exception;
}
