package com.volacious.jf.engine.realm;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Nik on 15.07.2018.
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface AutoDelete {
}
