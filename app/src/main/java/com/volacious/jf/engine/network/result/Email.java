package com.volacious.jf.engine.network.result;

import com.google.gson.annotations.SerializedName;

public class Email {

    @SerializedName("id")
    private int id;

    @SerializedName("value")
    private String value;

    @SerializedName("primary")
    private boolean primary;

    @SerializedName("status")
    private String status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
