package com.volacious.jf.engine.usb;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Log;

import androidx.annotation.Nullable;

import com.github.mjdev.libaums.UsbMassStorageDevice;
import com.github.mjdev.libaums.fs.FileSystem;
import com.github.mjdev.libaums.fs.UsbFile;
import com.volacious.jf.engine.copyservice.processors.copy.FileInterface;
import com.volacious.jf.engine.copyservice.processors.copy.UsbFileImpl;
import com.volacious.jf.util.toast.ToastManager;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class USBHelper {
    private static USBHelper instance;
    private USBContextHolder contextHolder;
    public static final String ACTION_USB_PERMISSION = "com.github.mjdev.libaums.USB_PERMISSION";
    private static final String TAG = USBHelper.class.getSimpleName();


    private USBHelper(USBContextHolder contextHolder) {
        this.contextHolder = contextHolder;
    }

    public static USBHelper getInstance() {
        return instance;
    }

    public static void initUSBHelper(USBContextHolder contextHolder) {
        instance = new USBHelper(contextHolder);
        instance.discoverDevice();
    }


    private UsbMassStorageDevice availableDevice;

    public UsbMassStorageDevice getAvailableDevice() {
        return availableDevice;
    }

    public void setAvailableDevice(UsbMassStorageDevice availableDevice) {
        if (this.availableDevice != availableDevice) {
            this.availableDevice = availableDevice;
            ToastManager.getInstance().showSuccess(availableDevice != null ? "USB Storage found" : "USB Storage removed");
            EventBus.getDefault().post(new USBEvent());
        }
    }

    UsbMassStorageDevice[] massStorageDevices;

    public void discoverDevice() {
        Context context = getContextHolder().getUSBContext();
        UsbManager usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
        massStorageDevices = UsbMassStorageDevice.getMassStorageDevices(context);

        if (massStorageDevices.length == 0) {
            return;
        }

        for (UsbMassStorageDevice massStorageDevice : massStorageDevices) {
            UsbDevice usbDevice = massStorageDevice.getUsbDevice();
            if (usbDevice != null && usbManager.hasPermission(usbDevice)) {
                setupDevice(usbDevice);
            } else {
                PendingIntent permissionIntent = PendingIntent.getBroadcast(getContextHolder().getUSBContext(), 0, new Intent(
                        ACTION_USB_PERMISSION), 0);
                usbManager.requestPermission(usbDevice, permissionIntent);
            }
        }
    }

    private void setupDevice(UsbDevice usbDevice) {
        try {
            UsbMassStorageDevice device = null;
            for (UsbMassStorageDevice massStorageDevice : massStorageDevices) {
                if (massStorageDevice.getUsbDevice().equals(usbDevice)) {
                    device = massStorageDevice;
                    break;
                }
            }
            if (device == null) {
                return;
            }

            device.init();

            // we always use the first partition of the device
            FileSystem currentFs = device.getPartitions().get(0).getFileSystem();
            UsbFile root = currentFs.getRootDirectory();
            UsbFile dcimFolder = searchUSB(root);
            if (dcimFolder != null) {
                List<FileInterface> files = filesList(dcimFolder);
                setAvailableDevice(device);
            }
        } catch (IOException e) {
            Log.e(TAG, "error setting up device", e);
        }
    }


    public USBContextHolder getContextHolder() {
        return contextHolder;
    }

    private final BroadcastReceiver usbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            setAvailableDevice(null);
            if (ACTION_USB_PERMISSION.equals(action)) {
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                    if (device != null) {
                        setupDevice(device);
                    }
                }
            } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                discoverDevice();
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                discoverDevice();
            }
        }
    };

    @Nullable
    private static UsbFile searchUSB(UsbFile root) {
        if (root.isDirectory()) {
            if (root.getName().equals("DCIM")) {
                return root;
            }
            try {
                UsbFile[] files = root.listFiles();
                for (UsbFile file : files) {
                    if (file.isDirectory()) {
                        UsbFile res = searchUSB(file);
                        if (res != null) {
                            return res;
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return null;
    }

    public BroadcastReceiver getUsbReceiver() {
        return usbReceiver;
    }

    public static long getFilesSize(UsbMassStorageDevice device) {
        FileSystem currentFs = device.getPartitions().get(0).getFileSystem();
        UsbFile root = currentFs.getRootDirectory();
        UsbFile dcimFolder = searchUSB(root);
        if (dcimFolder != null) {
            List<FileInterface> files = filesList(dcimFolder);
            return usbFileSize(dcimFolder);
        }
        return 0;
    }

    @Nullable
    public static List<FileInterface> getFilesList(UsbMassStorageDevice device) {
        FileSystem currentFs = device.getPartitions().get(0).getFileSystem();
        UsbFile root = currentFs.getRootDirectory();
        UsbFile dcimFolder = searchUSB(root);
        if (dcimFolder != null) {
            return filesList(dcimFolder);
        }
        return null;
    }

    @Nullable
    private static List<FileInterface> filesList(UsbFile sourceLocation) {
        List<FileInterface> res = new ArrayList<>();
        try {
            UsbFile[] files = sourceLocation.listFiles();
            for (UsbFile file : files) {
                if (file.isDirectory()) {
                    res.addAll(filesList(file));
                } else {
                    res.add(new UsbFileImpl(file));
                }
            }
            return res;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static long usbFileSize(UsbFile usbFile) {
        if (!usbFile.isDirectory()) {
            return usbFile.getLength();
        } else {
            try {
                long res = 0;
                for (UsbFile file : usbFile.listFiles()) {
                    res += usbFileSize(file);
                }
                return res;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }
}
