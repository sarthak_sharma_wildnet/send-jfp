package com.volacious.jf.engine.usb;

import com.github.mjdev.libaums.fs.UsbFile;

public class USBDriveInfo {
    private String driveName;
    private String absolutePath;
    private UsbFile usbFile;

    public USBDriveInfo(String driveName, String absolutePath, UsbFile usbFile) {
        this.driveName = driveName;
        this.absolutePath = absolutePath;
        this.usbFile = usbFile;
    }

    public String getDriveName() {
        return driveName;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public UsbFile getUsbFile() {
        return usbFile;
    }
}
