package com.volacious.jf.engine.realm.models;

import java.util.Date;

import io.realm.RealmObject;

public class FileCompleteModel extends RealmObject {
    private String file;
    private Date start;
    private Date end;
    private Long speed;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Long getSpeed() {
        return speed;
    }

    public void setSpeed(Long speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "\nFileCompleteModel{" +
                "file='" + getFile() + '\'' +
                ", start=" + getStart() +
                ", end=" + getEnd() +
                ", speed=" + getSpeed() +
                '}';
    }
}
