package com.volacious.jf.engine.realm;

import android.os.Looper;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Nik on 08.09.2018.
 */
public class RealmResultsDataSource<T extends RealmResults> implements DataSourceDisposable {
    private Disposable disposable;
    private int realmId = RealmManager.BASE_REALM;

    public RealmResultsDataSource() {}

    public RealmResultsDataSource(int realmId) {
        this.realmId = realmId;
    }

    private boolean returnToMainThread = true;

    public void dispose() {
        if (returnToMainThread) {
            disposeWorker();
        } else {
            DataSourceThreadHelper.getHandler().post(this::disposeWorker);
        }
    }

    private void disposeWorker() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
            disposable = null;
        }
    }

    public RealmResultsDataSource<T> mainThread(boolean returnToMainThread) {
        this.returnToMainThread = returnToMainThread;
        return this;
    }

    public RealmResultsDataSource<T> onChange(Consumer<T> onChange) {
        this.onChange = onChange;
        return this;
    }

    private Consumer<T> onChange;

    public RealmResultsDataSource<T> observe(RealmResultsGetterInterface<T> getter) {
        if (returnToMainThread) {
            RealmManager.executeWithRealm(realm ->
                    disposable = getter
                            .getObject(realm)
                            .asFlowable()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(onChange), realmId);
        } else {
            DataSourceThreadHelper.getHandler().post(() -> {
                boolean mt = Looper.myLooper() != Looper.getMainLooper();

                    RealmManager.executeWithRealm(realm ->
                    disposable = getter
                            .getObject(realm)
                            .asFlowable()
                            .observeOn(DataSourceThreadHelper.getScheduler())
                            .subscribe(onChange), realmId);
            });
        }
        return this;
    }

    public interface RealmResultsGetterInterface<T> {
        T getObject(Realm realm);
    }
}
