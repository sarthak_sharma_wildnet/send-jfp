package com.volacious.jf.engine.network;

import com.volacious.jf.engine.network.result.FileResult;
import com.volacious.jf.engine.network.result.FileTransfer;
import com.volacious.jf.engine.network.result.Photoshoot;
import com.volacious.jf.engine.network.result.User;


import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface VolaciousAPI {
    @Headers({"Content-Type: application/json"})
    @POST("/api/login")
    Observable<User> login(@Query("email") String email, @Query("password") String password);

    @GET("/api/photogs/{user_id}/photoshoots")
    Observable<ArrayList<Photoshoot>> getPhotoshoots(@Path(value = "user_id", encoded = true) int userId);

    @POST("/api/photoshoots/{photoshoot_id}/file-transfers")
    Observable<FileTransfer> createFileTransfer(@Path(value = "photoshoot_id", encoded = true) int photoshootId,
                                                @Body Map<String, Object> params);

    @GET("/api/photogs/{user_id}/file-transfers")
    Observable<ArrayList<FileTransfer>> getUserFileTransfers(@Path(value = "user_id", encoded = true) int userId);

    @Multipart
    @POST("/api/file-transfers/{filetransfer_id}/files")
    Observable<FileResult> uploadFile(@Path(value = "filetransfer_id", encoded = true) int filetransferId,
                                      @Part("checksum") RequestBody checksum,
                                      @Part MultipartBody.Part image);

    @PUT("/api/file-transfers/{ft_id}")
    Call<ResponseBody> updateFileTransfer(
            @Path(value = "ft_id", encoded = true) int filetransferId,
            @Body FileTransfer fileTransfer);

    @GET("/api/file-transfers/{ft_id}")
    Observable<FileTransfer> getFileTransfer(
            @Path(value = "ft_id", encoded = true) int filetransferId);
}
