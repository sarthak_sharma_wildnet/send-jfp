package com.volacious.jf.engine.logger.model.logLevel;

public class LogLevelResponse {
    private LogLevel logLevel;
    private LogDestination logDestination;
    private String token;

    public LogLevelResponse(LogLevel logLevel, LogDestination logDestination, String token) {
        this.logLevel = logLevel;
        this.logDestination = logDestination;
        this.token = token;
    }

    public LogLevel getLogLevel() {
        return logLevel;
    }

    public LogDestination getLogDestination() {
        return logDestination;
    }

    public String getToken() {
        return token;
    }
}
