package com.volacious.jf.engine.realm;

/**
 * Created by Nik on 08.09.2018.
 */
public interface DataSourceDisposable {
    void dispose();
}
