package com.volacious.jf.engine;

import java.util.Random;

public class RandomGenerator {

    private static Random randomGenerator;

    public static int getRandomIndex (int length){
        randomGenerator = new Random();
        return randomGenerator.nextInt(length);
    }

}
