package com.volacious.jf.engine.realm;

import androidx.annotation.Nullable;

public class Equalizer {
    public static boolean equals(@Nullable Object o1, @Nullable Object o2) {
        if (o1 == null && o2 == null) {
            return true;
        }
        if (o1 != null) {
            return o1.equals(o2);
        }
        return false;
    }
}
