package com.volacious.jf.engine.realm;

import android.util.Log;

import com.volacious.jf.helpers.PreferencesHelper;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmObject;

public class RealmManager {

    public static final int BASE_REALM = 2;

    private static RealmConfiguration realmConfiguration;

    private static List<WeakReference<RealmChangeDelegate>> realmChangeDelegateWeakReferences = new ArrayList<>();

    public static void safeExecute(@Nullable Realm realm, RealmTask realmTask) {
        if (realm != null) {
            realmTask.execute(realm);
        } else {
            executeRealmTransactionAsync(realm1 -> realmTask.execute(realm1));
        }
    }

    public static void executeRealmTransactionSync(Realm.Transaction transaction) {
        Realm realm = getRealmInstance();
        if (realm != null) {
            realm.executeTransaction(transaction);
        }
    }

    public static void executeRealmTransactionAsync(Realm.Transaction transaction) {
        executeRealmTransactionAsync(transaction, BASE_REALM);
    }

    public static void executeRealmTransactionAsync(Realm.Transaction transaction, int chooseRealm) {
        executeRealmTransactionAsync(transaction, chooseRealm, null);
    }

    public static void executeRealmTransactionAsync(Realm.Transaction transaction, Realm.Transaction.OnSuccess onSuccess) {
        executeRealmTransactionAsync(transaction, BASE_REALM, onSuccess);
    }

    public static void executeRealmTransactionAsync(Realm.Transaction transaction, int chooseRealm, Realm.Transaction.OnSuccess onSuccess) {
        executeRealmTransactionAsync(transaction, chooseRealm, onSuccess, null);
    }

    public static void executeRealmTransactionAsync(Realm.Transaction transaction, int chooseRealm, @Nullable Realm.Transaction.OnSuccess onSuccess, @Nullable Realm.Transaction.OnError onError) {
        DataSourceThreadHelper.getHandler().post(() -> {
            Realm realm = getRealmByID(chooseRealm);
            if (realm != null) {
                realm.executeTransactionAsync(transaction, onSuccess, onError);
            }
        });
    }

    private static void fireRealmChangeEvent() {
        Iterator<WeakReference<RealmChangeDelegate>> it = realmChangeDelegateWeakReferences.iterator();
        while (it.hasNext()) {
            WeakReference<RealmChangeDelegate> item = it.next();
            if (item.get() == null) {
                it.remove();
            } else {
                item.get().realmConfigurationUpdated();
            }
        }
    }

    public static void addRealmChangeDelegate(RealmChangeDelegate delegate) {
        Iterator<WeakReference<RealmChangeDelegate>> it = realmChangeDelegateWeakReferences.iterator();
        while (it.hasNext()) {
            WeakReference<RealmChangeDelegate> item = it.next();
            if (item.get() == null) {
                it.remove();
            } else if (item.get() == delegate) {
                return;
            }
        }
        realmChangeDelegateWeakReferences.add(new WeakReference<>(delegate));
        delegate.realmConfigurationUpdated();
    }

    public static void removeRealmChangeDelegate(RealmChangeDelegate delegate) {
        Iterator<WeakReference<RealmChangeDelegate>> it = realmChangeDelegateWeakReferences.iterator();
        while (it.hasNext()) {
            WeakReference<RealmChangeDelegate> item = it.next();
            if (item.get() == null || item.get() == delegate) {
                it.remove();
            }
        }
    }

    public interface RealmChangeDelegate {
        void realmConfigurationUpdated();
    }

    public interface RealmTask {
        void execute(Realm realm);
    }

    public static void executeWithRealm(RealmTask task) {
        executeWithRealm(task, BASE_REALM);
    }

    private static Realm getRealmByID(int id) {
        switch (id) {
            case BASE_REALM:
                return getRealmInstance();
        }
        return null;
    }

    public static void executeWithRealm(RealmTask task, int chooseREALM) {
        Realm realm = getRealmByID(chooseREALM);

        if (realm != null) {
            task.execute(realm);
        }
    }

    public static Realm getRealmInstance() {
        if (realmConfiguration == null) {
            updateRealmConfiguration();
            if (realmConfiguration == null) {
                return null;
            }
        }
        return Realm.getInstance(realmConfiguration);
    }

    public static void updateRealmConfiguration() {
        String uid = PreferencesHelper.getString(PreferencesHelper.USERNAME);
        RealmConfiguration newConfiguration;
        if (!uid.isEmpty()) {
            newConfiguration = new RealmConfiguration.Builder().name(uid).deleteRealmIfMigrationNeeded().build();
            fireRealmChangeEvent();
        } else {
            newConfiguration = null;
        }
        if (!Equalizer.equals(newConfiguration, realmConfiguration)) {
            realmConfiguration = newConfiguration;
            fireRealmChangeEvent();
        }
    }

    public static void clearList(@Nullable List<? extends RealmObject> list, @Nonnull Realm realm) {
        if (list != null) {
            RealmObject[] oldObjects = list.toArray(new RealmObject[0]);
            for (int i = 0; i < oldObjects.length; i++) {
                RealmObject object = oldObjects[i];
                RealmManager.deleteRecursively(object, realm, false);
            }
        }
    }

    public static void deleteRecursively(RealmObject object, @Nullable Realm realm, boolean onlyChildObjects) {
        safeExecute(realm, realm1 -> {
            if (object != null && object.isValid()) {
                Class clazz = object.getClass();
                if (clazz.getName().contains("Proxy")) {
                    clazz = clazz.getSuperclass();
                }

                for (Field field : clazz.getFields()) {
                    Class type = field.getType();
                    String name = field.getName();

                    if (field.isAnnotationPresent(AutoDelete.class)) {
                        if (RealmObject.class.isAssignableFrom(type)) {
                            String methodName = "get" + name.substring(0, 1).toUpperCase() + name.substring(1);
                            try {
                                RealmObject _obj = (RealmObject) clazz.getMethod(methodName).invoke(object);
                                deleteRecursively(_obj, realm1, false);
                            } catch (Exception ignored) {
                            }
                        } else if (RealmList.class.isAssignableFrom(type)) {
                            RealmList list = null;
                            String methodName = "get" + name.substring(0, 1).toUpperCase() + name.substring(1);
                            try {
                                list = (RealmList) clazz.getMethod(methodName).invoke(object);
                            } catch (Exception e) {
                            }
                            if (list != null) {
                                for (Object o : list) {
                                    if (RealmObject.class.isAssignableFrom(o.getClass())) {
                                        deleteRecursively((RealmObject) o, realm1, true);
                                    }
                                }
                                list.deleteAllFromRealm();
                            }
                        }
                    }
                }

                for (Method method : clazz.getMethods()) {
                    if (method.isAnnotationPresent(AutoDelete.class)) {
                        try {
                            method.invoke(object, realm1);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            Log.i("REALMMANAGER", "AUTODELETE ERROR " + method.getName());
                            e.printStackTrace();
                        }
                    }
                }
                if (!onlyChildObjects) {
                    object.deleteFromRealm();
                }
            }
        });
    }
}