package com.volacious.jf.engine.copyservice.processors.upload;

import android.content.Context;
import android.util.Log;

import com.volacious.jf.engine.copyservice.processors.FileUtils;
import com.volacious.jf.engine.network.result.FileTransfer;
import com.volacious.jf.helpers.DoubleHelper;
import com.volacious.jf.ui.fragment.uploadprocess.UIUpdateEvent;
import com.volacious.jf.ui.fragment.uploadprocess.UIUpdateInterface;
import com.volacious.jf.util.adapter.FileStatus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.volacious.jf.util.adapter.FileStatus.Status.copyDone;
import static com.volacious.jf.util.adapter.FileStatus.Status.uploadDone;
import static com.volacious.jf.util.adapter.FileStatus.Status.uploading;

public class UploadProgress implements UIUpdateInterface, Serializable {
    private long totalSize;
    private long doneSize;
    private long filesDone;
    private long filesCount;
    private List<File> files;
    private List<FileComplete> filesCompleted;
    private Long transferSpeed;
    private FileTransfer fileTransfer;

    public UploadProgress(FileTransfer fileTransfer) {
        this.fileTransfer = fileTransfer;
        this.files = fileTransfer.getFilesToUpload();
        filesCompleted = new ArrayList<>();
        filesCount = files.size();
        totalSize = FileUtils.getFilesSize(fileTransfer.getFilesToUpload());
        filesDone = 0;
    }

    public void setDoneSize(long doneSize) {
        this.doneSize = doneSize;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public long getDoneSize() {
        return doneSize;
    }

    public long getFilesDone() {
        return filesDone;
    }

    public void setFilesDone(long filesDone) {
        this.filesDone = filesDone;
        if (filesDone == filesCount) {
            transferSpeed = getTransferSpeed();
        }
    }

    public long getFilesCount() {
        return filesCount;
    }

    public List<File> getFiles() {
        return files;
    }

    public int getProgressPercent() {
        if (filesDone == filesCount) {
            return 100;
        }
        int res = (int) (((double) getDoneSize() / getTotalSize()) * 100);
        if (res < 0) {
            return 0;
        }
        return res;
    }

    public boolean isFinished() {
        return filesDone == filesCount;
    }

    public long getTransferSpeed() {
        if (transferSpeed != null) {
            return transferSpeed;
        } else {
            return 0;
        }
    }

    public void setTransferSpeed(long transferSpeed) {
        this.transferSpeed = transferSpeed;
    }

    public void updateTransferSpeed(long txBytesBefore, long txBytesAfter, Date dateBefore, Date dateAfter) {
        long timeDiff = dateAfter.getTime() - dateBefore.getTime();
        long res = (txBytesAfter - txBytesBefore) * 8 * 1000 / timeDiff;
        long newRate = (getTransferSpeed() * (getFilesDone() - 1) + res) / getFilesDone();
        Log.i("TRRRRR", "" + (((double) getTransferSpeed()) / 1024 / 1024) + " " + (((double) res) / 1024 / 1024));
        setTransferSpeed(newRate);
    }

    @Override
    public UIUpdateEvent getUIUpdateEvent() {
        UIUpdateEvent updateEvent = new UIUpdateEvent();
        updateEvent.setCopyLabel("Copy finished (100%)");
        updateEvent.setCopyStatus(2);
        updateEvent.setCopyIconVisible(true);
        updateEvent.setNoteText(getFileTransfer().getPhotogNotes());
        updateEvent.setUploadLabel(null);
        if (isStopped()) {
            updateEvent.setUploadStatus(1);
            updateEvent.setLoadingIndicatorVisible(false);
            updateEvent.setProgress(getProgressPercent());
            updateEvent.setStatusLabel("Upload stopped: " + getProgressPercent() + "%");
            updateEvent.setSpeedIconVisible(true);
            updateEvent.setFilesProgressStatus("" + (getFilesCount() - getFilesDone()) +
                    "/" + getFilesCount() + " files left");
            updateEvent.setSpeedLabel(DoubleHelper.asTwoNumbersAfterDot(((double) getTransferSpeed()) / 1024 / 1024) + " Mb/s");
        } else if (isFinished()) {
            updateEvent.setUploadStatus(2);
            updateEvent.setLoadingIndicatorVisible(false);
            updateEvent.setUploadIconVisible(true);
            updateEvent.setUploadLabel("Upload finished (100%)");
            updateEvent.setStatusLabel("Uploaded");
            updateEvent.setSpeedIconVisible(true);
            updateEvent.setSpeedLabel(DoubleHelper.asTwoNumbersAfterDot(((double) getTransferSpeed()) / 1024 / 1024) + " Mb/s");
            updateEvent.setFilesProgressStatus("Files (" + getFilesCount() + ")");
        } else {
            updateEvent.setUploadStatus(1);
            updateEvent.setLoadingIndicatorVisible(true);
            updateEvent.setProgress(getProgressPercent());
            updateEvent.setStatusLabel("Uploading " + getProgressPercent() + "%");
            updateEvent.setSpeedIconVisible(true);
            updateEvent.setFilesProgressStatus("" + (getFilesCount() - getFilesDone()) +
                    "/" + getFilesCount() + " files left");
            updateEvent.setSpeedLabel(DoubleHelper.asTwoNumbersAfterDot(((double) getTransferSpeed()) / 1024 / 1024) + " Mb/s");
        }

        List<FileStatus> fileStatuses = new ArrayList<>();
        List<File> allFiles = getFiles();
        for (int i = 0; i < allFiles.size(); i++) {
            if (UploadProcessor.getFilesInProgress().contains(allFiles.get(i))) {
                fileStatuses.add(new FileStatus(allFiles.get(i).getName(), uploading));
            } else if (isFileCompleted(allFiles.get(i))) {
                fileStatuses.add(new FileStatus(allFiles.get(i).getName(), uploadDone));
            } else {
                fileStatuses.add(new FileStatus(allFiles.get(i).getName(), copyDone));
            }
        }

        updateEvent.setFileStatuses(fileStatuses);

        return updateEvent;
    }

    public FileTransfer getFileTransfer() {
        return fileTransfer;
    }

    public void setFileTransfer(FileTransfer fileTransfer) {
        this.fileTransfer = fileTransfer;
    }

    public void saveToDisk() {
        if (getFileTransfer().getFilesToUpload().size() > 0) {
            File parent = getFileTransfer().getFilesToUpload().get(0).getParentFile();
            try {
                File upFile = new File(parent, "uploadprogress_temp");
                FileOutputStream fileOut =
                        new FileOutputStream(upFile);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(this);
                out.close();
                fileOut.close();

                File destUpFile = new File(parent, "uploadprogress");
                if (destUpFile.exists()) {
                    destUpFile.delete();
                }
                upFile.renameTo(destUpFile);
                System.out.printf("uploadprogress saved to " + upFile.getPath());
            } catch (IOException i) {
                i.printStackTrace();
            }
        }
    }

    public static UploadProgress retrieveUploadProgressFromDisk(Context context, int fileTransferId) {
        try {
            File appDir = context.getFilesDir();
            String dirName = "" + fileTransferId;
            File fileTransferDir = new File(appDir, dirName);
            File upFile = new File(fileTransferDir, "uploadprogress");
            FileInputStream fileIn = new FileInputStream(upFile);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            UploadProgress ft = (UploadProgress) in.readObject();
            in.close();
            fileIn.close();
            return ft;
        } catch (Exception ignored) {}
        return null;
    }

    public boolean isFileCompleted(File file) {
        if (filesCompleted != null) {
            for (FileComplete fileComplete : filesCompleted) {
                if (fileComplete.getFile().equals(file)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void addFileToCompleted(File file, Date start, Date end) {
        if (filesCompleted == null) {
            filesCompleted = new ArrayList<>();
        }
        FileComplete fileComplete = new FileComplete(file, start, end);
        filesCompleted.add(fileComplete);
        double mb = ((double) file.length()) / 1024 / 1024;
        Log.i("SPEEEED", "" + file.getName() + " " + mb + "  " + ((double) fileComplete.getTransferSpeed()) / 1024 / 1024);

    }

    public boolean isStopped() {
        if (getFileTransfer() != null) {
            return !isFinished() && UploadProcessor.fileTransferInProgress != getFileTransfer().getId();
        }
        return false;
    }
}