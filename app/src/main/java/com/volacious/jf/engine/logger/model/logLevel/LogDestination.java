package com.volacious.jf.engine.logger.model.logLevel;

public enum LogDestination {
    device("device"), server("server"), both("both");

    String destination;

    private LogDestination(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return this.destination;
    }
}