package com.volacious.jf.engine.network.result;

import com.google.gson.annotations.SerializedName;

public class PhotogProfile {
    @SerializedName("id")
    private int id;

    @SerializedName("UserId")
    private int UserId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }
}
