package com.volacious.jf.engine.copyservice.processors;

import android.media.ExifInterface;
import android.os.StatFs;

import androidx.annotation.Nullable;

import com.volacious.jf.engine.copyservice.processors.copy.CopyProgress;
import com.volacious.jf.engine.sdcardreceiver.ExternalStorage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class FileUtils {

    public static long filesCount(File sourceLocation) {
        long res = 0;
        for (File file : sourceLocation.listFiles()) {
            if (file.isDirectory()) {
                res += filesCount(file);
            } else {
                res += 1;
            }
        }
        return res;
    }

    public static void sortFiles(List<File> files) {
        files.sort((o1, o2) -> {
            try {
                ExifInterface e1 = new ExifInterface(o1.getPath());
                String datetime1 = e1.getAttribute(ExifInterface.TAG_DATETIME);

                ExifInterface e2 = new ExifInterface(o2.getPath());
                String datetime2 = e2.getAttribute(ExifInterface.TAG_DATETIME);

                if (datetime1 == null) {
                    return -1;
                }
                if (datetime2 == null) {
                    return 1;
                }
                return datetime1.compareTo(datetime2);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        });
    }

    public static List<File> filesList(File sourceLocation) {
        List<File> res = new ArrayList<>();
        for (File file : sourceLocation.listFiles()) {
            if (file.isDirectory()) {
                res.addAll(filesList(file));
            } else {
                res.add(file);
            }
        }
        return res;
    }

    public static long checkAvailableSpace(File directory) {
        StatFs stat = new StatFs(directory.getPath());

        long bytesAvailable = stat.getBlockSizeLong() *stat.getBlockCountLong();
        //long megAvailable   = bytesAvailable / 1048576;
        return bytesAvailable;
    }

    /**
     * @param sourceLocation
     * @param targetLocation
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void copyFile(File sourceLocation, File targetLocation)
            throws FileNotFoundException, IOException {
        InputStream in = new FileInputStream(sourceLocation);
        OutputStream out = new FileOutputStream(targetLocation);

        // Copy the bits from instream to outstream
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public static boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    public static long getFilesSize(List<File> files) {
        long result = 0;
        for (File file : files) {
            result += getFileSize(file);
        }
        return result;
    }

    public static long getFileSize(final File file) {
        if (file == null || !file.exists())
            return 0;
        if (!file.isDirectory())
            return file.length();
        final List<File> dirs = new LinkedList<>();
        dirs.add(file);
        long result = 0;
        while (!dirs.isEmpty()) {
            final File dir = dirs.remove(0);
            if (!dir.exists())
                continue;
            final File[] listFiles = dir.listFiles();
            if (listFiles == null || listFiles.length == 0)
                continue;
            for (final File child : listFiles) {
                result += child.length();
                if (child.isDirectory())
                    dirs.add(child);
            }
        }
        return result;
    }

    public static List<String> getExtSdCardDataPathsDCIM() {
        Map<String, File> externalLocations = ExternalStorage.getAllStorageLocations();
        List<String> result = new ArrayList<>();
        for (File dir : externalLocations.values()) {
            String path = searchPath(dir.getPath(), "DCIM");
            if (path != null) {
                result.add(path);
            }
        }
        return result;
    }

    private static @Nullable
    String searchPath(String path, String searchName) {
        File dir = new File(path);
        if (dir.getName().equals(searchName)) {
            return path;
        }
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : dir.listFiles()) {
                if (file.isDirectory()) {
                    String searchPath = searchPath(file.getPath(), searchName);
                    if(searchPath != null) {
                        return searchPath;
                    }
                }
            }
        }
        return null;
    }
}
