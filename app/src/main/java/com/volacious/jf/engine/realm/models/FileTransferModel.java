package com.volacious.jf.engine.realm.models;

import androidx.annotation.Nullable;

import com.volacious.jf.engine.network.result.FileTransfer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class FileTransferModel extends RealmObject {

    private int id;

    private String copyStatus;

    private String transferStatus;

    private int filesComplete;

    private int numRetries;

    private RealmList<ManifestItemModel> manifest;

    private int photogId;

    private int photoshootId;

    private int fileCount;

    private Date copyStartTime;

    private Date copyEndTime;

    private Date transferStartTime;

    private Date transferEndTime;

    private Float transferProgress;

    private int avgBitsPerSecond;

    private String photogNotes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCopyStatus() {
        return copyStatus;
    }

    public void setCopyStatus(String copyStatus) {
        this.copyStatus = copyStatus;
    }

    public String getTransferStatus() {
        return transferStatus;
    }

    public void setTransferStatus(String transferStatus) {
        this.transferStatus = transferStatus;
    }

    public int getFilesComplete() {
        return filesComplete;
    }

    public void setFilesComplete(int filesComplete) {
        this.filesComplete = filesComplete;
    }

    public int getNumRetries() {
        return numRetries;
    }

    public void setNumRetries(int numRetries) {
        this.numRetries = numRetries;
    }

    public int getPhotogId() {
        return photogId;
    }

    public int getPhotoshootId() {
        return photoshootId;
    }

    public int getFileCount() {
        return fileCount;
    }

    public Date getCopyStartTime() {
        return copyStartTime;
    }

    public void setCopyStartTime(Date copyStartTime) {
        this.copyStartTime = copyStartTime;
    }

    public Date getCopyEndTime() {
        return copyEndTime;
    }

    public void setCopyEndTime(Date copyEndTime) {
        this.copyEndTime = copyEndTime;
    }

    public Date getTransferStartTime() {
        return transferStartTime;
    }

    public void setTransferStartTime(Date transferStartTime) {
        this.transferStartTime = transferStartTime;
    }

    public Date getTransferEndTime() {
        return transferEndTime;
    }

    public void setTransferEndTime(Date transferEndTime) {
        this.transferEndTime = transferEndTime;
    }

    public int getAvgBitsPerSecond() {
        return avgBitsPerSecond;
    }

    public void setAvgBitsPerSecond(int avgBitsPerSecond) {
        this.avgBitsPerSecond = avgBitsPerSecond;
    }

    public RealmList<ManifestItemModel> getManifest() {
        return manifest;
    }

    public void setManifest(RealmList<ManifestItemModel> manifest) {
        this.manifest = manifest;
    }

    public void setPhotogId(int photogId) {
        this.photogId = photogId;
    }

    public void setPhotoshootId(int photoshootId) {
        this.photoshootId = photoshootId;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public Float getTransferProgress() {
        return transferProgress == null ? 0 : transferProgress;
    }

    public void setTransferProgress(Float transferProgress) {
        this.transferProgress = transferProgress;
    }

    public String getPhotogNotes() {
        return photogNotes;
    }

    public void setPhotogNotes(String photogNotes) {
        this.photogNotes = photogNotes;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof FileTransfer) {
            return getId() == ((FileTransfer) obj).getId();
        }
        return false;
    }

    public List<String> getFiles() {
        if (manifest != null) {
            List<String> result = new ArrayList<>();
            for (ManifestItemModel manifestItem : manifest) {
                result.add(manifestItem.getFilename());
            }
            return result;
        }
        return new ArrayList<>();
    }
}
