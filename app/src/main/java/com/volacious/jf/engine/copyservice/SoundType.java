package com.volacious.jf.engine.copyservice;

import com.volacious.jf.R;

public enum SoundType {
    COPY_STARTED (R.raw.electricdrill3),
    COPY_FINISHED (R.raw.cuttingpaper2),
    UPLOAD_STARTED (R.raw.clong1),
    UPLOAD_FINISHED (R.raw.windchime2);

    private final int value;

    SoundType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
