package com.volacious.jf.engine.network.result;


import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Photoshoot implements IDInterface {
    @SerializedName("id")
    private int id;

    @SerializedName("datetime")
    private Date datetime;

    @SerializedName("duration")
    private int duration;

    @SerializedName("price")
    private int price;

    @SerializedName("region")
    private String region;

    @SerializedName("apptTypeName")
    private String apptTypeName;

    @SerializedName("status")
    private String status;

    @SerializedName("paid")
    private boolean paid;

    @SerializedName("type")
    private String type;

    @SerializedName("sqFt")
    private int sqFt;

    @SerializedName("ownerType")
    private String ownerType;

    @SerializedName("ownerId")
    private int ownerId;

    @SerializedName("createdAt")
    private Date createdAt;

    @SerializedName("updatedAt")
    private Date updatedAt;

    @SerializedName("PropertyId")
    private int PropertyId;

    @SerializedName("photogId")
    private int photogId;

    @SerializedName("editorId")
    private int editorId;

    @SerializedName("Property")
    private Property Property;

    private List<FileTransfer> fileTransfers;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getApptTypeName() {
        return apptTypeName;
    }

    public void setApptTypeName(String apptTypeName) {
        this.apptTypeName = apptTypeName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSqFt() {
        return sqFt;
    }

    public void setSqFt(int sqFt) {
        this.sqFt = sqFt;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getPropertyId() {
        return PropertyId;
    }

    public void setPropertyId(int propertyId) {
        PropertyId = propertyId;
    }

    public int getPhotogId() {
        return photogId;
    }

    public void setPhotogId(int photogId) {
        this.photogId = photogId;
    }

    public int getEditorId() {
        return editorId;
    }

    public void setEditorId(int editorId) {
        this.editorId = editorId;
    }

    public com.volacious.jf.engine.network.result.Property getProperty() {
        return Property;
    }

    public void setProperty(com.volacious.jf.engine.network.result.Property property) {
        Property = property;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof Photoshoot) {
            return getId() == ((Photoshoot)obj).getId();
        }
        return false;
    }

    public List<FileTransfer> getFileTransfers() {
        return fileTransfers;
    }

    public void setFileTransfers(List<FileTransfer> fileTransfers) {
        this.fileTransfers = fileTransfers;
    }
}
