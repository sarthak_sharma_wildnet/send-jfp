package com.volacious.jf.engine.logger.model;

public class LogRequestResponse {
    private boolean done;

    public LogRequestResponse() {
    }

    public LogRequestResponse(boolean done) {
        this.done = done;
    }

    public boolean isDone() {
        return done;
    }
}
