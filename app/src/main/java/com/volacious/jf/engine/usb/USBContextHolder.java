package com.volacious.jf.engine.usb;

import android.content.Context;

public interface USBContextHolder {
    Context getUSBContext();
}
