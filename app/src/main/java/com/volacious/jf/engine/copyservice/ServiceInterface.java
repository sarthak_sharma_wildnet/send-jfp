package com.volacious.jf.engine.copyservice;

import android.content.Context;

import com.volacious.jf.engine.copyservice.processors.ServiceProcessor;
import com.volacious.jf.engine.copyservice.processors.upload.UploadProcessor;

import io.reactivex.annotations.Nullable;

public interface ServiceInterface {
    Context getContext();
    void processStarted(ServiceProcessor processor);
    void processFinished(ServiceProcessor processor, @Nullable ServiceProcessor nextProcessor);
    void playSound(SoundType soundType);
    void createNotificationChannel(int channelId);
    void deleteNotificationChannel(int channelId);
}
