package com.volacious.jf.engine.network.result;

import android.content.Context;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.volacious.jf.engine.APIManager;
import com.volacious.jf.engine.copyservice.processors.upload.UploadProcessor;
import com.volacious.jf.engine.copyservice.processors.upload.UploadProgress;
import com.volacious.jf.helpers.DateHelper;
import com.volacious.jf.helpers.DoubleHelper;
import com.volacious.jf.ui.fragment.uploadprocess.UIUpdateEvent;
import com.volacious.jf.ui.fragment.uploadprocess.UIUpdateInterface;
import com.volacious.jf.util.adapter.FileStatus;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.volacious.jf.util.adapter.FileStatus.Status.copyDone;
import static com.volacious.jf.util.adapter.FileStatus.Status.none;
import static com.volacious.jf.util.adapter.FileStatus.Status.uploadDone;

public class FileTransfer implements  IDInterface, Serializable {
    @SerializedName("id")
    private int id;

    @SerializedName("copyStatus")
    private String copyStatus;
    @SerializedName("transferStatus")
    private String transferStatus;

    @SerializedName("filesComplete")
    private int filesComplete;

    @SerializedName("numRetries")
    private int numRetries;

    @SerializedName("manifest")
    private List<ManifestItem> manifest;

    @SerializedName("photogId")
    private int photogId;

    @SerializedName("PhotoshootId")
    private int photoshootId;

    @SerializedName("fileCount")
    private int fileCount;

    @SerializedName("copyStartTime")
    private Date copyStartTime;

    @SerializedName("copyEndTime")
    private Date copyEndTime;

    @SerializedName("transferStartTime")
    private Date transferStartTime;

    @SerializedName("transferEndTime")
    private Date transferEndTime;

    @SerializedName("transferProgress")
    private Float transferProgress;

    @SerializedName("avgBitsPerSecond")
    private int avgBitsPerSecond;

    @SerializedName("photogNotes")
    private String photogNotes;

    private List<File> filesToUpload;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCopyStatus() {
        return copyStatus;
    }

    public void setCopyStatus(String copyStatus) {
        this.copyStatus = copyStatus;
    }

    public String getTransferStatus() {
        return transferStatus;
    }

    public void setTransferStatus(String transferStatus) {
        this.transferStatus = transferStatus;
    }

    public int getFilesComplete() {
        return filesComplete;
    }

    public void setFilesComplete(int filesComplete) {
        this.filesComplete = filesComplete;
    }

    public int getNumRetries() {
        return numRetries;
    }

    public void setNumRetries(int numRetries) {
        this.numRetries = numRetries;
    }

    public int getPhotogId() {
        return photogId;
    }

    public int getPhotoshootId() {
        return photoshootId;
    }

    public int getFileCount() {
        return fileCount;
    }

    public Date getCopyStartTime() {
        return copyStartTime;
    }

    public void setCopyStartTime(Date copyStartTime) {
        this.copyStartTime = copyStartTime;
    }

    public Date getCopyEndTime() {
        return copyEndTime;
    }

    public void setCopyEndTime(Date copyEndTime) {
        this.copyEndTime = copyEndTime;
    }

    public Date getTransferStartTime() {
        return transferStartTime;
    }

    public void setTransferStartTime(Date transferStartTime) {
        this.transferStartTime = transferStartTime;
    }

    public Date getTransferEndTime() {
        return transferEndTime;
    }

    public void setTransferEndTime(Date transferEndTime) {
        this.transferEndTime = transferEndTime;
    }

    public int getAvgBitsPerSecond() {
        return avgBitsPerSecond;
    }

    public void setAvgBitsPerSecond(int avgBitsPerSecond) {
        this.avgBitsPerSecond = avgBitsPerSecond;
    }

    public List<ManifestItem> getManifest() {
        return manifest;
    }

    public void setManifest(List<ManifestItem> manifest) {
        this.manifest = manifest;
    }

    public void setPhotogId(int photogId) {
        this.photogId = photogId;
    }

    public void setPhotoshootId(int photoshootId) {
        this.photoshootId = photoshootId;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public Float getTransferProgress() {
        return transferProgress == null ? 0 : transferProgress;
    }

    public void setTransferProgress(Float transferProgress) {
        this.transferProgress = transferProgress;
    }

    public String getPhotogNotes() {
        return photogNotes;
    }

    public void setPhotogNotes(String photogNotes) {
        this.photogNotes = photogNotes;
    }


    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof FileTransfer) {
            return getId() == ((FileTransfer) obj).getId();
        }
        return false;
    }

    public List<String> getFiles() {
        if (manifest != null) {
            List<String> result = new ArrayList<>();
            for (ManifestItem manifestItem : manifest) {
                result.add(manifestItem.getFilename());
            }
            return result;
        }
        return new ArrayList<>();
    }

    public List<File> getFilesToUpload() {
        return filesToUpload;
    }

    public void setFilesToUpload(List<File> filesToUpload) {
        this.filesToUpload = filesToUpload;
    }

    public void sendUpdate() {
        APIManager.retrofitService.updateFileTransfer(getId(), this).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    public boolean isPossibleToContinue(Context context) {
        boolean result = true;
        if (getFilesComplete() == getFileCount()) {
            result = false;
        }

        UploadProgress up = UploadProgress.retrieveUploadProgressFromDisk(context, getId());

        if (up != null) {
            for (File file : up.getFileTransfer().getFilesToUpload()) {
                if (!file.exists()) {
                    result = false;
                    break;
                }
            }
        } else {
            result = false;
        }

        return result;
    }
}
