package com.volacious.jf.helpers;

import android.util.Log;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class DoubleHelper {

    public static String asTwoNumbersAfterDot(Double d) {
        Log.i("FFFFFF", "" + d);
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(d);
    }

}
