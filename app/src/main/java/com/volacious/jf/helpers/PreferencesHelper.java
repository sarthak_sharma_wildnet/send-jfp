package com.volacious.jf.helpers;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.volacious.jf.engine.network.result.User;

/**
 * Created by 76332 on 23.02.2018.
 */

public class PreferencesHelper {
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String LAST_MAIN_FRAGMENT = "lastMainFragment";

    public static SharedPreferences sharedPreferences;

    public static String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public static void removeString(String key) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.apply();
    }

    public static void saveString(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }
}
