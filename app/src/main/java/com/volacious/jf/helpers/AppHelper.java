package com.volacious.jf.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Source: Courtesy
 */

public class AppHelper {
    public static String checksum;
// --Commented out by Inspection START (4/24/2018 4:08 PM):
//    /**
//     * Turn drawable resource into byte array.
//     *
//     * @param context parent context
//     * @param id      drawable resource id
//     * @return byte array
//     */
//    public static byte[] getFileDataFromDrawable(Context context, int id) {
//        Drawable drawable = ContextCompat.getDrawable(context, id);
//        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
//        return byteArrayOutputStream.toByteArray();
//    }
// --Commented out by Inspection STOP (4/24/2018 4:08 PM)

    /**
     * Turn drawable into byte array.
     *
     * @param drawable data
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, Drawable drawable) {

        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);


        checksum =MD5_Hash(toHexadecimal(byteArrayOutputStream.toByteArray()));
        return byteArrayOutputStream.toByteArray();
    }

    private static String toHexadecimal(byte[] digest){
        String hash = "";
        for(byte aux : digest) {
            int b = aux & 0xff;
            if (Integer.toHexString(b).length() == 1) hash += "0";
            hash += Integer.toHexString(b);
        }
        return hash;
    }


    public static String MD5_Hash(String s) {
        MessageDigest m = null;

        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        m.update(s.getBytes(),0,s.length());
        String hash = new BigInteger(1, m.digest()).toString(16);
        return hash;
    }

}
