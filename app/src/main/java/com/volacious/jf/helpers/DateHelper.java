package com.volacious.jf.helpers;

import androidx.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class DateHelper {

    public static boolean isToday(Date date) {
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.setTime(date);
        Calendar now = Calendar.getInstance();
        return now.get(Calendar.DATE) == dateCalendar.get(Calendar.DATE);
    }

    public static String getDateAsDMY(@Nullable Date date) {
        Date localDate;
        if (date == null) {
            localDate = Calendar.getInstance().getTime();
        } else {
            localDate = date;
        }
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        return df.format(localDate);
    }

    public static String getDateAsHM(@Nullable Date date) {
        Date localDate;
        if (date == null) {
            localDate = Calendar.getInstance().getTime();
        } else {
            localDate = date;
        }
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        return df.format(localDate);
    }

    public static String getDateAsMDYHM(@Nullable Date date) {
        Date localDate;
        if (date == null) {
            localDate = Calendar.getInstance().getTime();
        } else {
            localDate = date;
        }
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy HH:mm");
        return df.format(localDate);
    }

    public static String getDateAsMMDDYYYY(@Nullable Date date) {
        Date localDate;
        if (date == null) {
            localDate = Calendar.getInstance().getTime();
        } else {
            localDate = date;
        }
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy");
        return df.format(localDate);
    }

    public static String dateToISOString(Date date) {
        if (date != null) {
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.getDefault());
            dateformat.setTimeZone(TimeZone.getDefault());
            return dateformat.format(date);
        }
        return "";
    }

    public static String getDateAsMonthDYHM(@Nullable Date date) {
        String finalDate;
        Date localDate;
        if (date == null) {
            localDate = Calendar.getInstance().getTime();
        } else {
            localDate = date;
        }
        SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
        return df.format(localDate);
    }
}
